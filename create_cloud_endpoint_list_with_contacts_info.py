#Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
#the European Commission - subsequent versions of the EUPL (the "Licence");
#You may not use this work except in compliance with the Licence.
#You may obtain a copy of the Licence at:
#
#   https://joinup.ec.europa.eu/software/page/eupl
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the Licence is distributed on an "AS IS" basis,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the Licence for the specific language governing permissions and
#limitations under the Licence.

import csv
import json
import time
import sys
import boto3
from datetime import datetime
from botocore.exceptions import ClientError
import requests

#CREDENTIALS_FILE = '/home/ubuntu/.aws/credentials'
TENABLE_REGION = 'eu-central-1'
TENABLE_SECRET_KEY_NAME = 'prod/TenableAPI/S1_VA_AWS_ScriptRunner'
AWS_CONTACTS_FILENAME = 'aws-accounts.complete.txt.all'
AWS_CONTACTS_FILE = '/home/ubuntu/outputs/' + AWS_CONTACTS_FILENAME
AZURE_CONTACTS_FILENAME = 'azure-accounts.complete.txt.all'
AZURE_CONTACTS_FILE = '/home/ubuntu/outputs/' + AZURE_CONTACTS_FILENAME
#VULN_WITH_CONTACTS_FILENAME = 'vuln-cloud-assets-with-contacts.csv'
#VULN_WITH_CONTACTS_FILE = '/home/ubuntu/outputs/' + VULN_WITH_CONTACTS_FILENAME
ENDPOINTS_WITH_CONTACTS_FILENAME = 'endpoints-with-contacts.csv'
ENDPOINTS_WITH_CONTACTS_FILE = '/home/ubuntu/outputs/' + ENDPOINTS_WITH_CONTACTS_FILENAME
AWS_ENDPOINTS_FILENAME = 'aws-public_ip_address.csv'
AWS_ENDPOINTS_FILE = '/home/ubuntu/outputs/' + AWS_ENDPOINTS_FILENAME
AZURE_ENDPOINTS_FILENAME = 'azure-public_ip_address.csv'
AZURE_ENDPOINTS_FILE = '/home/ubuntu/outputs/' + AZURE_ENDPOINTS_FILENAME

#WAIT_TIME_BETWEEN_CALLS = 5
#WAIT_TIME_BETWEEN_ITERATIONS = 15
#NUMBER_OF_ITERATIONS = 5
SA_S3_BUCKET_NAME = 'ist-vm'
SA_S3_BUCKET_NAME_ENDPOINT = 'exports/'
S2_S3_BUCKET_NAME = 's2-share'
S2_S3_BUCKET_NAME_ENDPOINT = 'exports/'
TOPIC_ARN = 'arn:aws:sns:eu-central-1:042034201921:endpoints-with-contacts-csv-file-processing'

#starting execution of the script
startDateTime = datetime.today()

print("\n---> Script execution started @ {0} ...\n".format(startDateTime))

try:

#############################################################
#
# fetching cloud account contact information into list
#
#############################################################

    print("\n---> Fetching cloud account information into list")

    print("---> Opening AWS cloud account contact file {0} for reading".format(AWS_CONTACTS_FILE))

    with open(AWS_CONTACTS_FILE, mode='r') as csvFile:
        print("-----> Reading file started ...".format(AWS_CONTACTS_FILE))
        csvReader = csv.DictReader(csvFile, delimiter=';')
        print("-----> Reading file finished!".format(AWS_CONTACTS_FILE))
        print("-----> Reading file into list started ...".format(AWS_CONTACTS_FILE))
        awsContactList = list(csvReader)
        print("-----> Reading file into list finished!".format(AWS_CONTACTS_FILE))

    print("---> Opening AZURE cloud account contact file {0} for reading".format(AZURE_CONTACTS_FILE))

    with open(AZURE_CONTACTS_FILE, mode='r') as csvFile:
        print("-----> Reading file started ...".format(AZURE_CONTACTS_FILE))
        csvReader = csv.DictReader(csvFile, delimiter=';')
        print("-----> Reading file finished!".format(AZURE_CONTACTS_FILE))
        print("-----> Reading file into list started ...".format(AZURE_CONTACTS_FILE))
        azureContactList = list(csvReader)
        print("-----> Reading file into list finished!".format(AZURE_CONTACTS_FILE))

#############################################################
#
# fetching cloud endpoint information into list
#
#############################################################

    print("\n---> Fetching cloud endpoint information into list")

    print("---> Opening AWS cloud endpoint file {0} for reading".format(AWS_ENDPOINTS_FILE))

    with open(AWS_ENDPOINTS_FILE, mode='r') as csvFile:
        print("-----> Reading file started ...".format(AWS_ENDPOINTS_FILE))
        csvReader = csv.reader(csvFile, delimiter=';')
        print("-----> Reading file finished!".format(AWS_ENDPOINTS_FILE))
        print("-----> Reading file into list started ...".format(AWS_ENDPOINTS_FILE))
        awsEndpointList = list(csvReader)
        print("-----> Reading file into list finished!".format(AWS_ENDPOINTS_FILE))

    print("---> Opening AZURE cloud endpoint file {0} for reading".format(AZURE_ENDPOINTS_FILE))

    with open(AZURE_ENDPOINTS_FILE, mode='r') as csvFile:
        print("-----> Reading file started ...".format(AZURE_ENDPOINTS_FILE))
        csvReader = csv.reader(csvFile, delimiter=';')
        print("-----> Reading file finished!".format(AZURE_ENDPOINTS_FILE))
        print("-----> Reading file into list started ...".format(AZURE_ENDPOINTS_FILE))
        azureEndpointList = list(csvReader)
        print("-----> Reading file into list finished!".format(AZURE_ENDPOINTS_FILE))

#############################################################
#
# connecting endpoint information with contact information
#
#############################################################

    finalList = list()
    for awsEndpoint in awsEndpointList:
        finalListRow = list()
        for awsContact in awsContactList:
            if awsEndpoint[0] == awsContact['Account Identifier']:
                awsEndpoint.insert(0, 'AWS')
                awsEndpoint.append(awsContact['Account Organisation Acronym'])
                awsEndpoint.append(awsContact['Responsible contact names'])
                awsEndpoint.append(awsContact['Responsible contact emails'])
                awsEndpoint.append(awsContact['Responsible contact phones'])
                awsEndpoint.append(awsContact['Operation contact names'])
                awsEndpoint.append(awsContact['Operation contact emails'])
                awsEndpoint.append(awsContact['Operation contact phones'])
                awsEndpoint.append(awsContact['Security contact names'])
                awsEndpoint.append(awsContact['Security contact emails'])
                awsEndpoint.append(awsContact['Security contact phones'])
                finalList.append(awsEndpoint)
                break

    for azureEndpoint in azureEndpointList:
        finalListRow = list()
        for azureContact in azureContactList:
            if azureEndpoint[0] == azureContact['Account Identifier']:
                azureEndpoint.insert(0, 'AZURE')
                azureEndpoint.append(azureContact['Account Organisation Acronym'])
                azureEndpoint.append(azureContact['Responsible contact names'])
                azureEndpoint.append(azureContact['Responsible contact emails'])
                azureEndpoint.append(azureContact['Responsible contact phones'])
                azureEndpoint.append(azureContact['Operation contact names'])
                azureEndpoint.append(azureContact['Operation contact emails'])
                azureEndpoint.append(azureContact['Operation contact phones'])
                azureEndpoint.append(azureContact['Security contact names'])
                azureEndpoint.append(azureContact['Security contact emails'])
                azureEndpoint.append(azureContact['Security contact phones'])
                finalList.append(azureEndpoint)
                break



#############################################################
#
# writing end result file and putting it to S3, sending SNS
#
#############################################################

    print("\n---> Writing endpoint list with contacts into CSV file: {0}".format(ENDPOINTS_WITH_CONTACTS_FILE))

    with open(ENDPOINTS_WITH_CONTACTS_FILE, 'w') as endpointWithContactCsvFile:
        # using csv.writer method from CSV package
        writeCSV = csv.writer(endpointWithContactCsvFile, delimiter=';')
        writeCSV.writerows(finalList)
    endpointWithContactCsvFile.close()

    print("---> Writing endpoint list with contacts into CSV file successfully finished")

    #putting files in SA S3 bucket
    print("\n---> Putting endpoints with contacts CSV file to S3 bucket ({0})".format(SA_S3_BUCKET_NAME))

    s3 = boto3.client('s3')

    #reading file in temporary string to put in S3 bucket
    tmpFileString = ""
    file = open(ENDPOINTS_WITH_CONTACTS_FILE)
    tmpFileString = file.read()
    file.close()

    s3.put_object(Bucket=SA_S3_BUCKET_NAME, Body=tmpFileString, Key=SA_S3_BUCKET_NAME_ENDPOINT + ENDPOINTS_WITH_CONTACTS_FILENAME)
    print("---> File {0} successfully uploaded to S3 bucket {1}".format(ENDPOINTS_WITH_CONTACTS_FILE, SA_S3_BUCKET_NAME + '/' + SA_S3_BUCKET_NAME_ENDPOINT + ENDPOINTS_WITH_CONTACTS_FILENAME))

    s3.put_object(Bucket=S2_S3_BUCKET_NAME, Body=tmpFileString, Key=S2_S3_BUCKET_NAME_ENDPOINT + ENDPOINTS_WITH_CONTACTS_FILENAME)
    print("---> File {0} successfully uploaded to S3 bucket {1}".format(ENDPOINTS_WITH_CONTACTS_FILE, S2_S3_BUCKET_NAME + '/' + S2_S3_BUCKET_NAME_ENDPOINT + ENDPOINTS_WITH_CONTACTS_FILENAME))

    #sending notification that processing went well
    print("\n---> Send notification to SNS topic: {0}".format(TOPIC_ARN))

    sns = boto3.client('sns')

    messageToSend = "Endpoint with contacts CSV file processing is successfully finished ..."
    subjectToSend = "{0} - Endpoint with contacts CSV file processing - successfully finished".format(startDateTime.strftime("%Y-%m-%d"))

    sns.publish(TopicArn=TOPIC_ARN,
        Message = messageToSend,
        Subject = subjectToSend)

    print("---> SNS notification successfully sent!")

except Exception as e:
    print("ERROR> {0}".format(e))


#finishing execution of the script
endDateTime = datetime.today()
scriptExecutionTime = endDateTime - startDateTime

print("\n---> Script execution ended @ {0}".format(endDateTime))
print("\n---> Total execution time: {0}\n".format(scriptExecutionTime))
