#Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
#the European Commission - subsequent versions of the EUPL (the "Licence");
#You may not use this work except in compliance with the Licence.
#You may obtain a copy of the Licence at:
#
#   https://joinup.ec.europa.eu/software/page/eupl
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the Licence is distributed on an "AS IS" basis,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the Licence for the specific language governing permissions and
#limitations under the Licence.

import boto3
import csv
from datetime import datetime

TOPIC_ARN_DIGIT_S1_001 = 'arn:aws:sns:eu-central-1:042034201921:adhoc_instance_running-digit_s1_001'
TOPIC_ARN_DIGIT_S1_003 = 'arn:aws:sns:eu-central-1:042034201921:adhoc_instance_running-digit_s1_003'

startDateTime = datetime.today()

print("\n---> Script execution started @ {0} ...\n".format(startDateTime))

print("---> Getting EC2 client")

ec2 = boto3.client('ec2')

print("---> Getting SNS client")

sns = boto3.client('sns')

print("---> Getting EC2 instances")

ec2Instances=ec2.describe_instances()

for ec2Reservations in ec2Instances['Reservations']:
    for ec2Instance in ec2Reservations['Instances']:

        currentEc2InstanceName = "no name"
        currentEc2InstanceOwner = "no owner"
        currentEc2InstanceShouldBeNotified = 0

        for ec2InstanceTag in ec2Instance['Tags']:
            if ec2InstanceTag['Key'] == "Name":
                currentEc2InstanceName = ec2InstanceTag['Value']
            if ec2InstanceTag['Key'] == "owner":
                currentEc2InstanceOwner = ec2InstanceTag['Value']
            if ec2InstanceTag['Key'] == "instance-running-type" and ec2InstanceTag['Value'] != "always" and ec2Instance['State']['Name'] == "running": 
                currentEc2InstanceShouldBeNotified = 1

        if currentEc2InstanceShouldBeNotified == 1:
            subjectToSend = "{0} - AWS EC2 status check - instance is still running".format(startDateTime.strftime("%Y-%m-%d"))
            messageToSend = "Instance id: {0}, name: {1}, owner: {2} is {3} but tagged as ad-hoc running instance".format(ec2Instance['InstanceId'], currentEc2InstanceName, currentEc2InstanceOwner, ec2Instance['State']['Name'])

            if currentEc2InstanceOwner == "DIGIT.S1.001":
                currentTopicArn = TOPIC_ARN_DIGIT_S1_001

            if currentEc2InstanceOwner == "DIGIT.S1.003":
                currentTopicArn = TOPIC_ARN_DIGIT_S1_003

            print("\n---> Sending SNS notification")
            print("ARN: {0}".format(currentTopicArn))
            print("Subject: {0}".format(subjectToSend))
            print("Message: {0}".format(messageToSend))


            sns.publish(TopicArn=currentTopicArn, 
                Message = messageToSend, 
                Subject = subjectToSend)

endDateTime = datetime.today()
scriptExecutionTime = endDateTime - startDateTime

print("\n---> Script execution ended @ {0}".format(endDateTime))
print("\n---> Total execution time: {0}\n".format(scriptExecutionTime))
