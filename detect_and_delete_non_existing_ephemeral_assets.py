#Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
#the European Commission - subsequent versions of the EUPL (the "Licence");
#You may not use this work except in compliance with the Licence.
#You may obtain a copy of the Licence at:
#
#   https://joinup.ec.europa.eu/software/page/eupl
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the Licence is distributed on an "AS IS" basis,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the Licence for the specific language governing permissions and
#limitations under the Licence.

import csv
import json
import time
import sys
import boto3
from datetime import datetime
from botocore.exceptions import ClientError
import requests

TENABLE_REGION = 'eu-central-1'
TENABLE_SECRET_KEY_NAME = 'prod/TenableAPI/S1_VA_AWS_ScriptRunner'
WAIT_TIME_BETWEEN_CALLS = 5
WAIT_TIME_BETWEEN_ITERATIONS = 15
NUMBER_OF_ITERATIONS = 5

AWS_ENDPOINTS_FILENAME = 'aws-public_ip_address.csv'
AWS_ENDPOINTS_FILE = '/home/ubuntu/outputs/' + AWS_ENDPOINTS_FILENAME
AZURE_ENDPOINTS_FILENAME = 'azure-public_ip_address.csv'
AZURE_ENDPOINTS_FILE = '/home/ubuntu/outputs/' + AZURE_ENDPOINTS_FILENAME
ASSET_LAST_SEEN_MAX_MINUTES = 1440

TOPIC_ARN = 'arn:aws:sns:eu-central-1:042034201921:tenable-detect-and-delete-non-existing-ephemeral-assets'
ERROR_MESSAGE = None

#starting execution of the script
try:
    startDateTime = datetime.today()

    print("\n---> Script execution started @ {0} ...\n".format(startDateTime))

#################################################################
#
#    Fetching Tenable credentails from AWS secrets manager
#
#################################################################

    print("---> Fetching TENABLE credentials")

    session = boto3.session.Session()
    client = session.client(
        service_name='secretsmanager',
        region_name=TENABLE_REGION,
    )

    try:
        get_secret_value_response = client.get_secret_value(
            SecretId=TENABLE_SECRET_KEY_NAME
        )
    except ClientError as e:
        if e.response['Error']['Code'] == 'ResourceNotFoundException':
            print("The requested secret " + secret_name + " was not found")
        elif e.response['Error']['Code'] == 'InvalidRequestException':
            print("The request was invalid due to:", e)
        elif e.response['Error']['Code'] == 'InvalidParameterException':
            print("The request had invalid params:", e)
        elif e.response['Error']['Code'] == 'DecryptionFailure':
            print("The requested secret can't be decrypted using the provided KMS key:", e)
        elif e.response['Error']['Code'] == 'InternalServiceError':
            print("An error occurred on service side:", e)
        ERROR_MESSAGE = str(e)

    else:
        # Secrets Manager decrypts the secret value using the associated KMS CMK
        # Depending on whether the secret was a string or binary, only one of these fields will be populated
        if 'SecretString' in get_secret_value_response:
            #convert result string encoded as key value JSON payload into Python list
            secretDataText = get_secret_value_response['SecretString']
            secretDataList = json.loads(secretDataText)
            TENABLE_ACCESS_KEY = secretDataList['accessKey']
            TENABLE_SECRET_KEY = secretDataList['secretKey']
        else:
            #binary output is not compatible
            raise Exception("Tenable API secret returned in binary. Script execution cannot continue.")

    #setting tenable access key string for API calls
    TENABLE_API_KEY="accessKey={0};secretKey={1}".format(TENABLE_ACCESS_KEY, TENABLE_SECRET_KEY)

    print("---> TENABLE Credentials successfuly fetched")


#############################################################
#
# exporting assets from Tenable
#
#############################################################

    print("\n---> Exporting assets from Tenable")

    apiUrl = "https://cloud.tenable.com/assets/export"

    payload = {
        "filters": {"tag.CLOUD-PROVIDER": ["AWS", "AZURE"]},
        "chunk_size": 5000
    }
    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "X-ApiKeys": TENABLE_API_KEY
    }

    response = requests.request("POST", apiUrl, json=payload, headers=headers)

    if response.status_code != 200:
        errorText="Error occured while exporting assets from Tenable (API return code {0})".format(response.error_code)
        raise Exception(errorText)

    assetsExportResponse=json.loads(response.text)

#############################################################
#
# fetching assets export status from Tenable
#
#############################################################

    print("\n---> Fetching assets export status")

    print("\n---> Waiting for {0} seconds before continuing with next API call".format(WAIT_TIME_BETWEEN_CALLS))
    time.sleep(WAIT_TIME_BETWEEN_CALLS)

    apiUrl = "https://cloud.tenable.com/assets/export/{0}/status".format(assetsExportResponse['export_uuid'])

    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "X-ApiKeys": TENABLE_API_KEY
    }

    print("---> Calling vulnerabilities export status API")
    for i in range(0, NUMBER_OF_ITERATIONS):
        print("-----> Iteration {0} of {1}".format(i+1, NUMBER_OF_ITERATIONS))
        response = requests.request("GET", apiUrl, headers=headers)

        if response.status_code != 200:
            errorText="Error occured while fetching assets export status from Tenable (API return code {0})".format(response.error_code)
            raise Exception(errorText)

        assetsExportStatusResponse=json.loads(response.text)

        if assetsExportStatusResponse['status'] == "FINISHED":
            print("-----> Call successfuly finished!")
            break

        print("-----> Waiting for {0} seconds before continuing with next iteration step (API call)".format(WAIT_TIME_BETWEEN_ITERATIONS))
        time.sleep(WAIT_TIME_BETWEEN_ITERATIONS)

    #if the loops finished and execution did'n finish, raise expection and dump last execution results
    if assetsExportStatusResponse['status'] != "FINISHED":
        errorText="Error occured while fetching status for assets export. Response dump: {0}".format(response.text)
        raise Exception(errorText)


#############################################################
#
# fetching assets export chunks from Tenable
#
#############################################################

    print("\n---> Downloading assets export chunks")

    print("\n---> Waiting for {0} seconds before continuing with next API call".format(WAIT_TIME_BETWEEN_CALLS))
    time.sleep(WAIT_TIME_BETWEEN_CALLS)
    assetsData = list()

    for currentChunk in assetsExportStatusResponse['chunks_available']:

        print("\n-----> Initiating download of chunk {0} of {1}".format(currentChunk,len(assetsExportStatusResponse['chunks_available'])))
        apiUrl = "https://cloud.tenable.com/assets/export/{0}/chunks/{1}".format(assetsExportResponse['export_uuid'], currentChunk)

        headers = {
            "Accept": "application/json",
            "X-ApiKeys": TENABLE_API_KEY
        }

        print("-----> Calling asset chunk export API")

        for i in range(0, NUMBER_OF_ITERATIONS):
            print("-------> Iteration {0} of {1}".format(i+1, NUMBER_OF_ITERATIONS))
            response = requests.request("GET", apiUrl, headers=headers)

            if response.status_code != 200:
                errorText="Error occured while fetching assets chunk from Tenable (API return code {0})".format(response.error_code)
                raise Exception(errorText)
            else:
                print("-------> Chunk {0} started downloading ...".format(currentChunk))
                assetsData.extend(json.loads(response.text))
                #f = open("/home/ubuntu/outputs/vulntest/assets_chunk_{0}.json".format(currentChunk), "w")
                #f.write(response.text)
                #f.close()
                print("-------> Chunk {0} sucessfuly downloaded!".format(currentChunk))
                break

    print("---> Tenable assets successfuly downloaded")


#################################################################
#
#    Fetching AWS endpoints in the list
#
#################################################################

    print("\n---> Fetching AWS endpoints information into list")

    print("---> Opening file {0} for reading".format(AWS_ENDPOINTS_FILE))

    with open(AWS_ENDPOINTS_FILE, mode='r') as csvFile:
        print("-----> Reading file started ...")
        csvReader = csv.reader(csvFile, delimiter=';')
        print("-----> Reading file finished!")
        print("-----> Reading file into list started ...")
        awsEndpointsList = list(csvReader)
        print("-----> Reading file into list finished!")
    csvFile.close()

#    print(awsEndpointsList)

    print("---> AWS endpoints successfully retrieved")

#################################################################
#
#    Fetching Azure endpoints in the list
#
#################################################################

    print("\n---> Fetching Azure endpoints information into list")

    print("---> Opening file {0} for reading".format(AZURE_ENDPOINTS_FILE))

    with open(AZURE_ENDPOINTS_FILE, mode='r') as csvFile:
        print("-----> Reading file started ...")
        csvReader = csv.reader(csvFile, delimiter=';')
        print("-----> Reading file finished!")
        print("-----> Reading file into list started ...")
        azureEndpointsList = list(csvReader)
        print("-----> Reading file into list finished!")
    csvFile.close()

#    print(awsEndpointsList)

    print("---> Azure endpoints successfully retrieved")


#################################################################
#
#    Finding candidates to delete
#
#################################################################

    assetsDeletionList = []
    for currentEndpoint in awsEndpointsList:

        for tenableAsset in assetsData:

            try:
                for ipv4 in tenableAsset['ipv4s']:
                    if ipv4 == currentEndpoint[2] and tenableAsset['aws_ec2_instance_id'].lower() != currentEndpoint[4].lower():

                        for source in tenableAsset['sources']:
                            if source['name'] == "AWS-script":
                                asset_lastFoundDate = datetime.strptime(source['last_seen'], '%Y-%m-%dT%H:%M:%S.%fZ')

                                differenceInDates = startDateTime - asset_lastFoundDate
                                differenceInMinutes = differenceInDates.total_seconds() / 60
                                if differenceInMinutes >= ASSET_LAST_SEEN_MAX_MINUTES:
                                    deletionSet = { "field": "host.id", "operator": "eq", "value": tenableAsset['id'] }
                                    assetsDeletionList.append(deletionSet)
                                #print(differenceInMinutes)

                        #print(currentEndpoint)
                        #print(tenableAsset)
                        #print("\n\n------------------------------------\n\n")
            except Exception as e:
                raise e


    for currentEndpoint in azureEndpointsList:

        for tenableAsset in assetsData:

            try:
                for ipv4 in tenableAsset['ipv4s']:
                    if ipv4 == currentEndpoint[2] and tenableAsset['aws_ec2_instance_id'].lower() != currentEndpoint[4].lower():

                        for source in tenableAsset['sources']:
                            if source['name'] == "AZURE-script":
                                asset_lastFoundDate = datetime.strptime(source['last_seen'], '%Y-%m-%dT%H:%M:%S.%fZ')

                                differenceInDates = startDateTime - asset_lastFoundDate
                                differenceInMinutes = differenceInDates.total_seconds() / 60
                                if differenceInMinutes >= ASSET_LAST_SEEN_MAX_MINUTES:
                                    deletionSet = { "field": "host.id", "operator": "eq", "value": tenableAsset['id'] }
                                    assetsDeletionList.append(deletionSet)
                                #print(differenceInMinutes)

                        #print(currentEndpoint)
                        #print(tenableAsset)
                        #print("\n\n------------------------------------\n\n")
            except Exception as e:
                raise e

    #print("\n\n^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n\n")
    #print(assetsDeletionList)

#############################################################
#
# deleting assets from Tenable
#
#############################################################

    print("\n---> Deleting assets from Tenable")

    print("------>Assets to be deleted payload:")
    print(assetsDeletionList)
    print(len(assetsDeletionList))

    if len(assetsDeletionList) != 0:
        apiUrl = "https://cloud.tenable.com/api/v2/assets/bulk-jobs/delete"

        payload = {"query": { "or": assetsDeletionList}, "hard_delete": True }

        headers = {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "X-ApiKeys": TENABLE_API_KEY
        }

        response = requests.request("POST", apiUrl, json=payload, headers=headers)

        if response.status_code != 202:
            errorText="Error occured while deleting assets from Tenable (API return code {0})".format(response.text)
            raise Exception(errorText)

        assetsDeleteResponse=json.loads(response.text)

        print("---> Deleting assets from Tenable successfully completed. Total of {0} assets deleted.".format(assetsDeleteResponse['response']['data']['asset_count']))

    else:
        print("---> Deleting assets skipped since no assets found for deletion.")


except Exception as e:
    ERROR_MESSAGE = str(e)
    print(str(e))

finally:

#############################################################
#
# sending SNS notification
#
#############################################################

    #sending notification that processing went well
    print("\n---> Send notification to SNS topic: {0}".format(TOPIC_ARN))

    sns = boto3.client('sns')

    if ERROR_MESSAGE != None:
        messageToSend = "Detection and deletion of non-existing ephemeral Tenable assets had errors. Check logs for more information."
        subjectToSend = "{0} - Tenable - detection and deletion of non-existing ephemeral assets - error".format(startDateTime.strftime("%Y-%m-%d"))
    else:
        messageToSend = "Detection and deletion of non-existing ephemeral Tenable assets successfully finished."
        subjectToSend = "{0} - Tenable - detection and deletion of non-existing ephemeral assets - success".format(startDateTime.strftime("%Y-%m-%d"))

    sns.publish(TopicArn=TOPIC_ARN,
        Message = messageToSend,
        Subject = subjectToSend)

    print("---> SNS notification successfully sent!")

#finishing execution of the script
endDateTime = datetime.today()
scriptExecutionTime = endDateTime - startDateTime

print("\n---> Script execution ended @ {0}".format(endDateTime))
print("\n---> Total execution time: {0}\n".format(scriptExecutionTime))
