#!/bin/bash

#Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
#the European Commission - subsequent versions of the EUPL (the "Licence");
#You may not use this work except in compliance with the Licence.
#You may obtain a copy of the Licence at:
#
#   https://joinup.ec.europa.eu/software/page/eupl
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the Licence is distributed on an "AS IS" basis,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the Licence for the specific language governing permissions and
#limitations under the Licence.
 
# set variables to be used
timestamp=$(date '+%y%m%d%H%M')
timestamp_start=$(date)

script_to_run="/home/ubuntu/scripts/extract_awsIpFqdn_and_process_csv.py"
script_to_run_log="/home/ubuntu/logs/extract_awsIpFqdn_and_process_csv.py.log"
script_to_run_error_log="/home/ubuntu/logs/extract_awsIpFqdn_and_process_csv.py.error.log"
script_to_run_account_limit=99999
output_file_directory="/home/ubuntu/outputs/"
output_filename="aws-public_ip_address.csv"
output_filename_previous_iteration="aws-public_ip_address.csv.previous_iteration"
output_file=$(echo ${output_file_directory}${output_filename})
output_file_previous_iteration=$(echo ${output_file_directory}${output_filename_previous_iteration})
output_file_tmp=$(echo ${output_file_directory}${output_filename}.tmp)
output_file_backup="/home/ubuntu/outputs/backups/aws-public_ip_address.csv.$timestamp"
output_file_error_log="/home/ubuntu/logs/extract_awsIpFqdn_and_process_csv.sh.error.log"
output_temp_directory="/home/ubuntu/outputs/aws-external-endpoint-extract-temp/"
output_file_history="/home/ubuntu/outputs/aws-public_ip_address.csv.history"
output_file_history_tmp="/home/ubuntu/outputs/aws-public_ip_address.csv.history.tmp"

SNS_ARN="arn:aws:sns:eu-central-1:042034201921:aws-public-endpoints-extraction"

echo "Script execution started @ $timestamp_start"
echo "-------------------------------------------"

[ -f $output_file ] && cp $output_file $output_file_backup && cp "$output_file" "$output_file_previous_iteration" && rm $output_file
[ -f $output_file ] || touch $output_file

#calling the collection script
$(/usr/bin/python3 $script_to_run $script_to_run_account_limit $output_temp_directory $output_file_directory $output_filename > $script_to_run_log 2>$script_to_run_error_log)

#cleaning up output file
echo ""
echo "Sorting and making unique lines in the output file ..."
cat "$output_file" | sed "s/*.;*.;;;.*$//g" | sed "s/*.;*.;;.*\.local\.;.*//g" | sed "s/*.;*.;;.*\.internal\.;.*//g" | sed "s/*.;*.;;.*local\.;.*//g" | sed "s/\.;/;/g" | sort | uniq | grep . >> $output_file_tmp
mv "$output_file_tmp" "$output_file"


#creating history file
echo ""
echo "Creating history file ..."
cat "$output_file" >> "$output_file_history"
cat "$output_file_history" | sort | uniq > "$output_file_history_tmp"
cp "$output_file_history_tmp" "$output_file_history"
rm "$output_file_history_tmp"



echo ""
echo "Storing results to S3 bucket ..."
aws s3 cp "$output_file" s3://ist-vm/exports/
aws s3 cp "$output_file_history" s3://ist-vm/exports/
aws s3 cp "$output_file_history" s3://s2-share/exports/
aws s3 cp "$output_file_previous_iteration" s3://ist-vm/exports/

echo ""
echo "Sending SNS notification ..."
timestamp_end=$(date '+%Y-%m-%d')

#checking existance of errors for both error files (execution of this wrapper script and extraction script)
errorFileSize=$(wc -c "$output_file_error_log" | awk '{print $1}') 
[[ ! $errorFileSize -eq 0 ]] && errorLog="Errors OCCURED during extraction. Please check logs." || errorLog="No issues found during extraction."

SNS_message=$(echo "AWS public endpoint extraction finished. $errorLog")
SNS_subject=$(echo "$timestamp_end - AWS public endpoint extraction - finished")
executeResult=$(aws sns publish --topic-arn "$SNS_ARN" --subject "$SNS_subject" --message "$SNS_message")

#finishing script
timestamp_end=$(date)
echo ""
echo "Script execution ended @ $timestamp_end"
