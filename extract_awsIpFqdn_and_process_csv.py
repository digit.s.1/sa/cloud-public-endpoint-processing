#Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
#the European Commission - subsequent versions of the EUPL (the "Licence");
#You may not use this work except in compliance with the Licence.
#You may obtain a copy of the Licence at:
#
#   https://joinup.ec.europa.eu/software/page/eupl
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the Licence is distributed on an "AS IS" basis,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the Licence for the specific language governing permissions and
#limitations under the Licence.

import boto3
import csv
import subprocess
import sys
import threading
import os
import glob
import socket
from pprint import pprint
from datetime import datetime
from time import sleep

#defining throttling down period globally
THROTTLE_DOWN_PERIOD = 0.01

def getEC2InstanceData(fCredentials, fRegion, fAccountId, fAccountName):
    print("{0}> Getting EC2 instance data in region ({1}) started ...".format(fAccountId, fRegion))

    if fCredentials == None:
        ec2 = boto3.client('ec2', region_name=fRegion)
    else:
        ec2 = boto3.client('ec2', region_name=fRegion, aws_access_key_id = fCredentials['AccessKeyId'], aws_secret_access_key = fCredentials['SecretAccessKey'], aws_session_token = fCredentials['SessionToken'])

    try:
        ec2Instances = ec2.describe_instances()
        with open('{0}{1}-{2}-ec2.csv'.format(TEMP_FOLDER, fAccountId, fRegion), 'w') as fileHandle:
            for ec2Reservations in ec2Instances['Reservations']:
                for instance in ec2Reservations['Instances']:
                    try:
                        publicIpAddress = instance['PublicIpAddress']
                    except:
                        publicIpAddress = ""

                    #2022-03-18 decided to extract ip adresses only
                    #try:
                    #    publicDnsName = instance['PublicDnsName']
                    #except:
                    #    publicDnsName = ""
                    publicDnsName = ""

                    #2022-03-18 decided to extract ip adresses only
                    #try to fetch reverse DNS resolving for the given IP to add additional DNS record to this asset
                    #try:
                    #    if publicIpAddress != "":
                    #        publicDnsNameAdditional = socket.getnameinfo((publicIpAddress, 0), 0)[0]
                    #    else:
                    #        publicDnsNameAdditional = ""
                    #except BaseException as err:
                    #    publicDnsNameAdditional = ""
                    #    print("{0}> Unexpected ERROR EC2: type:{1} content:{2}".format(fAccountId, type(err), err))

                    if publicIpAddress != "" or publicDnsName != "":
                        fileHandle.write("{0};{1};{2};{3};{4}\n".format(fAccountId, fAccountName, publicIpAddress, publicDnsName, instance['InstanceId']))
                    #2022-03-18 decided to extract ip adresses only
                    #adding also additionally found DNS record found via reverse DNS call
                    #if publicDnsNameAdditional != "":
                    #    fileHandle.write("{0};{1};{2};{3};{4}\n".format(fAccountId, fAccountName, publicIpAddress, publicDnsNameAdditional, instance['InstanceId']))


            fileHandle.close()

    except BaseException as err:
        print("{0}> Unexpected ERROR EC2: type:{1} content:{2}".format(fAccountId, type(err), err))

    print("{0}> Getting EC2 instance data in region ({1}) finished!".format(fAccountId, fRegion))

def getELBInstanceData(fCredentials, fRegion, fAccountId, fAccountName):
    print("{0}> Getting ELB instance data in region ({1}) started ...".format(fAccountId, fRegion))

    if fCredentials == None:
        elb = boto3.client('elb', region_name=fRegion)
    else:
        elb = boto3.client('elb', region_name=fRegion, aws_access_key_id = fCredentials['AccessKeyId'], aws_secret_access_key = fCredentials['SecretAccessKey'], aws_session_token = fCredentials['SessionToken'])

    try:
        elbInstances = elb.describe_load_balancers()
        with open('{0}{1}-{2}-elb.csv'.format(TEMP_FOLDER, fAccountId, fRegion), 'w') as fileHandle:
            for instance in elbInstances['LoadBalancerDescriptions']:

                #skipping instances which are internal
                if instance['Scheme'] == "internal": continue

                try:
                    publicIpAddress = instance['PublicIpAddress']
                except:
                    publicIpAddress = ""

                try:
                    publicDnsName = instance['DNSName']
                except:
                    publicDnsName = ""

                if publicIpAddress != "" or publicDnsName != "":
                    fileHandle.write("{0};{1};{2};{3};{4}\n".format(fAccountId, fAccountName, publicIpAddress, publicDnsName, fAccountId + "/" + instance['DNSName']))
            fileHandle.close()

    except BaseException as err:
        print("{0}> Unexpected ERROR ELB: type:{1} content:{2}".format(fAccountId, type(err), err))

    print("{0}> Getting ELB instance data in region ({1}) finished!".format(fAccountId, fRegion))

def getELBv2InstanceData(fCredentials, fRegion, fAccountId, fAccountName):
    print("{0}> Getting ELBv2 instance data in region ({1}) started ...".format(fAccountId, fRegion))

    if fCredentials == None:
        elb = boto3.client('elbv2', region_name=fRegion)
    else:
        elb = boto3.client('elbv2', region_name=fRegion, aws_access_key_id = fCredentials['AccessKeyId'], aws_secret_access_key = fCredentials['SecretAccessKey'], aws_session_token = fCredentials['SessionToken'])

    try:
        elbInstances = elb.describe_load_balancers()
        with open('{0}{1}-{2}-elbv2.csv'.format(TEMP_FOLDER, fAccountId, fRegion), 'w') as fileHandle:
            for instance in elbInstances['LoadBalancers']:
                recordFound = 0
                publicIpAddress = ""
                publicDnsName = ""

                #skipping instances which are internal
                if instance['Scheme'] == "internal": continue

                for availabilityZone in instance['AvailabilityZones']:
                    for loadBalancerAddress in availabilityZone['LoadBalancerAddresses']:
                        try:
                            publicIpAddress = loadBalancerAddress['IpAddress']
                        except:
                            publicIpAddress = ""

                        try:
                            publicDnsName = instance['DNSName']
                        except:
                            publicDnsName = ""

                        if publicIpAddress != "" or publicDnsName != "":
                            fileHandle.write("{0};{1};{2};{3};{4}\n".format(fAccountId, fAccountName, publicIpAddress, publicDnsName, instance['LoadBalancerArn']))
                            recordFound = 1
                if recordFound == 0: 
                    try:
                        publicDnsName = instance['DNSName']
                    except:
                        publicDnsName = ""

                    fileHandle.write("{0};{1};{2};{3};{4}\n".format(fAccountId, fAccountName, publicIpAddress, publicDnsName, instance['LoadBalancerArn']))

            fileHandle.close()

    except BaseException as err:
        print("{0}> Unexpected ERROR ELBv2: type:{1} content:{2}".format(fAccountId, type(err), err))

    print("{0}> Getting ELBv2 instance data in region ({1}) finished!".format(fAccountId, fRegion))


def getRDSClusterInstanceData(fCredentials, fRegion, fAccountId, fAccountName):
    print("{0}> Getting RDS Cluster data in region ({1}) started ...".format(fAccountId, fRegion))

    if fCredentials == None:
        rds = boto3.client('rds', region_name=fRegion)
    else:
        rds = boto3.client('rds', region_name=fRegion, aws_access_key_id = fCredentials['AccessKeyId'], aws_secret_access_key = fCredentials['SecretAccessKey'], aws_session_token = fCredentials['SessionToken'])

    try:
        rdsInstances = rds.describe_db_clusters()
        with open('{0}{1}-{2}-rds-cluster.csv'.format(TEMP_FOLDER, fAccountId, fRegion), 'w') as fileHandle:
            for instance in rdsInstances['DBClusters']:
                recordFound = 0
                publicIpAddress = ""
                publicDnsName = ""
                try:
                    publicIpAddress = instance['Endpoint']
                except:
                    publicIpAddress = ""
                try:
                    publicDnsName = instance['ReaderEndpoint']
                except:
                    publicDnsName = ""

                #in RDS cluster case both endpoint and reader endpoint are FQDNs
                if publicIpAddress != "":
                    fileHandle.write("{0};{1};;{2};{3}\n".format(fAccountId, fAccountName, publicIpAddress, instance['DBClusterArn']))
                if publicDnsName != "":
                    fileHandle.write("{0};{1};;{2};{3}\n".format(fAccountId, fAccountName, publicDnsName, instance['DBClusterArn']))

            fileHandle.close()

    except BaseException as err:
        print("{0}> Unexpected ERROR RDS cluster: type:{1} content:{2}".format(fAccountId, type(err), err))

    print("{0}> Getting RDS cluster data in region ({1}) finished!".format(fAccountId, fRegion))

def getRDSInstanceData(fCredentials, fRegion, fAccountId, fAccountName):
    print("{0}> Getting RDS instance data in region ({1}) started ...".format(fAccountId, fRegion))

    if fCredentials == None:
        rds = boto3.client('rds', region_name=fRegion)
    else:
        rds = boto3.client('rds', region_name=fRegion, aws_access_key_id = fCredentials['AccessKeyId'], aws_secret_access_key = fCredentials['SecretAccessKey'], aws_session_token = fCredentials['SessionToken'])

    try:
        rdsInstances = rds.describe_db_instances()
        with open('{0}{1}-{2}-rds-instance.csv'.format(TEMP_FOLDER, fAccountId, fRegion), 'w') as fileHandle:
            for instance in rdsInstances['DBInstances']:

                #skipping non public instances
                if instance['PubliclyAccessible'] != True: continue
                publicIpAddress = ""
                publicDnsName = ""
                try:
                    publicIpAddress = ""
                except:
                    publicIpAddress = ""
                try:
                    publicDnsName = instance['Endpoint']['Address']
                except:
                    publicDnsName = ""
                if publicIpAddress != "" or publicDnsName != "":
                    fileHandle.write("{0};{1};{2};{3};{4}\n".format(fAccountId, fAccountName, publicIpAddress, publicDnsName, instance['DBInstanceArn']))

            fileHandle.close()

    except BaseException as err:
        print("{0}> Unexpected ERROR RDS Instance: type:{1} content:{2}".format(fAccountId, type(err), err))

    print("{0}> Getting RDS instance data in region ({1}) finished!".format(fAccountId, fRegion))

def getRedshiftInstanceData(fCredentials, fRegion, fAccountId, fAccountName):
    print("{0}> Getting Redshift data in region ({1}) started ...".format(fAccountId, fRegion))

    if fCredentials == None:
        redshift = boto3.client('redshift', region_name=fRegion)
    else:
        redshift = boto3.client('redshift', region_name=fRegion, aws_access_key_id = fCredentials['AccessKeyId'], aws_secret_access_key = fCredentials['SecretAccessKey'], aws_session_token = fCredentials['SessionToken'])

    redshiftInstances = redshift.describe_clusters()

    try:
        with open('{0}{1}-{2}-redshift.csv'.format(TEMP_FOLDER, fAccountId, fRegion), 'w') as fileHandle:
            for instance in redshiftInstances['Clusters']:

                #skip instances which are not public
                if instance['PubliclyAccessible'] != True: continue

                publicIpAddress = ""
                publicDnsName = ""
                try:
                    publicIpAddress = ""
                except:
                    publicIpAddress = ""
                try:
                    publicDnsName = instance['Endpoint']['Address']
                except:
                    publicDnsName = ""

                if publicIpAddress != "" or publicDnsName != "":
                    fileHandle.write("{0};{1};{2};{3};{4}\n".format(fAccountId, fAccountName, publicIpAddress, publicDnsName, fAccountId + "/" + instance['Endpoint']['Address']))

            fileHandle.close()

    except BaseException as err:
        print("{0}> Unexpected ERROR Redshift: type:{1} content:{2}".format(fAccountId, type(err), err))

    print("{0}> Getting Redshift data in region ({1}) finished!".format(fAccountId, fRegion))

def getCloudfrontInstanceData(fCredentials, fRegion, fAccountId, fAccountName):
    print("{0}> Getting Cloudfront data started ...".format(fAccountId))

    if fCredentials == None:
        cloudfront = boto3.client('cloudfront')
    else:
        cloudfront = boto3.client('cloudfront', aws_access_key_id = fCredentials['AccessKeyId'], aws_secret_access_key = fCredentials['SecretAccessKey'], aws_session_token = fCredentials['SessionToken'])

    try:
        cloudfrontInstances = cloudfront.list_distributions()
        if "Items" not in cloudfrontInstances['DistributionList'].keys(): return

        with open('{0}{1}-all-cloudfront.csv'.format(TEMP_FOLDER, fAccountId), 'w') as fileHandle:
            for instance in cloudfrontInstances['DistributionList']['Items']:
 #               print(instance)
                publicIpAddress = ""
                publicDnsName = ""

                if "AliasICPRecordals" in instance.keys():
                    for aliases in instance['AliasICPRecordals']:
                        try:
                            publicIpAddress = ""
                        except:
                            publicIpAddress = ""

                        try:
                            publicDnsName = aliases['CNAME']
                        except:
                            publicDnsName = ""


                        #try to fetch IP adress from the extracted DNS entry
                        try:
                            if publicDnsName != "" and ("*" not in publicDnsName):
                                publicIpAddress = socket.gethostbyname(publicDnsName)
                            else:
                                publicIpAddress = ""
                        except:
                            publicIpAddress = ""

                        #try to fetch reverse DNS resolving for the given IP to add additional DNS record to this asset
                        try:
                            if publicIpAddress != "":
                                publicDnsNameAdditional = socket.getnameinfo((publicIpAddress, 0), 0)[0]
                            else:
                                publicDnsNameAdditional = ""
                        except BaseException as err:
                            publicDnsNameAdditional = ""
                            print("{0}> Unexpected ERROR CloudFront - AliasICPRecordals: type:{1} content:{2}".format(fAccountId, type(err), err))


                        if (publicIpAddress != "" or publicDnsName != "") and ("*" not in publicDnsName):
                            fileHandle.write("{0};{1};{2};{3};{4}\n".format(fAccountId, fAccountName, publicIpAddress, publicDnsName, instance['ARN'] + '/' + publicDnsName))
                        #adding also additionally found DNS record found via reverse DNS call
                        if publicDnsNameAdditional != "":
                            fileHandle.write("{0};{1};{2};{3};{4}\n".format(fAccountId, fAccountName, publicIpAddress, publicDnsNameAdditional, instance['ARN'] + '/' + publicDnsName))


                if "Origins" in instance.keys():
                    for origins in instance['Origins']['Items']:
                        try:
                            publicIpAddress = ""
                        except:
                            publicIpAddress = ""

                        try:
                            publicDnsName = origins['DomainName']
                        except:
                            publicDnsName = ""

                        #try to fetch IP adress from the extracted DNS entry
                        try:
                            if publicDnsName != "" and ("*" not in publicDnsName):
                                publicIpAddress = socket.gethostbyname(publicDnsName)
                            else:
                                publicIpAddress = ""
                        except:
                            publicIpAddress = ""

                        #try to fetch reverse DNS resolving for the given IP to add additional DNS record to this asset
                        try:
                            if publicIpAddress != "":
                                publicDnsNameAdditional = socket.getnameinfo((publicIpAddress, 0), 0)[0]
                            else:
                                publicDnsNameAdditional = ""
                        except BaseException as err:
                            publicDnsNameAdditional = ""
                            print("{0}> Unexpected ERROR CloudFront - Origins: type:{1} content:{2}".format(fAccountId, type(err), err))


                        if (publicIpAddress != "" or publicDnsName != "") and ("*" not in publicDnsName):
                            fileHandle.write("{0};{1};{2};{3};{4}\n".format(fAccountId, fAccountName, publicIpAddress, publicDnsName, instance['ARN'] + '/' + publicDnsName))
                        #adding also additionally found DNS record found via reverse DNS call
                        if publicDnsNameAdditional != "":
                            fileHandle.write("{0};{1};{2};{3};{4}\n".format(fAccountId, fAccountName, publicIpAddress, publicDnsNameAdditional, instance['ARN'] + '/' + publicDnsName))

            fileHandle.close()

    except BaseException as err:
        print("{0}> Unexpected ERROR Cloudfront: type:{1} content:{2}".format(fAccountId, type(err), err))

    print("{0}> Getting Cloudfront data finished!".format(fAccountId))


def getRoute53InstanceData(fCredentials, fRegion, fAccountId, fAccountName):
    print("{0}> Getting Route53 instance data started ...".format(fAccountId))

    if fCredentials == None:
        route53 = boto3.client('route53')
    else:
        route53 = boto3.client('route53', aws_access_key_id = fCredentials['AccessKeyId'], aws_secret_access_key = fCredentials['SecretAccessKey'], aws_session_token = fCredentials['SessionToken'])

    try:
        route53Instances = route53.list_hosted_zones()
        with open('{0}{1}-all-route53.csv'.format(TEMP_FOLDER, fAccountId), 'w') as fileHandle:
            for instance in route53Instances['HostedZones']:
                if instance['Config']['PrivateZone'] == True: continue

                route53HostedZoneRecords = route53.list_resource_record_sets(HostedZoneId=instance['Id'])
                for record in route53HostedZoneRecords['ResourceRecordSets']:
                    if record['Type'] not in ('A', 'CNAME'): continue

                    publicDnsName = ""

                    try:
                        if len(record['Name']) > 0:
                            #dns records transformation and filtering:
                            #transforming to lowercase
                            publicDnsName = record['Name'].lower()
                            #stripping off the last character (which is a dot .)
                            publicDnsName = publicDnsName[:-1]
                            #various filtering which we should avoid putting in output
                            if publicDnsName.find("in-addr.arpa") != -1: publicDnsName = ""
                            elif publicDnsName.find(".") == -1: publicDnsName = ""
                            elif publicDnsName == "local": publicDnsName = ""
                            elif publicDnsName.endswith(".int") == True: publicDnsName = ""
                            elif publicDnsName.endswith(".internal") == True: publicDnsName = ""
                            elif publicDnsName.endswith(".local") == True: publicDnsName = ""
                            elif publicDnsName.endswith(".prod") == True: publicDnsName = ""
                            elif publicDnsName.endswith(".test") == True: publicDnsName = ""
                            elif publicDnsName.endswith(".development") == True: publicDnsName = ""
                            elif publicDnsName.endswith(".mr") == True: publicDnsName = ""
                            else: None

                    except:
                        publicDnsName = ""

                    #try to fetch IP adress from the extracted DNS entry
                    try:
                        if publicDnsName != "":
                            publicIpAddress = socket.gethostbyname(publicDnsName)
                        else:
                            publicIpAddress = ""
                    except:
                        publicIpAddress = ""

                    #try to fetch reverse DNS resolving for the given IP to add additional DNS record to this asset
                    try:
                        if publicIpAddress != "":
                            publicDnsNameAdditional = socket.getnameinfo((publicIpAddress, 0), 0)[0]
                        else:
                            publicDnsNameAdditional = ""
                    except BaseException as err:
                        publicDnsNameAdditional = ""
                        print("{0}> Unexpected ERROR Route53: type:{1} content:{2}".format(fAccountId, type(err), err))

                    if publicIpAddress != "" or publicDnsName != "":
                        fileHandle.write("{0};{1};{2};{3};{4}\n".format(fAccountId, fAccountName, publicIpAddress, publicDnsName, instance['Id'] + '/' + record['Type'] + '/' + record['Name'][:-1]))
                    #adding also additionally found DNS record found via reverse DNS call
                    if publicDnsNameAdditional != "":
                        fileHandle.write("{0};{1};{2};{3};{4}\n".format(fAccountId, fAccountName, publicIpAddress, publicDnsNameAdditional, instance['Id'] + '/' + record['Type'] + '/' + record['Name'][:-1]))

            fileHandle.close()

    except BaseException as err:
        print("{0}> Unexpected ERROR Route53: type:{1} content:{2}".format(fAccountId, type(err), err))

    print("{0}> Getting Route53 instance data finished!".format(fAccountId))


def getElasticsearchInstanceData(fCredentials, fRegion, fAccountId, fAccountName):
    print("{0}> Getting Elasticsearch instance data started ...".format(fAccountId))

    if fCredentials == None:
        es = boto3.client('es')
    else:
        es = boto3.client('es', aws_access_key_id = fCredentials['AccessKeyId'], aws_secret_access_key = fCredentials['SecretAccessKey'], aws_session_token = fCredentials['SessionToken'])

    esDomainNames = es.list_domain_names()
    try:
        with open('{0}{1}-all-elasticsearch.csv'.format(TEMP_FOLDER, fAccountId), 'w') as fileHandle:
            for esDomain in esDomainNames['DomainNames']:
                esInstance = es.describe_elasticsearch_domain(DomainName=esDomain['DomainName'])
                publicIpAddress = ""
                publicDnsName = ""
                try:
                    publicIpAddress = ""
                except:
                    publicIpAddress = ""
                try:
                    publicDnsName = esInstance['DomainStatus']['Endpoint']
                except:
                    try:
                        publicDnsName = esInstance['DomainStatus']['Endpoints']['vpc']
                    except:
                        publicDnsName = ""

                if publicIpAddress != "" or publicDnsName != "":
                    fileHandle.write("{0};{1};{2};{3};{4}\n".format(fAccountId, fAccountName, publicIpAddress, publicDnsName, esInstance['DomainStatus']['ARN']))

            fileHandle.close()

    except BaseException as err:
        print("{0}> Unexpected ERROR ElasticSearch: type:{1} content:{2}".format(fAccountId, type(err), err))

    print("{0}> Getting Elasticsearch instance data finished!".format(fAccountId))


def getECSInstanceData(fCredentials, fRegion, fAccountId, fAccountName):
    print("{0}> Getting ECS instance data started ...".format(fAccountId))

    if fCredentials == None:
        ecs = boto3.client('ecs', region_name=fRegion)
    else:
        ecs = boto3.client('ecs', region_name=fRegion, aws_access_key_id = fCredentials['AccessKeyId'], aws_secret_access_key = fCredentials['SecretAccessKey'], aws_session_token = fCredentials['SessionToken'])

    try:
        ecsTasks = ecs.list_tasks()
        with open('{0}{1}-{2}-ecs.csv'.format(TEMP_FOLDER, fAccountId, fRegion), 'w') as fileHandle:
            ecsTaskDescriptions = ecs.describe_tasks(ecsTasks['taskArns'])
            for ecsTask in ecsTaskDescriptions['tasks']:
                for ecsContainer in ecsTask['containers']:
                    for ecsContainerNetworkbinding in ecsContainer['networkBindings']:
                        publicIpAddress = ""
                        publicDnsName = ""
                        try:
                            publicIpAddress = ""
                        except:
                            publicIpAddress = ""
                        try:
                            publicDnsName = ecsContainerNetworkbinding['bindIP']
                        except:
                            publicDnsName = ""

                        if publicIpAddress != "" or publicDnsName != "":
                            fileHandle.write("{0};{1};{2};{3};{4}\n".format(fAccountId, fAccountName, publicIpAddress, publicDnsName, ecsContainer['containerArn']))
        fileHandle.close()

    except BaseException as err:
        print("{0}> Unexpected ERROR ECS: type:{1} content:{2}".format(fAccountId, type(err), err))

    print("{0}> Getting ECS instance data finished!".format(fAccountId))


def getEKSInstanceData(fCredentials, fRegion, fAccountId, fAccountName):
    print("{0}> Getting EKS instance data started ...".format(fAccountId))

    if fCredentials == None:
        eks = boto3.client('eks', region_name=fRegion)
    else:
        eks = boto3.client('eks', region_name=fRegion, aws_access_key_id = fCredentials['AccessKeyId'], aws_secret_access_key = fCredentials['SecretAccessKey'], aws_session_token = fCredentials['SessionToken'])

    try:
        eksClusterNameList = eks.list_clusters()

        #skipping the whole process if there are no EKS clusters
        if len(eksClusterNameList['clusters']) == 0: 
            print("{0}> Getting EKS instance data finished!".format(fAccountId))
            return

        with open('{0}{1}-{2}-eks.csv'.format(TEMP_FOLDER, fAccountId, fRegion), 'w') as fileHandle:

            for eksClusterName in eksClusterNameList['clusters']:
                eksCluster = eks.describe_cluster(name = eksClusterName)
#                pprint(eksCluster)

                #we are recording EKS clusters with public endpoints only
                if eksCluster['cluster']['resourcesVpcConfig']['endpointPublicAccess'] != True: 
                    print("{0}> Cluster {1} is not opened for public access! Skipping ...".format(fAccountId, eksCluster['cluster']['arn']))
                    return

                publicIpAddress = ""
                publicDnsName = ""
                try:
                    publicIpAddress = ""
                except:
                    publicIpAddress = ""
                try:
                    publicDnsName = eksCluster['cluster']['endpoint']
                except:
                    publicDnsName = ""

                if publicIpAddress != "" or publicDnsName != "":
                    fileHandle.write("{0};{1};{2};{3};{4}\n".format(fAccountId, fAccountName, publicIpAddress, publicDnsName, eksCluster['cluster']['arn']))

            fileHandle.close()

    except BaseException as err:
        print("{0}> Unexpected ERROR EKS: type:{1} content:{2}".format(fAccountId, type(err), err))

    print("{0}> Getting EKS instance data finished!".format(fAccountId))


def getWorkspacesInstanceData(fCredentials, fRegion, fAccountId, fAccountName, fRegions):
    print("{0}> Getting Workspace instance data started ...".format(fAccountId))
    networkInterfaces = []
    workspacesInstances = []

    if fCredentials == None:
        for region in fRegions:
            try:
                workspaces = boto3.client('workspaces', region_name=region)
                workspacesInstancesTemp = workspaces.describe_workspaces()
                if len(workspacesInstancesTemp['Workspaces']) != 0:
                    workspacesInstances.extend(workspacesInstancesTemp['Workspaces'])
            except:
                print("{0}> Getting Workspaces instance data not supported for region {1}".format(fAccountId, region))

            ec2 = boto3.client('ec2', region_name=region)
            networkInterfacesTemp = ec2.describe_network_interfaces()
            networkInterfaces.extend(networkInterfacesTemp['NetworkInterfaces'])
    else:
        for region in fRegions:
            try:
                workspaces = boto3.client('workspaces', region_name=region, aws_access_key_id = fCredentials['AccessKeyId'], aws_secret_access_key = fCredentials['SecretAccessKey'], aws_session_token = fCredentials['SessionToken'])
                workspacesInstancesTemp = workspaces.describe_workspaces()
                if len(workspacesInstancesTemp['Workspaces']) != 0:
                    workspacesInstances.extend(workspacesInstancesTemp['Workspaces'])
            except:
                print("{0}> Getting Workspaces instance data not supported for region {1}".format(fAccountId, region))

            ec2 = boto3.client('ec2', region_name=region, aws_access_key_id = fCredentials['AccessKeyId'], aws_secret_access_key = fCredentials['SecretAccessKey'], aws_session_token = fCredentials['SessionToken'])
            networkInterfacesTemp = ec2.describe_network_interfaces()
            networkInterfaces.extend(networkInterfacesTemp['NetworkInterfaces'])

    try:

#        pprint(workspacesInstances)
#        pprint(networkInterfaces)

        with open('{0}{1}-{2}-workspaces.csv'.format(TEMP_FOLDER, fAccountId, fRegion), 'w') as fileHandle:

            for workspace in workspacesInstances:

                workspacePrivateIpAddress = workspace['IpAddress']

                for networkInterface in networkInterfaces:

                    publicIpAddress = ""
                    publicDnsName = ""

                    if len(networkInterface['PrivateIpAddresses']) != 0:
                        for privateIpAddress in networkInterface['PrivateIpAddresses']:
                            if privateIpAddress['PrivateIpAddress'] == workspacePrivateIpAddress:
                                try:
                                    publicIpAddress = privateIpAddress['Association']['PublicIp']
                                except:
                                    publicIpAddress = ""

                                break
                            else:
                                continue
                    else:
                        if networkInterface['PrivateIpAddress'] == workspacePrivateIpAddress:
                            try:
                                publicIpAddress = privateIpAddress['Association']['PublicIp']
                            except:
                                publicIpAddress = ""
                            break
                        else:
                            continue

                    workspaceCustomArn = "aws:workspaces:{0}:{1}:{2}".format(fAccountId, workspace['DirectoryId'], workspace['WorkspaceId'])

                    if publicIpAddress != "" or publicDnsName != "":
                        print("{0};{1};{2};{3};{4}\n".format(fAccountId, fAccountName, publicIpAddress, publicDnsName, workspaceCustomArn))
                        fileHandle.write("{0};{1};{2};{3};{4}\n".format(fAccountId, fAccountName, publicIpAddress, publicDnsName, workspaceCustomArn))

            fileHandle.close()

    except BaseException as err:
        print("{0}> Unexpected ERROR Workspaces: type:{1} content:{2}".format(fAccountId, type(err), err))

    print("{0}> Getting Workspaces data finished!".format(fAccountId))


def getOpeanSearchInstanceData(fCredentials, fRegion, fAccountId, fAccountName):
    print("{0}> Getting OpenSearch instance data started ...".format(fAccountId))

    if fCredentials == None:
        opensearch = boto3.client('opensearch', region_name=fRegion)
    else:
        opensearch = boto3.client('opensearch', region_name=fRegion, aws_access_key_id = fCredentials['AccessKeyId'], aws_secret_access_key = fCredentials['SecretAccessKey'], aws_session_token = fCredentials['SessionToken'])

    try:
        openSearchDomainList = opensearch.list_domain_names()

        #skipping the whole process if there are no EKS clusters
        #if len(eksClusterNameList['clusters']) == 0:
        #    print("{0}> Getting EKS instance data finished!".format(fAccountId))
        #    return

        with open('{0}{1}-{2}-opensearch.csv'.format(TEMP_FOLDER, fAccountId, fRegion), 'w') as fileHandle:

            for openSearchDomainName in openSearchDomainList['DomainName']:
                openSearchDomain = opensearch.describe_domain(DomainName=openSearchDomainName)

                pprint(openSearchDomain)
                break

                #we are recording EKS clusters with public endpoints only
                if eksCluster['cluster']['resourcesVpcConfig']['endpointPublicAccess'] != True:
                    print("{0}> Cluster {1} is not opened for public access! Skipping ...".format(fAccountId, eksCluster['cluster']['arn']))
                    return

                publicIpAddress = ""
                publicDnsName = ""
                try:
                    publicIpAddress = ""
                except:
                    publicIpAddress = ""
                try:
                    publicDnsName = eksCluster['cluster']['endpoint']
                except:
                    publicDnsName = ""

                if publicIpAddress != "" or publicDnsName != "":
                    fileHandle.write("{0};{1};{2};{3};{4}\n".format(fAccountId, fAccountName, publicIpAddress, publicDnsName, eksCluster['cluster']['arn']))

            fileHandle.close()

    except BaseException as err:
        print("{0}> Unexpected ERROR OpenSearch: type:{1} content:{2}".format(fAccountId, type(err), err))

    print("{0}> Getting OpenSearch instance data finished!".format(fAccountId))


class EC2Thread (threading.Thread):
    def __init__(self, fCredentials, fRegion, fAccountId, fAccountName):
        threading.Thread.__init__(self)
        self.fCredentials = fCredentials
        self.fRegion = fRegion
        self.fAccountId = fAccountId
        self.fAccountName = fAccountName
    def run(self):
        getEC2InstanceData(self.fCredentials, self.fRegion, self.fAccountId, self.fAccountName)

class ELBThread (threading.Thread):
    def __init__(self, fCredentials, fRegion, fAccountId, fAccountName):
        threading.Thread.__init__(self)
        self.fCredentials = fCredentials
        self.fRegion = fRegion
        self.fAccountId = fAccountId
        self.fAccountName = fAccountName
    def run(self):
        getELBInstanceData(self.fCredentials, self.fRegion, self.fAccountId, self.fAccountName)

class ELBv2Thread (threading.Thread):
    def __init__(self, fCredentials, fRegion, fAccountId, fAccountName):
        threading.Thread.__init__(self)
        self.fCredentials = fCredentials
        self.fRegion = fRegion
        self.fAccountId = fAccountId
        self.fAccountName = fAccountName
    def run(self):
        getELBv2InstanceData(self.fCredentials, self.fRegion, self.fAccountId, self.fAccountName)

class RDSClusterThread (threading.Thread):
    def __init__(self, fCredentials, fRegion, fAccountId, fAccountName):
        threading.Thread.__init__(self)
        self.fCredentials = fCredentials
        self.fRegion = fRegion
        self.fAccountId = fAccountId
        self.fAccountName = fAccountName
    def run(self):
        getRDSClusterInstanceData(self.fCredentials, self.fRegion, self.fAccountId, self.fAccountName)

class RDSInstanceThread (threading.Thread):
    def __init__(self, fCredentials, fRegion, fAccountId, fAccountName):
        threading.Thread.__init__(self)
        self.fCredentials = fCredentials
        self.fRegion = fRegion
        self.fAccountId = fAccountId
        self.fAccountName = fAccountName
    def run(self):
        getRDSInstanceData(self.fCredentials, self.fRegion, self.fAccountId, self.fAccountName)

class RedshiftThread (threading.Thread):
    def __init__(self, fCredentials, fRegion, fAccountId, fAccountName):
        threading.Thread.__init__(self)
        self.fCredentials = fCredentials
        self.fRegion = fRegion
        self.fAccountId = fAccountId
        self.fAccountName = fAccountName
    def run(self):
        getRedshiftInstanceData(self.fCredentials, self.fRegion, self.fAccountId, self.fAccountName)

class CloudfrontThread (threading.Thread):
    def __init__(self, fCredentials, fRegion, fAccountId, fAccountName):
        threading.Thread.__init__(self)
        self.fCredentials = fCredentials
        self.fRegion = fRegion
        self.fAccountId = fAccountId
        self.fAccountName = fAccountName
    def run(self):
        getCloudfrontInstanceData(self.fCredentials, self.fRegion, self.fAccountId, self.fAccountName)


class Route53Thread (threading.Thread):
    def __init__(self, fCredentials, fRegion, fAccountId, fAccountName):
        threading.Thread.__init__(self)
        self.fCredentials = fCredentials
        self.fRegion = fRegion
        self.fAccountId = fAccountId
        self.fAccountName = fAccountName
    def run(self):
        getRoute53InstanceData(self.fCredentials, self.fRegion, self.fAccountId, self.fAccountName)

class ElasticsearchThread (threading.Thread):
    def __init__(self, fCredentials, fRegion, fAccountId, fAccountName):
        threading.Thread.__init__(self)
        self.fCredentials = fCredentials
        self.fRegion = fRegion
        self.fAccountId = fAccountId
        self.fAccountName = fAccountName
    def run(self):
        getElasticsearchInstanceData(self.fCredentials, self.fRegion, self.fAccountId, self.fAccountName)

class ECSThread (threading.Thread):
    def __init__(self, fCredentials, fRegion, fAccountId, fAccountName):
        threading.Thread.__init__(self)
        self.fCredentials = fCredentials
        self.fRegion = fRegion
        self.fAccountId = fAccountId
        self.fAccountName = fAccountName
    def run(self):
        getECSInstanceData(self.fCredentials, self.fRegion, self.fAccountId, self.fAccountName)

class EKSThread (threading.Thread):
    def __init__(self, fCredentials, fRegion, fAccountId, fAccountName):
        threading.Thread.__init__(self)
        self.fCredentials = fCredentials
        self.fRegion = fRegion
        self.fAccountId = fAccountId
        self.fAccountName = fAccountName
    def run(self):
        getEKSInstanceData(self.fCredentials, self.fRegion, self.fAccountId, self.fAccountName)


class WorkspacesThread (threading.Thread):
    def __init__(self, fCredentials, fRegion, fAccountId, fAccountName, fRegions):
        threading.Thread.__init__(self)
        self.fCredentials = fCredentials
        self.fRegion = fRegion
        self.fAccountId = fAccountId
        self.fAccountName = fAccountName
        self.fRegions = fRegions
    def run(self):
        getWorkspacesInstanceData(self.fCredentials, self.fRegion, self.fAccountId, self.fAccountName, self.fRegions)

class OpenSearchThread (threading.Thread):
    def __init__(self, fCredentials, fRegion, fAccountId, fAccountName):
        threading.Thread.__init__(self)
        self.fCredentials = fCredentials
        self.fRegion = fRegion
        self.fAccountId = fAccountId
        self.fAccountName = fAccountName
    def run(self):
        getOpenSearchInstanceData(self.fCredentials, self.fRegion, self.fAccountId, self.fAccountName)

def extractAccountInfo(fAccountId, fAccountName, fThreadList, fCallerAccount):

    global THROTTLE_DOWN_PERIOD

    print("{0}> Account ({1}) processing started ...".format(fAccountId, fAccountName))
    print("{0}> Assuming role".format(fAccountId))

    stsClient = boto3.client('sts')

    # Call the assume_role method of the STSConnection object and pass the role
    # ARN and a role session name.
    try:
        if fAccountId != fCallerAccount:
            assumedRoleObject=stsClient.assume_role(
                RoleArn="arn:aws:iam::{0}:role/SecurityAccessReadOnlyRole".format(fAccountId),
                RoleSessionName="getAccountDataSession-{0}".format(fAccountId)
            )
    except:
        print("{0}> Not authorised to assume the role. Exiting!".format(fAccountId))
        return

    if fAccountId == fCallerAccount:
        credentials = None
    else:
        credentials = assumedRoleObject['Credentials']

    print("{0}> Getting active region data".format(fAccountId))

    if credentials == None:
        ec2 = boto3.client('ec2')
    else:
        ec2 = boto3.client('ec2', aws_access_key_id = credentials['AccessKeyId'], aws_secret_access_key = credentials['SecretAccessKey'], aws_session_token = credentials['SessionToken'])

    regions = [region['RegionName'] for region in ec2.describe_regions()['Regions'] if region['OptInStatus'] == 'opt-in-not-required']

    #initatedThreads = []
    for region in regions:
        #getEC2InstanceData(credentials, region, fAccountId, fAccountName)

        #VALIDATED
        newThread = EC2Thread(credentials, region, fAccountId, fAccountName)
        fThreadList.append(newThread)
        newThread.start()

        #VALIDATED
        #2022-03-17 - decided to put out of scope
        #newThread = ELBThread(credentials, region, fAccountId, fAccountName)
        #fThreadList.append(newThread)
        #newThread.start()

        #VALIDATED
        #2022-03-17 - decided to put out of scope
        #newThread = ELBv2Thread(credentials, region, fAccountId, fAccountName)
        #fThreadList.append(newThread)
        #newThread.start()

        #VALIDATED but PaaS - not scanning
        #newThread = RDSClusterThread(credentials, region, fAccountId, fAccountName)
        #fThreadList.append(newThread)
        #newThread.start()

        #VALIDATED but PaaS - not scanning
        #newThread = RDSInstanceThread(credentials, region, fAccountId, fAccountName)
        #fThreadList.append(newThread)
        #newThread.start()

        #VALIDATED but PaaS - not scanning
        #newThread = RedshiftThread(credentials, region, fAccountId, fAccountName)
        #fThreadList.append(newThread)
        #newThread.start()

        #VALIDATED
        #2022-03-17 - decided to put out of scope
        #newThread = ECSThread(credentials, region, fAccountId, fAccountName)
        #fThreadList.append(newThread)
        #newThread.start()

        #VALIDATED
        #2022-03-17 - decided to put out of scope
        #newThread = EKSThread(credentials, region, fAccountId, fAccountName)
        #fThreadList.append(newThread)
        #newThread.start()

        #NOT VALIDATED
#        newThread = OpenSearchThread(credentials, region, fAccountId, fAccountName)
#        fThreadList.append(newThread)
#        newThread.start()

        sleep(THROTTLE_DOWN_PERIOD)

    #VALIDATED
    #workspaces is region dependant - but due to specific nature of extracting enpoint region loops are done internally in the function, therefore allregions are passed by as last argument
    newThread = WorkspacesThread(credentials, "", fAccountId, fAccountName, regions)
    fThreadList.append(newThread)
    newThread.start()

    #VALIDATED but PaaS - not scanning
    #elasticSearch is region indenpendant
    #newThread = ElasticsearchThread(credentials, "", fAccountId, fAccountName)
    #fThreadList.append(newThread)
    #newThread.start()

    #VALIDATED
    #cloudfront is region independant
    #2022-03-17 - decided to put out of scope
    #newThread = CloudfrontThread(credentials, "", fAccountId, fAccountName)
    #fThreadList.append(newThread)
    #newThread.start()

    #route53 is region independant
    #2022-03-17 - decided to put out of scope
    #newThread = Route53Thread(credentials, "", fAccountId, fAccountName)
    #fThreadList.append(newThread)
    #newThread.start()

    print("{0}> Account ({1}) processing finished!".format(fAccountId, fAccountName))

class AccountsThread (threading.Thread):
    def __init__(self, fAccountId, fAccountName, fThreadList, fCallerAccount):
        threading.Thread.__init__(self)
        self.fAccountId = fAccountId
        self.fAccountName = fAccountName
        self.fThreadList = fThreadList
        self.fCallerAccount = fCallerAccount
    def run(self):
        extractAccountInfo(self.fAccountId, self.fAccountName, self.fThreadList, self.fCallerAccount)


##########################################################
##########################################################
#
#      MAIN CLASS
#
##########################################################
##########################################################

#checking input arguments
print("\n---> Checking input arguments")
if len(sys.argv) < 5:
    print("\n---> No sufficient arguments provided (5). Exiting.")
    sys.exit()
else:
    ACCOUNT_NUMBER_LIMIT = sys.argv[1]
    TEMP_FOLDER = sys.argv[2]
    DESTINATION_DIRECTORY = sys.argv[3]
    DESTINATION_FILENAME = sys.argv[4]

#setting up environment
CONTACTS_FILENAME = 'aws-accounts.txt.all'
CONTACTS_FILE = '/home/ubuntu/outputs/' + CONTACTS_FILENAME
#THROTTLE_DOWN_PERIOD = 0.05

startDateTime = datetime.today()

print("\n---> Script execution started @ {0} ...\n".format(startDateTime))

#check and create folders provided via arguments if it doesn't exist
print("\n---> Checking if the directories exist ...")
if os.path.exists(TEMP_FOLDER) == False: 
    os.mkdir(TEMP_FOLDER)
    print("---> Directory {0} does not exist, it is created ...".format(TEMP_FOLDER))

if os.path.exists(DESTINATION_DIRECTORY) == False: 
    os.mkdir(DESTINATION_DIRECTORY)
    print("---> Directory {0} does not exist, it is created ...".format(DESTINATION_DIRECTORY))
print("---> Checking if the directories exist finished!")


print("\n---> Looping through accounts started ...")

totalCountOfAccounts = sum(1 for line in open(CONTACTS_FILE, mode='r'))

initatedThreads = []

#feching current caller identity
stsClient = boto3.client('sts')
currentCallerId = stsClient.get_caller_identity()

with open(CONTACTS_FILE, mode='r') as contactsFile:
    csvReader = csv.reader(contactsFile, delimiter=';')
    currentAccountNumber = 1
    for currentRow in csvReader:
        #if currentRow[0] != "178129072641" and currentRow[0] != "651225517062": continue

        print("---> Account {0} of {1}: Account ID {2}, Account name {3}".format(currentAccountNumber, totalCountOfAccounts, currentRow[0], currentRow[1]))

        extractAccountInfo(currentRow[0], currentRow[1], initatedThreads, currentCallerId['Account'])
#MULTHREAD AT ACCOUNT LEVEL - EXPERIMENTAL - DO NOT UNCOMMENT
#        newThread = AccountsThread(currentRow[0], currentRow[1], initatedThreads, currentCallerId['Account'])
#        initatedThreads.append(newThread)
#        newThread.start()

        if currentAccountNumber == int(ACCOUNT_NUMBER_LIMIT): break
        else: currentAccountNumber += 1

        sleep(THROTTLE_DOWN_PERIOD)

print("---> Looping through accounts finished!")

#waiting all threads to finish
print("\n---> Waiting for working threads to finish ...")
for t in initatedThreads:
    t.join()
print("---> Working threads to finished!")

#reading all results into an array
print("\n---> Joning temporary results into one list started ...")
tmpFileList = glob.glob(os.path.join(TEMP_FOLDER, "*"))

destinationFileContentList = []

for filePath in tmpFileList:
    with open(filePath) as f:
        destinationFileContentList.append(f.read())
    f.close()
print("---> Joning temporary results into one list finished!")

#saving results
print("\n---> Saving final results to destination file '{0}' started ...".format(DESTINATION_DIRECTORY + DESTINATION_FILENAME))
with open(DESTINATION_DIRECTORY + DESTINATION_FILENAME, 'w') as f:
    for item in destinationFileContentList:
        f.write("%s" % item)
f.close()
print("---> Saving final results to destination file finished!")

##########################################################
#      CLEANING UP
##########################################################

#removing temporary files

print("\n---> Removing files from temporary folder '{0}' started ...".format(TEMP_FOLDER))

try:
    for f in tmpFileList:
        os.remove(f)
except OSError as e:
    print("\nXXX> Unexpeted ERROR: %s : %s" % (f, e.strerror))

print("---> Removing files from temporary folder '{0}' finished!".format(TEMP_FOLDER))

#removing temporay folder
print("\n---> Removing temporary folder '{0}' started ...".format(TEMP_FOLDER))

try:
    os.rmdir(TEMP_FOLDER)
except OSError as e:
    print("\nXXX> Unexpeted ERROR: %s : %s" % (f, e.strerror))

print("---> Removing temporary folder '{0}' finished!".format(TEMP_FOLDER))


##########################################################
#      FINISHING SCRIPT
##########################################################

#finishing script
endDateTime = datetime.today()
scriptExecutionTime = endDateTime - startDateTime

print("\n---> Script execution ended @ {0}".format(endDateTime))
print("\n---> Total execution time: {0}\n".format(scriptExecutionTime))
