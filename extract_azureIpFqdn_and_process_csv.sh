#!/bin/bash

#Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
#the European Commission - subsequent versions of the EUPL (the "Licence");
#You may not use this work except in compliance with the Licence.
#You may obtain a copy of the Licence at:
#
#   https://joinup.ec.europa.eu/software/page/eupl
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the Licence is distributed on an "AS IS" basis,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the Licence for the specific language governing permissions and
#limitations under the Licence.

timestamp_start=$(date)

CORE_SCRIPT_FILENAME="extract_azureIpFqdn_and_process_csv.sh"
LOG_FILE="/home/ubuntu/logs/$CORE_SCRIPT_FILENAME.log"
ERROR_LOG_FILE="/home/ubuntu/logs/$CORE_SCRIPT_FILENAME.error.log"

echo "-------------------------------------------"
echo "---> Script ($CORE_SCRIPT_FILENAME) execution started @ $timestamp_start"

AZURE_IP_FQDN_EXTRACT_SCRIPT_FILE="/home/ubuntu/scripts/extract_ip_fqdn_from_azure.ps1"
CSV_TRANSFORM_SCRIPT_FILE="/home/ubuntu/scripts/convert_azureGraphCsv_to_tenableCsv.py"
CSV_TRANSFORM_ENDPOINT_WITHOUT_FQDN_SCRIPT_FILE="/home/ubuntu/scripts/convert_to_cloudEndpointCsv_without_FQDN.py"

OUTPUT_FILENAME="azure-public_ip_address.csv"
OUTPUT_FILENAME_HISTORY="azure-public_ip_address.csv.history"
OUTPUT_FILENAME_PREVIOUS_ITERATION="azure-public_ip_address.csv.previous_iteration"
OUTPUT_FILE_TMP="/home/ubuntu/outputs/$OUTPUT_FILENAME.tmp"
OUTPUT_FILE_TMP2="/home/ubuntu/outputs/$OUTPUT_FILENAME.tmp2"
OUTPUT_FILE_TMP3="/home/ubuntu/outputs/$OUTPUT_FILENAME.tmp3"
OUTPUT_FILE="/home/ubuntu/outputs/$OUTPUT_FILENAME"
OUTPUT_FILE_HISTORY="/home/ubuntu/outputs/$OUTPUT_FILENAME_HISTORY"
OUTPUT_FILE_HISTORY_TMP="/home/ubuntu/outputs/$OUTPUT_FILENAME_HISTORY.tmp"
OUTPUT_FILE_PREVIOUS_ITERATION="/home/ubuntu/outputs/$OUTPUT_FILENAME_PREVIOUS_ITERATION"

AZURE_MANUAL_EXPORT_FILE="/home/ubuntu/outputs/azure-export-2022-03-17.csv"

S3_BUCKET_NAME="ist-vm/exports"
S2_S3_BUCKET_NAME="s2-share/exports"
SNS_ARN="arn:aws:sns:eu-central-1:042034201921:azure-public-endpoints-extraction"

[[ ! -f $CSV_TRANSFORM_ENDPOINT_WITHOUT_FQDN_SCRIPT_FILE ]] && echo "Transform does not exist!!!" && exit
#[[ ! -f $ERROR_LOG_FILE ]] && touch $ERROR_LOG_FILE

#creating copy of previous iteration for extracted endpoints
cp "$OUTPUT_FILE" "$OUTPUT_FILE_PREVIOUS_ITERATION"

#extracting azure IPs and FQDNs into temporary CSV file
echo ""
echo "---> Extracting Azure endpoints ..."
/usr/bin/pwsh $AZURE_IP_FQDN_EXTRACT_SCRIPT_FILE $OUTPUT_FILE_TMP
echo "---> Extraction finished!"

#transforming the extracted temp CSV into final format to import into tenable 
echo ""
echo "---> Transforming CSV files got from extraction ..."
/usr/bin/python3 $CSV_TRANSFORM_SCRIPT_FILE $OUTPUT_FILE_TMP $OUTPUT_FILE
echo "---> Transformation finished!"

echo ""
echo "---> Merging with manual extraction file, making uniqe lines and cleaning temp files ..."
#removing temporary file
rm $OUTPUT_FILE_TMP

#making copy of final file into temporary file for the next transformational step
cp $OUTPUT_FILE $OUTPUT_FILE_TMP

#merging manually extracted file (this file is manually exported from Azure GUI since not all subs are visible to the extract script) with previously extracted data into another temporary file
cat $OUTPUT_FILE_TMP $AZURE_MANUAL_EXPORT_FILE > $OUTPUT_FILE_TMP2

#removing the FQDN from the endpoint extraction
echo ""
echo "---> Removing FQDN from end result file ..."
/usr/bin/python3 $CSV_TRANSFORM_ENDPOINT_WITHOUT_FQDN_SCRIPT_FILE $OUTPUT_FILE_TMP2 $OUTPUT_FILE_TMP3
echo "---> FQDN removal finished!"

#making merged CSV lines unique and purged from null values and outputing result into final CSV file
cat $OUTPUT_FILE_TMP3 | sed 's/;null;/;;/g' | tr -d '\r' | sort | uniq > $OUTPUT_FILE

#removing all temporary files
rm $OUTPUT_FILE_TMP $OUTPUT_FILE_TMP2 $OUTPUT_FILE_TMP3
echo "---> Operations finished!"

#creating history file
echo ""
echo "---> Creating history file ..."
cat "$OUTPUT_FILE" >> "$OUTPUT_FILE_HISTORY"
cat "$OUTPUT_FILE_HISTORY" | sort | uniq > "$OUTPUT_FILE_HISTORY_TMP"
cp "$OUTPUT_FILE_HISTORY_TMP" "$OUTPUT_FILE_HISTORY"
rm "$OUTPUT_FILE_HISTORY_TMP"
echo "---> History file created!"


#storing results in S3 bucket
echo ""
echo "---> Storing results in S3 bucket ..."
aws s3 cp $OUTPUT_FILE s3://$S3_BUCKET_NAME/$OUTPUT_FILENAME
aws s3 cp $OUTPUT_FILE_HISTORY s3://$S3_BUCKET_NAME/$OUTPUT_FILENAME_HISTORY
aws s3 cp $OUTPUT_FILE_HISTORY s3://$S2_S3_BUCKET_NAME/$OUTPUT_FILENAME_HISTORY
aws s3 cp $OUTPUT_FILE_PREVIOUS_ITERATION s3://$S3_BUCKET_NAME/$OUTPUT_FILENAME_PREVIOUS_ITERATION
echo "---> Results stored in S3!"


#sending results to SNS
echo ""
echo "---> Sending SNS notification ..."
timestamp_end=$(date '+%Y-%m-%d')
errorFileSize=$(wc -c "$ERROR_LOG_FILE" | awk '{print $1}')
[[ ! $errorFileSize -eq 0 ]] && errorLog="Errors OCCURED during processing. Please check logs." || errorLog="No issues found during processing."

SNS_message=$(echo "AZURE public endpoint extraction finished. $errorLog")
SNS_subject=$(echo "$timestamp_end - AZURE public endpoint extraction - finished")
executeResult=$(aws sns publish --topic-arn "$SNS_ARN" --subject "$SNS_subject" --message "$SNS_message")
echo "---> SNS notification sent!"

#finishing script
timestamp_end=$(date)
echo ""
echo "---> Script ($CORE_SCRIPT_FILENAME) execution ended @ $timestamp_end"
echo "-------------------------------------------"
