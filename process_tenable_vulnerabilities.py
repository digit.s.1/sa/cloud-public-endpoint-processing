#Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
#the European Commission - subsequent versions of the EUPL (the "Licence");
#You may not use this work except in compliance with the Licence.
#You may obtain a copy of the Licence at:
#
#   https://joinup.ec.europa.eu/software/page/eupl
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the Licence is distributed on an "AS IS" basis,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the Licence for the specific language governing permissions and
#limitations under the Licence.

import csv
import json
import time
import sys
import boto3
from datetime import datetime
from botocore.exceptions import ClientError
import requests

#CREDENTIALS_FILE = '/home/ubuntu/.aws/credentials'
TENABLE_REGION = 'eu-central-1'
TENABLE_SECRET_KEY_NAME = 'prod/TenableAPI/S1_VA_AWS_ScriptRunner'
AWS_CONTACTS_FILENAME = 'aws-accounts.complete.txt.all'
AWS_CONTACTS_FILE = '/home/ubuntu/outputs/' + AWS_CONTACTS_FILENAME
AZURE_CONTACTS_FILENAME = 'azure-accounts.complete.txt.all'
AZURE_CONTACTS_FILE = '/home/ubuntu/outputs/' + AZURE_CONTACTS_FILENAME
VULN_WITH_CONTACTS_FILENAME = 'vuln-cloud-assets-with-contacts.csv'
VULN_WITH_CONTACTS_FILE = '/home/ubuntu/outputs/' + VULN_WITH_CONTACTS_FILENAME
VULN_EXCEPTIONS_FILENAME = 'process_tenable_vulnerabilities-exclusion_list.csv'
VULN_EXCEPTIONS_FILE = '/home/ubuntu/outputs/' + VULN_EXCEPTIONS_FILENAME

WAIT_TIME_BETWEEN_CALLS = 5
WAIT_TIME_BETWEEN_ITERATIONS = 15
NUMBER_OF_ITERATIONS = 5
SA_S3_BUCKET_NAME = 'ist-vm'
SA_S3_BUCKET_NAME_ENDPOINT = 'exports/'
SA_S3_BUCKET_VULN_EXCEPTIONS_FILE = 'script_configuration/process_tenable_vulnerabilities-exclusion_list.csv'
SA_S3_BUCKET_VULN_EXCEPTIONS_BACKUP_FILE = 'backup/script_configuration/process_tenable_vulnerabilities-exclusion_list.csv'

TOPIC_ARN = 'arn:aws:sns:eu-central-1:042034201921:vulnerability-data-processing'

#starting execution of the script
startDateTime = datetime.today()

print("\n---> Script execution started @ {0} ...\n".format(startDateTime))

#############################################################
#
# fetching credentials information
#
#############################################################

#reading credentials information form aws config file
#print("---> Fetching AWS credentials")

#with open(CREDENTIALS_FILE, 'r') as f:
#    fileLines = f.readlines()

#for currentLine in fileLines:
#    currentLineSplit = currentLine.split("=")
#    if currentLineSplit[0] == "aws_access_key_id":
#        aws_access_key_id_ = currentLineSplit[1].strip()
#    elif currentLineSplit[0] == "aws_secret_access_key":
#        aws_secret_access_key_ = currentLineSplit[1].strip()

#print("---> AWS Credentials successfuly fetched")

try:
    #reading credentials information from aws secret manager
    print("---> Fetching TENABLE credentials")

    session = boto3.session.Session()
    client = session.client(
        service_name='secretsmanager',
        region_name=TENABLE_REGION,
    )

    try:
        get_secret_value_response = client.get_secret_value(
            SecretId=TENABLE_SECRET_KEY_NAME
        )
    except ClientError as e:
        if e.response['Error']['Code'] == 'ResourceNotFoundException':
            print("The requested secret " + secret_name + " was not found")
        elif e.response['Error']['Code'] == 'InvalidRequestException':
            print("The request was invalid due to:", e)
        elif e.response['Error']['Code'] == 'InvalidParameterException':
            print("The request had invalid params:", e)
        elif e.response['Error']['Code'] == 'DecryptionFailure':
            print("The requested secret can't be decrypted using the provided KMS key:", e)
        elif e.response['Error']['Code'] == 'InternalServiceError':
            print("An error occurred on service side:", e)
    else:
        # Secrets Manager decrypts the secret value using the associated KMS CMK
        # Depending on whether the secret was a string or binary, only one of these fields will be populated
        if 'SecretString' in get_secret_value_response:
            #convert result string encoded as key value JSON payload into Python list
            secretDataText = get_secret_value_response['SecretString']
            secretDataList = json.loads(secretDataText)
            TENABLE_ACCESS_KEY = secretDataList['accessKey']
            TENABLE_SECRET_KEY = secretDataList['secretKey']
        else:
            #binary output is not compatible
            raise Exception("Tenable API secret returned in binary. Script execution cannot continue.")

    #setting tenable access key string for API calls
    TENABLE_API_KEY="accessKey={0};secretKey={1}".format(TENABLE_ACCESS_KEY, TENABLE_SECRET_KEY)

    print("---> TENABLE Credentials successfuly fetched")


#############################################################
#
# fetching vulnerabilities exceptions files from S3
#
#############################################################


    #putting files in SA S3 bucket
    print("\n---> Getting vulnerability exception file from S3 bucket ({0}) to '{1}'".format(SA_S3_BUCKET_NAME, VULN_EXCEPTIONS_FILE))

    s3 = boto3.client('s3')
    s3.download_file(SA_S3_BUCKET_NAME, SA_S3_BUCKET_VULN_EXCEPTIONS_FILE, VULN_EXCEPTIONS_FILE)


    print("\n---> Fetching vulnerability exception information into list")

    print("---> Opening vulnerability exception file {0} for reading".format(VULN_EXCEPTIONS_FILE))

    with open(VULN_EXCEPTIONS_FILE, mode='r') as csvFile:
        print("-----> Reading file started ...")
        csvReader = csv.DictReader(csvFile, delimiter=';')
        print("-----> Reading file finished!")
        print("-----> Reading file into list started ...")
        vulnerabilityExceptionList = list(csvReader)
        print("-----> Reading file into list finished!")

#    print(vulnerabilityExceptionList)

    print("---> Vulnerabilities exceptions successfully retrieved")


#############################################################
#
# exporting vulnerabilities from Tenable
#
#############################################################

    print("\n---> Exporting vulnerabilities from Tenable")

    apiUrl = "https://cloud.tenable.com/vulns/export"

    payload = {
        "filters": {"tag.CLOUD-PROVIDER": ["AWS", "AZURE"]},
        "include_unlicensed": True
    }
    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "X-ApiKeys": TENABLE_API_KEY
    }

    response = requests.request("POST", apiUrl, json=payload, headers=headers)

    if response.status_code != 200:
        errorText="Error occured while exporting vulnerabilities from Tenable (API return code {0})".format(response.error_code)
        raise Exception(errorText)

    vulnerabilitiesExportResponse=json.loads(response.text)

#############################################################
#
# fetching vulnerabilities export status from Tenable
#
#############################################################

    print("\n---> Fetching vulnerabilities export status")

    print("\n---> Waiting for {0} seconds before continuing with next API call".format(WAIT_TIME_BETWEEN_CALLS))
    time.sleep(WAIT_TIME_BETWEEN_CALLS)

    apiUrl = "https://cloud.tenable.com/vulns/export/{0}/status".format(vulnerabilitiesExportResponse['export_uuid'])

    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "X-ApiKeys": TENABLE_API_KEY
    }

    print("---> Calling vulnerabilities export status API")
    for i in range(0, NUMBER_OF_ITERATIONS):
        print("-----> Iteration {0} of {1}".format(i+1, NUMBER_OF_ITERATIONS))
        response = requests.request("GET", apiUrl, headers=headers)

        if response.status_code != 200:
            errorText="Error occured while fetching vulnerabilities export status from Tenable (API return code {0})".format(response.error_code)
            raise Exception(errorText)

        vulnerabilitiesExportStatusResponse=json.loads(response.text)

        if vulnerabilitiesExportStatusResponse['status'] == "FINISHED":
            print("-----> Call successfuly finished!")
            break

        print("-----> Waiting for {0} seconds before continuing with next iteration step (API call)".format(WAIT_TIME_BETWEEN_ITERATIONS))
        time.sleep(WAIT_TIME_BETWEEN_ITERATIONS)

    #if the loops finished and execution did'n finish, raise expection and dump last execution results
    if vulnerabilitiesExportStatusResponse['status'] != "FINISHED":
        errorText="Error occured while fetching status for vulnerabilities export. Response dump: {0}".format(response.text)
        raise Exception(errorText)

#############################################################
#
# downloading vulnerabilities export chunks from Tenable
#
#############################################################

    print("\n---> Downloading vulnerabilities export chunks")

    print("\n---> Waiting for {0} seconds before continuing with next API call".format(WAIT_TIME_BETWEEN_CALLS))
    time.sleep(WAIT_TIME_BETWEEN_CALLS)
    vulnerabilitiesData = list()

    for currentChunk in vulnerabilitiesExportStatusResponse['chunks_available']:

        print("\n-----> Initiating download of chunk {0} of {1}".format(currentChunk,len(vulnerabilitiesExportStatusResponse['chunks_available'])))
        apiUrl = "https://cloud.tenable.com/vulns/export/{0}/chunks/{1}".format(vulnerabilitiesExportResponse['export_uuid'], currentChunk)

        headers = {
            "Accept": "application/octet-stream",
            "X-ApiKeys": TENABLE_API_KEY
        }

        print("-----> Calling vulnerabilities export status API")

        for i in range(0, NUMBER_OF_ITERATIONS):
            print("-------> Iteration {0} of {1}".format(i+1, NUMBER_OF_ITERATIONS))
            response = requests.request("GET", apiUrl, headers=headers)

            if response.status_code != 200:
                errorText="Error occured while exporting vulnerabilities chunk from Tenable (API return code {0})".format(response.error_code)
                raise Exception(errorText)
            else:
                print("-------> Chunk {0} started downloading...".format(currentChunk))
                vulnerabilitiesData.extend(json.loads(response.text))
                print("-------> Chunk {0} sucessfuly downloaded!".format(currentChunk))

                #f = open("/home/ubuntu/outputs/vulntest/chunk_{0}.json".format(currentChunk), "w")
                #f.write(response.text)
                #f.close()
                break

        #TO DELETE DEBUG ONLY
        #if currentChunk == 1:
        #    break

    print("---> Tenable vulnerabilities successfuly exported")

#############################################################
#
# exporting assets from Tenable
#
#############################################################

    print("\n---> Exporting assets from Tenable")

    apiUrl = "https://cloud.tenable.com/assets/export"

    payload = {
        "filters": {"tag.CLOUD-PROVIDER": ["AWS", "AZURE"]},
        "chunk_size": 5000
    }
    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "X-ApiKeys": TENABLE_API_KEY
    }

    response = requests.request("POST", apiUrl, json=payload, headers=headers)

    if response.status_code != 200:
        errorText="Error occured while exporting assets from Tenable (API return code {0})".format(response.error_code)
        raise Exception(errorText)

    assetsExportResponse=json.loads(response.text)


#############################################################
#
# fetching assets export status from Tenable
#
#############################################################

    print("\n---> Fetching assets export status")

    print("\n---> Waiting for {0} seconds before continuing with next API call".format(WAIT_TIME_BETWEEN_CALLS))
    time.sleep(WAIT_TIME_BETWEEN_CALLS)

    apiUrl = "https://cloud.tenable.com/assets/export/{0}/status".format(assetsExportResponse['export_uuid'])

    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "X-ApiKeys": TENABLE_API_KEY
    }

    print("---> Calling vulnerabilities export status API")
    for i in range(0, NUMBER_OF_ITERATIONS):
        print("-----> Iteration {0} of {1}".format(i+1, NUMBER_OF_ITERATIONS))
        response = requests.request("GET", apiUrl, headers=headers)

        if response.status_code != 200:
            errorText="Error occured while fetching assets export status from Tenable (API return code {0})".format(response.error_code)
            raise Exception(errorText)

        assetsExportStatusResponse=json.loads(response.text)

        if assetsExportStatusResponse['status'] == "FINISHED":
            print("-----> Call successfuly finished!")
            break

        print("-----> Waiting for {0} seconds before continuing with next iteration step (API call)".format(WAIT_TIME_BETWEEN_ITERATIONS))
        time.sleep(WAIT_TIME_BETWEEN_ITERATIONS)

    #if the loops finished and execution did'n finish, raise expection and dump last execution results
    if assetsExportStatusResponse['status'] != "FINISHED":
        errorText="Error occured while fetching status for assets export. Response dump: {0}".format(response.text)
        raise Exception(errorText)


#############################################################
#
# fetching assets export chunks from Tenable
#
#############################################################

    print("\n---> Downloading assets export chunks")

    print("\n---> Waiting for {0} seconds before continuing with next API call".format(WAIT_TIME_BETWEEN_CALLS))
    time.sleep(WAIT_TIME_BETWEEN_CALLS)
    assetsData = list()

    for currentChunk in assetsExportStatusResponse['chunks_available']:

        print("\n-----> Initiating download of chunk {0} of {1}".format(currentChunk,len(assetsExportStatusResponse['chunks_available'])))
        apiUrl = "https://cloud.tenable.com/assets/export/{0}/chunks/{1}".format(assetsExportResponse['export_uuid'], currentChunk)

        headers = {
            "Accept": "application/json",
            "X-ApiKeys": TENABLE_API_KEY
        }

        print("-----> Calling asset chunk export API")

        for i in range(0, NUMBER_OF_ITERATIONS):
            print("-------> Iteration {0} of {1}".format(i+1, NUMBER_OF_ITERATIONS))
            response = requests.request("GET", apiUrl, headers=headers)

            if response.status_code != 200:
                errorText="Error occured while fetching assets chunk from Tenable (API return code {0})".format(response.error_code)
                raise Exception(errorText)
            else:
                print("-------> Chunk {0} started downloading ...".format(currentChunk))
                assetsData.extend(json.loads(response.text))
                #f = open("/home/ubuntu/outputs/vulntest/assets_chunk_{0}.json".format(currentChunk), "w")
                #f.write(response.text)
                #f.close()
                print("-------> Chunk {0} sucessfuly downloaded!".format(currentChunk))
                break

    print("---> Tenable assets successfuly downloaded")



#############################################################
#
# fetching cloud account contact information into list
#
#############################################################

    print("\n---> Fetching cloud account information into list")

    print("---> Opening AWS cloud account contact file {0} for reading".format(AWS_CONTACTS_FILE))

    with open(AWS_CONTACTS_FILE, mode='r') as csvFile:
        print("-----> Reading file started ...".format(AWS_CONTACTS_FILE))
        csvReader = csv.DictReader(csvFile, delimiter=';')
        print("-----> Reading file finished!".format(AWS_CONTACTS_FILE))
        print("-----> Reading file into list started ...".format(AWS_CONTACTS_FILE))
        awsContactList = list(csvReader)
        print("-----> Reading file into list finished!".format(AWS_CONTACTS_FILE))

    print("---> Opening AZURE cloud account contact file {0} for reading".format(AZURE_CONTACTS_FILE))

    with open(AZURE_CONTACTS_FILE, mode='r') as csvFile:
        print("-----> Reading file started ...".format(AZURE_CONTACTS_FILE))
        csvReader = csv.DictReader(csvFile, delimiter=';')
        print("-----> Reading file finished!".format(AZURE_CONTACTS_FILE))
        print("-----> Reading file into list started ...".format(AZURE_CONTACTS_FILE))
        azureContactList = list(csvReader)
        print("-----> Reading file into list finished!".format(AZURE_CONTACTS_FILE))


#############################################################
#
# merging vulnerabilities, assets and contact data
#
#############################################################

#vuln:     vulnerabilitiesData
#assets:   assetsData
#contacts: awsContactList
#contacts: azureContactList

    #loop through vulnerabilities
    outputList = []
    outputRow = []

    outputRow.append("Plugin Id")
    outputRow.append("Plugin Name")
    outputRow.append("Severity")
    outputRow.append("Asset Tenable Id")
    outputRow.append("Asset Cloud Id")
    outputRow.append("Asset IP Address")
    outputRow.append("Protocol")
    outputRow.append("Port")
    outputRow.append("Asset FQDN")
    outputRow.append("Plugin Output")
    outputRow.append("Synopsis")
    outputRow.append("Description")
    outputRow.append("Solution")
    outputRow.append("See Also")
    outputRow.append("Vulnerability Priority Rating")
    outputRow.append("CVSS V2 Base Score")
    outputRow.append("CVSS V3 Base Score")
    outputRow.append("CVSS V2 Vector")
    outputRow.append("CVSS V3 Vector")
    outputRow.append("Exploit Available")
    outputRow.append("Exploit Maturity")
    outputRow.append("CVE")
    outputRow.append("Risk Level")
    outputRow.append("Vulnerability First Found")
    outputRow.append("Vulnerability Last Found")
    outputRow.append("Cloud Provider")
    outputRow.append("Cloud Account Identifier")
    outputRow.append("Cloud Account Name")
    outputRow.append("Cloud Account Organisation Acronym")
    outputRow.append("Responsible contact names")
    outputRow.append("Responsible contact emails")
    outputRow.append("Responsible contact phones")
    outputRow.append("Operation contact names")
    outputRow.append("Operation contact emails")
    outputRow.append("Operation contact phones")
    outputRow.append("Security contact names")
    outputRow.append("Security contact emails")
    outputRow.append("Security contact phones")

    outputList.append(outputRow)

    for vuln in vulnerabilitiesData:

        #reset all temporary variable placeholders
        outputRow = []

        PluginId= ""
        PluginName= ""
        Severity= ""
        AssetTenableId= ""
        AssetCloudId= ""
        AssetIPAddress= ""
        PluginProtocol= ""
        PluginPort= ""
        AssetFQDN= ""
        PluginOutput= ""
        PluginSynopsis= ""
        PluginDescription= ""
        PluginSolution= ""
        PluginSeeAlso= ""
        VulnerabilityPriorityRating= ""
        CVSSV2BaseScore= ""
        CVSSV3BaseScore= ""
        CVSSV2Vector= ""
        CVSSV3Vector= ""
        ExploitAvailable= ""
        ExploitMaturity= ""
        CVE= ""
        RiskLevel= ""
        VulnerabilityFirstFound= ""
        VulnerabilityLastFound= ""
        CloudProvider= ""
        CloudAccountIdentifier= ""
        CloudAccountName= ""
        CloudAccountOrganisationAcronym= ""
        ResponsibleContactNames= ""
        ResponsibleContactEmails= ""
        ResponsibleContactPhones= ""
        OperationContactNames= ""
        OperationContactEmails= ""
        OperationContactPhones= ""
        SecurityContactNames= ""
        SecurityContactEmails= ""
        SecurityContactPhones= ""

        #TO DELETE, DEBUG PURPOSES ONLY
        #if vuln['severity'] != "high" or vuln['severity'] != "medium":
        #we are skipping info severity lines
        if vuln['severity'] == "info":
            continue

        # changed by Robert Schweers on 06/01/2023 (ex. "=""152182""" -> 152182)
        #PluginId = "=\"" + str(vuln['plugin']['id']) + "\""
        PluginId = vuln['plugin']['id']


        #print(f'PLUGIN: {json.dumps(vuln, indent=3)}')


        PluginName = vuln['plugin']['name']
        Severity = vuln['severity']
        CVSSV3BaseScore = ''
        if 'cvss3_base_score' in vuln['plugin']:
            CVSSV3BaseScore = vuln['plugin']['cvss3_base_score']
        CVSSV2BaseScore = ''
        if 'cvss_base_score' in vuln['plugin']:
            CVSSV2BaseScore = vuln['plugin']['cvss_base_score']
        PluginProtocol = vuln['port']['protocol']
        PluginPort = vuln['port']['port']

        try:
            PluginOutput = vuln['output'].replace("\"", "\"\"")
            if PluginOutput[0:1] == "-" or PluginOutput[0:1] == "+" or PluginOutput[0:1] == "=" or PluginOutput[0:1] == "@":
                PluginOutput = "'" + PluginOutput
            PluginOutput = (PluginOutput[:32000] + "... TRUNCATED ...") if len(PluginOutput) > 32000 else PluginOutput
        except:
            PluginOutput = ""

        try:
            PluginSynopsis = vuln['plugin']['synopsis'].replace("\"", "\"\"")
            if PluginSynopsis[0:1] == "-" or PluginSynopsis[0:1] == "+" or PluginSynopsis[0:1] == "=" or PluginSynopsis[0:1] == "@":
                PluginSynopsis = "'" + PluginSynopsis
            PluginSynopsis = (PluginSynopsis[:32000] + "... TRUNCATED ...") if len(PluginSynopsis) > 32000 else PluginSynopsis
        except:
            PluginSynopsis = ""

        try:
            PluginDescription = vuln['plugin']['description'].replace("\"", "\"\"")
            if PluginDescription[0:1] == "-" or PluginDescription[0:1] == "+" or PluginDescription[0:1] == "=" or PluginDescription[0:1] == "@":
                PluginDescription = "'" + PluginDescription
            PluginDescription = (PluginDescription[:32000] + "... TRUNCATED ...") if len(PluginDescription) > 32000 else PluginDescription
        except:
            PluginDescription = ""

        try:
            PluginSolution = vuln['plugin']['solution'].replace("\"", "\"\"")
            if PluginSolution[0:1] == "-" or PluginSolution[0:1] == "+" or PluginSolution[0:1] == "=" or PluginSolution[0:1] == "@":
                PluginSolution = "'" + PluginSolution
            PluginSolution = (PluginSolution[:32000] + "... TRUNCATED ...") if len(PluginSolution) > 32000 else PluginSolution
        except:
            PluginSolution = ""

        try:
            PluginSeeAlso = '\n'.join(vuln['plugin']['see_also']).replace("\"", "\"\"")
            if PluginSeeAlso[0:1] == "-" or PluginSeeAlso[0:1] == "+" or PluginSeeAlso[0:1] == "=" or PluginSeeAlso[0:1] == "@":
                PluginSeeAlso = "'" + PluginSeeAlso
            PluginSeeAlso = (PluginSeeAlso[:32000] + "... TRUNCATED ...") if len(PluginSeeAlso) > 32000 else PluginSeeAlso
        except:
            PluginSeeAlso = ""

        try:
            VulnerabilityPriorityRating = vuln['plugin']['vpr']['score']
        except:
            VulnerabilityPriorityRating = ""

        try:
            CVSSV3Vector = vuln['plugin']['cvss3_vector']['raw']
        except:
            CVSSV3Vector = ""

        try:
            CVSSV2Vector = vuln['plugin']['cvss_vector']['raw']
        except:
            CVSSV2Vector = ""

        try:
            ExploitAvailable = vuln['plugin']['exploit_available']
            if ExploitAvailable == True:
                ExploitAvailable = "Yes"
            else:
                ExploitAvailable = "No"
        except:
            ExploitAvailable = ""

        try:
            ExploitMaturity = vuln['plugin']['vpr']['drivers']['exploit_code_maturity']
        except:
            ExploitMaturity = ""

        try:
            CVE = ', '.join(vuln['plugin']['cve'])
        except:
            CVE = ""

        RiskLevel = vuln['plugin']['risk_factor']

        try:
            VulnerabilityFirstFound = vuln['first_found']
        except:
            VulnerabilityFirstFound = ""

        try:
            VulnerabilityLastFound = vuln['last_found']
        except:
            VulnerabilityLastFound = ""


        for asset in assetsData:

            #skip assets not having ec2_owner_id
            #TO DELETE, DEBUG PURPOSES ONLY
            #if asset['aws_owner_id'] == "null":
            #    continue

            foundAssetMatch = 0

            if vuln['asset']['uuid'] == asset['id']: 

                foundAssetMatch = 1

                AssetTenableId = asset['id']

                try:
                    AssetCloudId = asset['aws_ec2_instance_id']
                except:
                    AssetCloudId = ""


                try:
                    AssetIPAddress = "=\"" + asset['ipv4s'][0] + "\""
                except:
                    try:
                        AssetIPAddress = "=\"" + asset['ipv4s'] + "\""
                    except:
                        AssetIPAddress = ""

                try:
                    AssetFQDN = asset['fqdns'][0]
                except:
                    try:
                        AssetFQDN = ' '.join(asset['fqdns'])
                    except:
                        AssetFQDN = ""
                AssetFQDN = AssetFQDN.replace("[]","")

                foundContactMatch = 0

                try:
                    CloudAccountIdentifier = "=\"" + asset['aws_owner_id'] + "\""
                except:
                    CloudAccountIdentifier = ""

                for awsContact in awsContactList:

                    if awsContact['Account Identifier'] == asset['aws_owner_id']:

                        foundContactMatch = 1

                        CloudProvider = "AWS"
                        CloudAccountIdentifier = "=\"" + awsContact['Account Identifier'] + "\""
                        CloudAccountName = awsContact['Account Name']
                        CloudAccountOrganisationAcronym = awsContact['Account Organisation Acronym']
                        ResponsibleContactNames = awsContact['Responsible contact names']
                        ResponsibleContactEmails = awsContact['Responsible contact emails']
                        ResponsibleContactPhones = "=\"" + awsContact['Responsible contact phones'] + "\""
                        OperationContactNames = awsContact['Operation contact names']
                        OperationContactEmails = awsContact['Operation contact emails']
                        OperationContactPhones = "=\"" + awsContact['Operation contact phones'] + "\""
                        SecurityContactNames = awsContact['Security contact names']
                        SecurityContactEmails = awsContact['Security contact emails']
                        SecurityContactPhones = "=\"" + awsContact['Security contact phones'] + "\""

                        break

                    else:
                        continue

                    #awsContactList loop ends here

                if foundContactMatch == 1:
                    break

                for azureContact in azureContactList:

                    if azureContact['Account Identifier'] == asset['aws_owner_id']:

                        foundContactMatch = 1

                        CloudProvider = "AZURE"
                        CloudAccountIdentifier = "=\"" + azureContact['Account Identifier'] + "\""
                        CloudAccountName = azureContact['Account Name']
                        CloudAccountOrganisationAcronym = azureContact['Account Organisation Acronym']
                        ResponsibleContactNames = azureContact['Responsible contact names']
                        ResponsibleContactEmails = azureContact['Responsible contact emails']
                        ResponsibleContactPhones = "=\"" + azureContact['Responsible contact phones'] + "\""
                        OperationContactNames = azureContact['Operation contact names']
                        OperationContactEmails = azureContact['Operation contact emails']
                        OperationContactPhones = "=\"" + azureContact['Operation contact phones'] + "\""
                        SecurityContactNames = azureContact['Security contact names']
                        SecurityContactEmails = azureContact['Security contact emails']
                        SecurityContactPhones = "=\"" + azureContact['Security contact phones'] + "\""

                        break

                    else:
                        continue

                    #awsContactList loop ends here

                break

            else:
                continue

            #assetsData loop ends here


        if foundAssetMatch == 0:
            AssetTenableId = ""
            AssetCloudId = ""
            AssetIPAddress = ""
            AssetFQDN = ""

        if foundContactMatch == 0:
            CloudProvider = ""
            CloudAccountIdentifier = ""
            CloudAccountName = ""
            CloudAccountOrganisationAcronym = ""
            ResponsibleContactNames = ""
            ResponsibleContactEmails = ""
            ResponsibleContactPhones = ""
            OperationContactNames = ""
            OperationContactEmails = ""
            OperationContactPhones = ""
            SecurityContactNames = ""
            SecurityContactEmails = ""
            SecurityContactPhones = ""


        #verifying the current row content for exceptions. If excpetion found, row is not generated
        skipRow = None
        for vulnerabilityException in vulnerabilityExceptionList:
            # changed by Robert Schweers on 06/01/2023 (ex. "=""152182""" -> 152182)
            # if vulnerabilityException['CLOUD_ACCOUNT_ID'] == CloudAccountIdentifier.replace('"','').replace('=','') and vulnerabilityException['TENABLE_ASSET_CLOUD_ID'] == AssetCloudId.replace('"','').replace('=','') and vulnerabilityException['PLUGIN_ID'] == PluginId.replace('"','').replace('=',''):
            if vulnerabilityException['CLOUD_ACCOUNT_ID'] == CloudAccountIdentifier.replace('"','').replace('=','') and vulnerabilityException['TENABLE_ASSET_CLOUD_ID'] == AssetCloudId.replace('"','').replace('=','') and vulnerabilityException['PLUGIN_ID'] == PluginId:
                #DEBUG PURPOSES ONLY
                #print("found match! {0} - {1} - {2}".format(CloudAccountIdentifier, AssetTenableId, PluginId))
                skipRow = True
                break

        #skip the row if exception is found
        if skipRow == True:
            continue

        outputRow.append(PluginId)
        outputRow.append(PluginName)
        outputRow.append(Severity)
        outputRow.append(AssetTenableId)
        outputRow.append(AssetCloudId)
        outputRow.append(AssetIPAddress)
        outputRow.append(PluginProtocol)
        outputRow.append(PluginPort)
        outputRow.append(AssetFQDN)
        outputRow.append(PluginOutput)
        outputRow.append(PluginSynopsis)
        outputRow.append(PluginDescription)
        outputRow.append(PluginSolution)
        outputRow.append(PluginSeeAlso)
        outputRow.append(VulnerabilityPriorityRating)
        outputRow.append(CVSSV2BaseScore)
        outputRow.append(CVSSV3BaseScore)
        outputRow.append(CVSSV2Vector)
        outputRow.append(CVSSV3Vector)
        outputRow.append(ExploitAvailable)
        outputRow.append(ExploitMaturity)
        outputRow.append(CVE)
        outputRow.append(RiskLevel)
        outputRow.append(VulnerabilityFirstFound)
        outputRow.append(VulnerabilityLastFound)
        outputRow.append(CloudProvider)
        outputRow.append(CloudAccountIdentifier)
        outputRow.append(CloudAccountName)
        outputRow.append(CloudAccountOrganisationAcronym)
        outputRow.append(ResponsibleContactNames)
        outputRow.append(ResponsibleContactEmails)
        outputRow.append(ResponsibleContactPhones)
        outputRow.append(OperationContactNames)
        outputRow.append(OperationContactEmails)
        outputRow.append(OperationContactPhones)
        outputRow.append(SecurityContactNames)
        outputRow.append(SecurityContactEmails)
        outputRow.append(SecurityContactPhones)

        outputList.append(outputRow)
        #vulnerabilitiesData loop ends here

    print("---> Writing vulnerability, assets and cloud account merged data content into CSV file: {0}".format(VULN_WITH_CONTACTS_FILE))

    with open(VULN_WITH_CONTACTS_FILE, 'w') as vulnWithContactCsvFile:
        # using csv.writer method from CSV package
        writeCSV = csv.writer(vulnWithContactCsvFile, delimiter=';')
        writeCSV.writerows(outputList)
    vulnWithContactCsvFile.close()

    #putting files in SA S3 bucket
    print("\n---> Putting vulnerability, assets and cloud account CSV file to S3 bucket ({0})".format(SA_S3_BUCKET_NAME))

    s3 = boto3.client('s3')

    #reading file in temporary string to put in S3 bucket
    tmpFileString = ""
    file = open(VULN_WITH_CONTACTS_FILE)
    tmpFileString = file.read()
    file.close()

    s3.put_object(Bucket=SA_S3_BUCKET_NAME, Body=tmpFileString, Key=SA_S3_BUCKET_NAME_ENDPOINT + VULN_WITH_CONTACTS_FILENAME)
    print("---> File {0} successfully uploaded to S3 bucket {1}".format(VULN_WITH_CONTACTS_FILE, SA_S3_BUCKET_NAME + '/' + SA_S3_BUCKET_NAME_ENDPOINT + VULN_WITH_CONTACTS_FILENAME))

    print("\n---> Creating vulnerability exception CSV file backup to S3 bucket ({0})".format(SA_S3_BUCKET_NAME))

    #reading file in temporary string to put in S3 bucket
    tmpFileString = ""
    file = open(VULN_EXCEPTIONS_FILE)
    tmpFileString = file.read()
    file.close()

    backupFileName = SA_S3_BUCKET_VULN_EXCEPTIONS_BACKUP_FILE + ".backup_" + startDateTime.strftime("%Y-%m-%d")
    s3.put_object(Bucket=SA_S3_BUCKET_NAME, Body=tmpFileString, Key=backupFileName)
    print("---> File {0} successfully uploaded to S3 bucket {1}".format(VULN_WITH_CONTACTS_FILE, SA_S3_BUCKET_NAME + '/' + backupFileName))


    #sending notification that processing went well
    print("\n---> Send notification to SNS topic: {0}".format(TOPIC_ARN))

    sns = boto3.client('sns')

    messageToSend = "Vulnerability data processing (merge with contact information) is successfully finished ..."
    subjectToSend = "{0} - Vulnerability data processing - successfully finished".format(startDateTime.strftime("%Y-%m-%d"))

    sns.publish(TopicArn=TOPIC_ARN,
        Message = messageToSend,
        Subject = subjectToSend)

    print("---> SNS notification successfully sent!")

except Exception as e:
    print("ERROR> {0}".format(e))


#finishing execution of the script
endDateTime = datetime.today()
scriptExecutionTime = endDateTime - startDateTime

print("\n---> Script execution ended @ {0}".format(endDateTime))
print("\n---> Total execution time: {0}\n".format(scriptExecutionTime))
