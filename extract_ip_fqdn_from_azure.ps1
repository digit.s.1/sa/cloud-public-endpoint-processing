#Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
#the European Commission - subsequent versions of the EUPL (the "Licence");
#You may not use this work except in compliance with the Licence.
#You may obtain a copy of the Licence at:
#
#   https://joinup.ec.europa.eu/software/page/eupl
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the Licence is distributed on an "AS IS" basis,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the Licence for the specific language governing permissions and
#limitations under the Licence.

Disable-AzContextAutosave -Scope Process

$credentialsRaw = aws secretsmanager get-secret-value --secret-id prod/AzureAPI/S1_VA_AWS_ScriptRunner
$credentialsJSON = $credentialsRaw | ConvertFrom-Json -AsHashtable
$credentialsHash = $credentialsJSON["SecretString"] | ConvertFrom-Json -AsHashtable

$SecureStringPwd = $credentialsHash["secretValue"] | ConvertTo-SecureString -AsPlainText -Force
$pscredential = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $credentialsHash["applicationID"], $SecureStringPwd
Connect-AzAccount -ServicePrincipal -Credential $pscredential -Tenant $credentialsHash["tenantID"] -SkipContextPopulation

$VerboseMode = "On" #verbose mode is switched on if value equals "On"; any other value else means verbose mode is switched off

$OutputFileName = $args[0]

$CSVHeader = "Subscription Id;Subscription Name;ipv4;fqdn;resource id\r\n"
$CSV = $CSVHeader

$sunscriptionsData = Get-AzSubscription
$TotalSubscriptionCount = $sunscriptionsData.count

if($VerboseMode -eq "On") { echo "---> Starting script (extract_ip_fqdn_from_azure.ps1) ..." "---> Total number of subscriptions: $TotalSubscriptionCount" "--------------"}
$ScriptExecStartDate = Get-Date

#calling Azure Graph query that searches for all publich ip addresses across all visible subscriptions
if($VerboseMode -eq "On") { echo "---> Calling AZ Graph ..."}

$CSV = Search-AzGraph -Query 'resources
| where type =~ "microsoft.compute/virtualmachines"
| project subscriptionId=split(id, "/", 2)[0], resourceId=id, vmId = tolower(tostring(id)), vmName = name
| join (Resources
    | where type =~ "microsoft.network/networkinterfaces"
    | mv-expand ipconfig=properties.ipConfigurations
    | project vmId = tolower(tostring(properties.virtualMachine.id)), publicIpId = tostring(ipconfig.properties.publicIPAddress.id)
    | join kind=leftouter (Resources
        | where type =~ "microsoft.network/publicipaddresses"
        | project publicIpId = id, ipAddress = properties.ipAddress, fqdn=properties.dnsSettings.fqdn
    ) on publicIpId
    | project-away publicIpId, publicIpId1
) on vmId
| extend subscriptionId = tostring(subscriptionId) 
| join kind=inner (resourcecontainers | where type == "microsoft.resources/subscriptions" | project subscriptionId, subscriptionIdFull=id, subscriptionName=name | extend subscriptionId = tostring(subscriptionId)) on $left.subscriptionId == $right.subscriptionId
| project-away vmId, vmId1
| order by subscriptionId asc
| sort by vmName asc
| where isnotempty(ipAddress)'


$CSVold = Search-AzGraph -Query 'resources
| where type contains "publicipaddresses" and isnotempty(properties.ipAddress)
| project subscriptionId=split(id, "/", 2)[0], ipAddress=properties.ipAddress, fqdn=properties.dnsSettings.fqdn, resourceId=id
| extend subscriptionId = tostring(subscriptionId)
| join kind=inner (resourcecontainers | where type == "microsoft.resources/subscriptions" | project subscriptionId, subscriptionIdFull=id, subscriptionName=name | extend subscriptionId = tostring(subscriptionId)) on $left.subscriptionId == $right.subscriptionId
| order by subscriptionId asc'

if($VerboseMode -eq "On") { echo "---> AZ Graph execution finished!"}

#exporting fetched query results into CSV
if($VerboseMode -eq "On") { echo "---> Storing query execution into CSV file ..."}
echo $CSV | Export-Csv -Path $OutputFileName -NoTypeInformation -Delimiter ';'
if($VerboseMode -eq "On") { echo "---> Storing finished!"}

#Recording end time and date after execution of the script
$ScriptExecEndDate = Get-Date
#Calculating difference of start and end time
$ScriptExecTime = New-TimeSpan -Start $ScriptExecStartDate -End $ScriptExecEndDate
#Getting difference in seconds
$ScriptExecTimeInSec = $ScriptExecTime.TotalSeconds

if($VerboseMode -eq "On") { echo "--------------" "---> Total execution time: $ScriptExecTimeInSec" }
