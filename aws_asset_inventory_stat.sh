#!/bin/bash

#Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
#the European Commission - subsequent versions of the EUPL (the "Licence");
#You may not use this work except in compliance with the Licence.
#You may obtain a copy of the Licence at:
#
#   https://joinup.ec.europa.eu/software/page/eupl
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the Licence is distributed on an "AS IS" basis,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the Licence for the specific language governing permissions and
#limitations under the Licence.


# set variables to be used
timestamp=$(date '+%y%m%d%H%M')
account_ids="/home/ubuntu/outputs/aws-accounts.txt.all"

working_directory="/home/ubuntu/outputs/aws_asset_inventory/20211017-output"

#backup_output_directory=$(echo "$output_directory"-)$(date +%F)

#[[ ! -d $backup_output_directory ]] && mkdir -p $backup_output_directory && echo $backup_output_directory

#cp -rf $output_directory $backup_output_directory

#rm -rf $output_directory
 
#mkdir -p $output_directory

#timestamp_start=$(date)
#echo "Script execution started @ $timestamp_start"
#echo "-------------------------------------------"

current_count=1

total_number_of_accounts=$(wc -l $account_ids | sed 's/ aws-accounts.txt//g')

while read -r line_text #account_id
do

  account_id=$(echo $line_text | cut -f1 -d\;)
  account_name=$(echo $line_text | cut -f2 -d\;)
  #echo "Account id: $account_id Account name: $account_name"
  
  #echo "current count: $current_count of $total_number_of_accounts"

  #####################################################
  ##  Looping through active regions for current account
  #####################################################
  for region in $(aws ec2 describe-regions  | jq '.Regions[] | select(.OptInStatus=="opt-in-not-required") | .RegionName' --raw-output); do
    #echo "Region: $region"

    current_directory=$(echo "$working_directory/$account_id/$region")
    #echo $current_directory
 
  #####################################################
  ##  EC 2 Elastic IP(s) fetch 
  #####################################################

    current_service=$(echo "EC2")
    result=$(jq -r '.Reservations[].OwnerId' $current_directory/ec2.json | sort | uniq -c | sed -e "s/  / /g"| sed -e "s/  / /g"| sed -e "s/  / /g"| sed -e "s/ /;/g" | sed "s/^/$current_service/" | sed "s/$/;$region/")



    [ -z $result ] && echo "$current_service;0;$account_id;$region" || echo $result


#    result=$(jq -r '.Reservations[].OwnerId' $current_directory/ec2.json | sort | uniq -c | sed -e "s/  / /g"| sed -e "s/  / /g"| sed -e "s/  / /g"| sed -e "s/ /;/g" | sed "s/^/EC2/" | sed "s/$/;$region/")
  
#    [ -z $result ] && echo "EC2;0;$account_id;$region" || echo $result
  #####################################################
  ##   Elastic Load Balancer(s) (ELB) fetch 
  #####################################################

    unset result
    current_service=$(echo "ELB") 

    result=$(jq -r '.LoadBalancerDescriptions | length' $current_directory/elb.json | sed -e "s/  / /g"| sed -e "s/  / /g"| sed -e "s/  / /g"| sed -e "s/ /;/g" | sed "s/^/$current_service;/" | sed "s/$/;$account_id;$region/")
    [ -z $result ] && echo "$current_service;0;$account_id;$region" || echo $result



  #####################################################
  ##  CloudFront fetch 
  #####################################################

  #####################################################
  ##  RDS fetch 
  #####################################################

    #DBClusters
    unset result
    current_service=$(echo "RDS_cluster")

    result=$(jq -r '.DBClusters | length' $current_directory/rds_cluster.json | sed -e "s/  / /g"| sed -e "s/  / /g"| sed -e "s/  / /g"| sed -e "s/ /;/g" | sed "s/^/$current_service;/" | sed "s/$/;$account_id;$region/")
    [ -z $result ] && echo "$current_service;0;$account_id;$region" || echo $result
    
    #DBInstances
    unset result
    current_service=$(echo "RDS_instance")

    result=$(jq -r '.DBInstances | length' $current_directory/rds_instances.json | sed -e "s/  / /g"| sed -e "s/  / /g"| sed -e "s/  / /g"| sed -e "s/ /;/g" | sed "s/^/$current_service;/" | sed "s/$/;$account_id;$region/")
    [ -z $result ] && echo "$current_service;0;$account_id;$region" || echo $result
  

  #####################################################
  ##  Redshift fetch 
  #####################################################

    #TODO

  #####################################################
  ##  Elastic Cluster Service (ECS) fetch
  #####################################################
    unset result
    current_service=$(echo "ECS")

    result=$(jq -r '.clusters | length' $current_directory/ecs.json | sed -e "s/  / /g"| sed -e "s/  / /g"| sed -e "s/  / /g"| sed -e "s/ /;/g" | sed "s/^/$current_service;/" | sed "s/$/;$account_id;$region/")
    [ -z $result ] && echo "$current_service;0;$account_id;$region" || echo $result


  #end of region loop
  done

  #####################################################
  ##  Elastic Kubernetes Service (EKS) fetch
  #####################################################

  unset result
  current_service=$(echo "EKS")

  result=$(jq -r '.clusters | length' $current_directory/eks.json | sed -e "s/  / /g"| sed -e "s/  / /g"| sed -e "s/  / /g"| sed -e "s/ /;/g" | sed "s/^/$current_service;/" | sed "s/$/;$account_id;$region/")
  [ -z $result ] && echo "$current_service;0;$account_id;$region" || echo $result


  #####################################################
  ##  Config fetch
  #####################################################

  unset result
  current_service=$(echo "Config")

  result=$(cat $current_directory/config.json | grep "recorder: ON" | wc -l | sed "s/^/$current_service;/" | sed "s/$/;$account_id;ALL/")
  [ -z $result ] && echo "$current_service;0;$account_id;ALL" || echo $result


  #####################################################
  ##  S3 fetch
  #####################################################

  unset result
  current_service=$(echo "S3")

  result=$(jq -r '.Buckets | length' $current_directory/s3.json | sed -e "s/  / /g"| sed -e "s/  / /g"| sed -e "s/  / /g"| sed -e "s/ /;/g" | sed "s/^/$current_service;/" | sed "s/$/;$account_id;ALL/")
  [ -z $result ] && echo "$current_service;0;$account_id;ALL" || echo $result


  current_count=$(($current_count + 1))

done < "$account_ids"

#timestamp_end=$(date)
#echo "Script execution ended @ $timestamp_end"

#echo "Storing results to S3 bucket"
#aws s3 cp "$output_file" s3://ist-vm/exports/

