#Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
#the European Commission - subsequent versions of the EUPL (the "Licence");
#You may not use this work except in compliance with the Licence.
#You may obtain a copy of the Licence at:
#
#   https://joinup.ec.europa.eu/software/page/eupl
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the Licence is distributed on an "AS IS" basis,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the Licence for the specific language governing permissions and
#limitations under the Licence.

import csv
import json
import time
import sys
import boto3
from datetime import datetime, timedelta
from botocore.exceptions import ClientError
import requests

#checking input arguments
print("\n---> Checking input arguments")
if len(sys.argv) == 1:
    print("\n-----> No argument provided. Exiting.")
    sys.exit()
else:
    ASSET_DELETION_LIST_FILE = sys.argv[1]


TENABLE_REGION = 'eu-central-1'
TENABLE_SECRET_KEY_NAME = 'prod/TenableAPI/S1_VA_AWS_ScriptRunner'
#ENDPOINTS_FILENAME = '{0}-public_ip_address.csv'.format(ACCOUNT_TYPE.lower())
#ENDPOINTS_FILE = '/home/ubuntu/outputs/' + ENDPOINTS_FILENAME

#ASSET_TAG = ACCOUNT_TYPE.upper()
#ASSET_DATA_SOURCE = "{0}-script".format(ACCOUNT_TYPE.upper())

WAIT_TIME_BETWEEN_CALLS = 5
WAIT_TIME_BETWEEN_ITERATIONS = 15
WAIT_TIME_ASSET_PROPAGATION = 300
NUMBER_OF_ITERATIONS = 5
LAST_SEEN_MAX_AGE_IN_DAYS = 30

#starting execution of the script
startDateTime = datetime.today()

print("\n---> Script execution started @ {0} ...\n".format(startDateTime))

try:
    #reading credentials information from aws secret manager
    print("---> Fetching TENABLE credentials")

    session = boto3.session.Session()
    client = session.client(
        service_name='secretsmanager',
        region_name=TENABLE_REGION,
    )

    try:
        get_secret_value_response = client.get_secret_value(
            SecretId=TENABLE_SECRET_KEY_NAME
        )
    except ClientError as e:
        if e.response['Error']['Code'] == 'ResourceNotFoundException':
            print("The requested secret " + secret_name + " was not found")
        elif e.response['Error']['Code'] == 'InvalidRequestException':
            print("The request was invalid due to:", e)
        elif e.response['Error']['Code'] == 'InvalidParameterException':
            print("The request had invalid params:", e)
        elif e.response['Error']['Code'] == 'DecryptionFailure':
            print("The requested secret can't be decrypted using the provided KMS key:", e)
        elif e.response['Error']['Code'] == 'InternalServiceError':
            print("An error occurred on service side:", e)
    else:
        # Secrets Manager decrypts the secret value using the associated KMS CMK
        # Depending on whether the secret was a string or binary, only one of these fields will be populated
        if 'SecretString' in get_secret_value_response:
            #convert result string encoded as key value JSON payload into Python list
            secretDataText = get_secret_value_response['SecretString']
            secretDataList = json.loads(secretDataText)
            TENABLE_ACCESS_KEY = secretDataList['accessKey']
            TENABLE_SECRET_KEY = secretDataList['secretKey']
        else:
            #binary output is not compatible
            raise Exception("Tenable API secret returned in binary. Script execution cannot continue.")

    #setting tenable access key string for API calls
    TENABLE_API_KEY="accessKey={0};secretKey={1}".format(TENABLE_ACCESS_KEY, TENABLE_SECRET_KEY)

    print("---> TENABLE Credentials successfuly fetched")

#############################################################
#
# creating list of assets to be deleted
#
#############################################################

    print("\n---> Creating list of assets to be deleted")

    deletionSetList = list()

    with open(ASSET_DELETION_LIST_FILE, mode='r') as csvFile:
        csvReader = csv.reader(csvFile, delimiter=';')
        for asset in csvReader:
            deletionSet = { "field": "host.id", "operator": "eq", "value": asset[0] }
            deletionSetList.append(deletionSet)
    csvFile.close()

    print("---> Creation of list of assets to be deleted successfully finished")

#############################################################
#
# deleting assets from Tenable
#
#############################################################

    print("\n---> Deleting assets from Tenable")

    print("------>Assets to be deleted payload:")
    print(deletionSetList)
    print(len(deletionSetList))

    if len(deletionSetList) != 0:
        apiUrl = "https://cloud.tenable.com/api/v2/assets/bulk-jobs/delete"

        payload = {"query": { "or": deletionSetList}, "hard_delete": True }

        headers = {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "X-ApiKeys": TENABLE_API_KEY
        }

        response = requests.request("POST", apiUrl, json=payload, headers=headers)

        if response.status_code != 202:
            errorText="Error occured while deleting assets from Tenable (API return code {0})".format(response.text)
            raise Exception(errorText)

        assetsDeleteResponse=json.loads(response.text)

        print("---> Deleting assets from Tenable successfully completed. Total of {0} assets deleted.".format(assetsDeleteResponse['response']['data']['asset_count']))

    else:
        print("---> Deleting assets skipped since no assets found for deletion.")

except Exception as e:
    print("ERROR> {0}".format(e))
    ERROR_MESSAGE = format(e)

#finishing execution of the script
endDateTime = datetime.today()
scriptExecutionTime = endDateTime - startDateTime

print("\n---> Script execution ended @ {0}".format(endDateTime))
print("\n---> Total execution time: {0}\n".format(scriptExecutionTime))
