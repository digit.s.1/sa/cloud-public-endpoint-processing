#Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
#the European Commission - subsequent versions of the EUPL (the "Licence");
#You may not use this work except in compliance with the Licence.
#You may obtain a copy of the Licence at:
#
#   https://joinup.ec.europa.eu/software/page/eupl
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the Licence is distributed on an "AS IS" basis,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the Licence for the specific language governing permissions and
#limitations under the Licence.

import csv
import json
import time
import sys
import boto3
from datetime import datetime
from botocore.exceptions import ClientError
import requests

TENABLE_REGION = 'eu-central-1'
TENABLE_SECRET_KEY_NAME = 'prod/TenableAPI/S1_VA_AWS_ScriptRunner'
AWS_ENDPOINTS_FILENAME = 'aws-public_ip_address.csv'
AWS_ENDPOINTS_FILE = '/home/ubuntu/outputs/' + AWS_ENDPOINTS_FILENAME
AZURE_ENDPOINTS_FILENAME = 'azure-public_ip_address.csv'
AZURE_ENDPOINTS_FILE = '/home/ubuntu/outputs/' + AZURE_ENDPOINTS_FILENAME

WAIT_TIME_BETWEEN_CALLS = 5
WAIT_TIME_BETWEEN_ITERATIONS = 15
NUMBER_OF_ITERATIONS = 5
TOPIC_ARN = 'arn:aws:sns:eu-central-1:042034201921:tenable-assets-not-seen-tagging'
TAG_VALUE_LAST_SEEN_WITHIN_LAST_24H_ALL = "WITHIN_LAST_24H"
TAG_VALUE_LAST_SEEN_WITHIN_LAST_24H_AWS = "WITHIN_LAST_24H-AWS"
TAG_VALUE_LAST_SEEN_WITHIN_LAST_24H_AZURE = "WITHIN_LAST_24H-AZURE"

#starting execution of the script
startDateTime = datetime.today()

print("\n---> Script execution started @ {0} ...\n".format(startDateTime))

try:
    #reading credentials information from aws secret manager
    print("---> Fetching TENABLE credentials")

    session = boto3.session.Session()
    client = session.client(
        service_name='secretsmanager',
        region_name=TENABLE_REGION,
    )

    try:
        get_secret_value_response = client.get_secret_value(
            SecretId=TENABLE_SECRET_KEY_NAME
        )
    except ClientError as e:
        if e.response['Error']['Code'] == 'ResourceNotFoundException':
            print("The requested secret " + TENABLE_SECRET_KEY_NAME + " was not found")
        elif e.response['Error']['Code'] == 'InvalidRequestException':
            print("The request was invalid due to:", e)
        elif e.response['Error']['Code'] == 'InvalidParameterException':
            print("The request had invalid params:", e)
        elif e.response['Error']['Code'] == 'DecryptionFailure':
            print("The requested secret can't be decrypted using the provided KMS key:", e)
        elif e.response['Error']['Code'] == 'InternalServiceError':
            print("An error occurred on service side:", e)
    else:
        # Secrets Manager decrypts the secret value using the associated KMS CMK
        # Depending on whether the secret was a string or binary, only one of these fields will be populated
        if 'SecretString' in get_secret_value_response:
            #convert result string encoded as key value JSON payload into Python list
            secretDataText = get_secret_value_response['SecretString']
            secretDataList = json.loads(secretDataText)
            TENABLE_ACCESS_KEY = secretDataList['accessKey']
            TENABLE_SECRET_KEY = secretDataList['secretKey']
        else:
            #binary output is not compatible
            raise Exception("Tenable API secret returned in binary. Script execution cannot continue.")

    #setting tenable access key string for API calls
    TENABLE_API_KEY="accessKey={0};secretKey={1}".format(TENABLE_ACCESS_KEY, TENABLE_SECRET_KEY)

    print("---> TENABLE Credentials successfuly fetched")

#############################################################
#
# fetching TAG uuids from Tenable
#
#############################################################

    print("\n---> Exporting tag uuids from Tenable started")

    print("-----> Exporting tag uuid for tag value '{0}' from Tenable".format(TAG_VALUE_LAST_SEEN_WITHIN_LAST_24H_ALL))

    apiUrl = "https://cloud.tenable.com/tags/values?f=value%3Aeq%3A{0}".format(TAG_VALUE_LAST_SEEN_WITHIN_LAST_24H_ALL)

    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "X-ApiKeys": TENABLE_API_KEY
    }

    response = requests.request("GET", apiUrl, headers=headers)

    if response.status_code != 200:
        errorText="Error occured while fetching tag uuids from Tenable (API return code {0})".format(response.text)
        raise Exception(errorText)

    discoveryWithin24hTag = json.loads(response.text)
    discoveryWithin24hTagUuid = discoveryWithin24hTag['values'][0]['uuid']

    print("-----> Exporting tag uuid finished")

    print("-----> Exporting tag uuid for tag value '{0}' from Tenable".format(TAG_VALUE_LAST_SEEN_WITHIN_LAST_24H_AWS))

    apiUrl = "https://cloud.tenable.com/tags/values?f=value%3Aeq%3A{0}".format(TAG_VALUE_LAST_SEEN_WITHIN_LAST_24H_AWS)

    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "X-ApiKeys": TENABLE_API_KEY
    }

    response = requests.request("GET", apiUrl, headers=headers)

    if response.status_code != 200:
        errorText="Error occured while fetching tag uuids from Tenable (API return code {0})".format(response.text)
        raise Exception(errorText)

    discoveryWithin24hTagAws = json.loads(response.text)
    discoveryWithin24hTagAwsUuid = discoveryWithin24hTagAws['values'][0]['uuid']

    print("-----> Exporting tag uuid finished")

    print("-----> Exporting tag uuid for tag value '{0}' from Tenable".format(TAG_VALUE_LAST_SEEN_WITHIN_LAST_24H_AZURE))

    apiUrl = "https://cloud.tenable.com/tags/values?f=value%3Aeq%3A{0}".format(TAG_VALUE_LAST_SEEN_WITHIN_LAST_24H_AZURE)

    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "X-ApiKeys": TENABLE_API_KEY
    }

    response = requests.request("GET", apiUrl, headers=headers)

    if response.status_code != 200:
        errorText="Error occured while fetching tag uuids from Tenable (API return code {0})".format(response.text)
        raise Exception(errorText)

    discoveryWithin24hTagAzure = json.loads(response.text)
    discoveryWithin24hTagAzureUuid = discoveryWithin24hTagAzure['values'][0]['uuid']

    print("-----> Exporting tag uuid finished")

#    print("All uuid: {0}".format(discoveryWithin24hTagUuid))
#    print("AWS uuid: {0}".format(discoveryWithin24hTagAwsUuid))
#    print("Azure uuid: {0}".format(discoveryWithin24hTagAzureUuid))

    print("---> Exporting tag uuids from Tenable sucessfully finished")

#############################################################
#
# exporting assets from Tenable
#
#############################################################

    print("\n---> Exporting assets from Tenable")

    apiUrl = "https://cloud.tenable.com/assets/export"


    payload = {
        "filters": {"tag.CLOUD-PROVIDER": ["AWS", "AZURE"]},
        "chunk_size": 5000
    }
    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "X-ApiKeys": TENABLE_API_KEY
    }

    response = requests.request("POST", apiUrl, json=payload, headers=headers)

    if response.status_code != 200:
        errorText="Error occured while exporting assets from Tenable (API return code {0})".format(response.text)
        raise Exception(errorText)

    assetsExportResponse=json.loads(response.text)


#############################################################
#
# fetching assets export status from Tenable
#
#############################################################

    print("\n---> Fetching assets export status")

    print("\n---> Waiting for {0} seconds before continuing with next API call".format(WAIT_TIME_BETWEEN_CALLS))
    time.sleep(WAIT_TIME_BETWEEN_CALLS)

    apiUrl = "https://cloud.tenable.com/assets/export/{0}/status".format(assetsExportResponse['export_uuid'])

    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "X-ApiKeys": TENABLE_API_KEY
    }

    print("---> Calling vulnerabilities export status API")
    for i in range(0, NUMBER_OF_ITERATIONS):
        print("-----> Iteration {0} of {1}".format(i+1, NUMBER_OF_ITERATIONS))
        response = requests.request("GET", apiUrl, headers=headers)

        if response.status_code != 200:
            errorText="Error occured while fetching assets export status from Tenable (API return code {0})".format(response.text)
            raise Exception(errorText)

        assetsExportStatusResponse=json.loads(response.text)

        if assetsExportStatusResponse['status'] == "FINISHED":
            print("-----> Call successfuly finished!")
            break

        print("-----> Waiting for {0} seconds before continuing with next iteration step (API call)".format(WAIT_TIME_BETWEEN_ITERATIONS))
        time.sleep(WAIT_TIME_BETWEEN_ITERATIONS)

    #if the loops finished and execution did'n finish, raise expection and dump last execution results
    if assetsExportStatusResponse['status'] != "FINISHED":
        errorText="Error occured while fetching status for assets export. Response dump: {0}".format(response.text)
        raise Exception(errorText)


#############################################################
#
# fetching assets export chunks from Tenable
#
#############################################################

    print("\n---> Downloading assets export chunks")

    print("\n---> Waiting for {0} seconds before continuing with next API call".format(WAIT_TIME_BETWEEN_CALLS))
    time.sleep(WAIT_TIME_BETWEEN_CALLS)
    assetsData = list()

    for currentChunk in assetsExportStatusResponse['chunks_available']:

        print("\n-----> Initiating download of chunk {0} of {1}".format(currentChunk,len(assetsExportStatusResponse['chunks_available'])))
        apiUrl = "https://cloud.tenable.com/assets/export/{0}/chunks/{1}".format(assetsExportResponse['export_uuid'], currentChunk)

        headers = {
            "Accept": "application/json",
            "X-ApiKeys": TENABLE_API_KEY
        }

        print("-----> Calling asset chunk export API")

        for i in range(0, NUMBER_OF_ITERATIONS):
            print("-------> Iteration {0} of {1}".format(i+1, NUMBER_OF_ITERATIONS))
            response = requests.request("GET", apiUrl, headers=headers)

            if response.status_code != 200:
                errorText="Error occured while fetching assets chunk from Tenable (API return code {0})".format(response.text)
                raise Exception(errorText)
            else:
                print("-------> Chunk {0} started downloading ...".format(currentChunk))
                assetsData.extend(json.loads(response.text))
                print("-------> Chunk {0} sucessfuly downloaded!".format(currentChunk))
                break

    print("---> Tenable assets successfuly downloaded")


#############################################################
#
# fetching cloud endpoint information into list
#
#############################################################

    print("\n---> Fetching cloud endpoint information into list")

    print("---> Opening AWS cloud endpoint file {0} for reading".format(AWS_ENDPOINTS_FILE))

    with open(AWS_ENDPOINTS_FILE, mode='r') as csvFile:
        print("-----> Reading file started ...".format(AWS_ENDPOINTS_FILE))
        csvReader = csv.reader(csvFile, delimiter=';')
        print("-----> Reading file finished!".format(AWS_ENDPOINTS_FILE))
        print("-----> Reading file into list started ...".format(AWS_ENDPOINTS_FILE))
        awsEndpointList = list(csvReader)
        print("-----> Reading file into list finished!".format(AWS_ENDPOINTS_FILE))

    print("---> Opening AZURE cloud endpoint file {0} for reading".format(AZURE_ENDPOINTS_FILE))

    with open(AZURE_ENDPOINTS_FILE, mode='r') as csvFile:
        print("-----> Reading file started ...".format(AZURE_ENDPOINTS_FILE))
        csvReader = csv.reader(csvFile, delimiter=';')
        print("-----> Reading file finished!".format(AZURE_ENDPOINTS_FILE))
        print("-----> Reading file into list started ...".format(AZURE_ENDPOINTS_FILE))
        azureEndpointList = list(csvReader)
        print("-----> Reading file into list finished!".format(AZURE_ENDPOINTS_FILE))

#############################################################
#
# creating list of assets for tagging
#
#############################################################

    print("\n---> Creation of seen and not seen assets list started")

    assetsSeenList = list()
    assetsSeenListAws = list()
    assetsSeenListAzure = list()

    assetsNotSeenList = list()
    assetsAll = list()
    tenableAssetCount = 0
    for tenableAsset in assetsData:
        assetsAll.append(tenableAsset['id'])
#        print("{0};{1}".format(tenableAsset['id'], tenableAsset['aws_ec2_instance_id']))
        tenableAssetCount += 1
        assetSeenFlag = None
        for awsEndpoint in awsEndpointList:
            if tenableAsset['aws_ec2_instance_id'] != None and awsEndpoint[4] != None:
                if tenableAsset['aws_ec2_instance_id'].lower() == awsEndpoint[4].lower():
                    assetsSeenList.append(tenableAsset['id'])
                    assetsSeenListAws.append(tenableAsset['id'])
                    assetSeenFlag = True
                    break

        if assetSeenFlag != True:
            for azureEndpoint in azureEndpointList:
                if tenableAsset['aws_ec2_instance_id'] != None and azureEndpoint[4] != None:
                    if tenableAsset['aws_ec2_instance_id'].lower() == azureEndpoint[4].lower():
                        assetsSeenList.append(tenableAsset['id'])
                        assetsSeenListAzure.append(tenableAsset['id'])
                        assetSeenFlag = True
                        break

        if assetSeenFlag != True:
            assetsNotSeenList.append(tenableAsset['id'])

    print("-----> Asset - seen list count: {0}".format(len(assetsSeenList)))
    print("-----> Asset - AWS - seen list count: {0}".format(len(assetsSeenListAws)))
    print("-----> Asset - Azure - seen list count: {0}".format(len(assetsSeenListAzure)))
    print("-----> Asset - not seen list count: {0}".format(len(assetsNotSeenList)))
    print("-----> Asset - tenable export list count: {0}".format(tenableAssetCount))
    print("-----> Asset - all list count: {0}".format(len(assetsAll)))

    print("---> Creation of seen and not seen assets list successfuly finished")

#    print("Not Seen List:")
#    for ans in assetsNotSeenList:
#        print(ans)

#    print("All List:")
#    for ans in assetsAll:
#        print(ans)

#    sys.exit()

#############################################################
#
# removing not seen tag from all assets in Tenable
#
#############################################################

    print("\n---> Removing seen tags from all assets in Tenable started")


    print("-----> Removing tag '{0}' from all assets in Tenable started".format(TAG_VALUE_LAST_SEEN_WITHIN_LAST_24H_ALL))

    apiUrl = "https://cloud.tenable.com/tags/assets/assignments"

    payload = {
        "assets": assetsAll,
        "tags": ["{0}".format(discoveryWithin24hTagUuid)],
        "action": "remove"
    }

    #print(json.dumps(payload, indent=3))

    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "X-ApiKeys": TENABLE_API_KEY
    }

    response = requests.request("POST", apiUrl, json=payload, headers=headers)

    if response.status_code != 200:
        errorText="Error occurred while removing not seen tag from all assets in Tenable (API return code {0})".format(response.text)
        raise Exception(errorText)

    print("-----> Removing tag '{0}' from all assets in Tenable finished".format(TAG_VALUE_LAST_SEEN_WITHIN_LAST_24H_ALL))

    print("-----> Removing tag '{0}' from all assets in Tenable started".format(TAG_VALUE_LAST_SEEN_WITHIN_LAST_24H_AWS))

    apiUrl = "https://cloud.tenable.com/tags/assets/assignments"

    payload = {
        "assets": assetsAll,
        "tags": ["{0}".format(discoveryWithin24hTagAwsUuid)],
        "action": "remove"
    }

    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "X-ApiKeys": TENABLE_API_KEY
    }

    response = requests.request("POST", apiUrl, json=payload, headers=headers)

    if response.status_code != 200:
        errorText="Error occured while removing not seen tag from all assets in Tenable (API return code {0})".format(response.text)
        raise Exception(errorText)

    print("-----> Removing tag '{0}' from all assets in Tenable finished".format(TAG_VALUE_LAST_SEEN_WITHIN_LAST_24H_AWS))

    print("-----> Removing tag '{0}' from all assets in Tenable started".format(TAG_VALUE_LAST_SEEN_WITHIN_LAST_24H_AZURE))

    apiUrl = "https://cloud.tenable.com/tags/assets/assignments"

    payload = {
        "assets": assetsAll,
        "tags": ["{0}".format(discoveryWithin24hTagAzureUuid)],
        "action": "remove"
    }

    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "X-ApiKeys": TENABLE_API_KEY
    }

    response = requests.request("POST", apiUrl, json=payload, headers=headers)

    if response.status_code != 200:
        errorText="Error occured while removing not seen tag from all assets in Tenable (API return code {0})".format(response.text)
        raise Exception(errorText)

    print("-----> Removing tag '{0}' from all assets in Tenable finished".format(TAG_VALUE_LAST_SEEN_WITHIN_LAST_24H_AZURE))

    print("\n---> Removing seen tags from all assets in Tenable successfully finished")


#############################################################
#
# fetching assets export status from Tenable
#
#############################################################

# this API is not ready on Tenable site, therefore only 4x the usual wait time will be applied
# to make synthetic wait for execution

#    print("\n---> Fetching assets tag removal job status")

    print("\n---> Waiting for {0} seconds before continuing with next API call".format(4*WAIT_TIME_BETWEEN_CALLS))
    time.sleep(4*WAIT_TIME_BETWEEN_CALLS)

#    apiUrl = "https://cloud.tenable.com/tags/assets/assignments/{0}/status".format(removeTagFromAllAssetsResponse['job_uuid'])

#    headers = {
#        "Accept": "application/json",
#        "Content-Type": "application/json",
#        "X-ApiKeys": TENABLE_API_KEY
#    }

#    print("---> Calling tag removal job status API")
#    for i in range(0, NUMBER_OF_ITERATIONS):
#        print("-----> Iteration {0} of {1}".format(i+1, NUMBER_OF_ITERATIONS))
#        response = requests.request("GET", apiUrl, headers=headers)
#        print(response)
#        if response.status_code != 200:
#            errorText="Error occured while fetching asset tag removal status from Tenable (API return code {0})".format(response.text)
#            raise Exception(errorText)

#        renoveTagFromAllAssetsStatusResponse=json.loads(response.text)

#        if renoveTagFromAllAssetsStatusResponse['status'] == "FINISHED":
#            print("-----> Call successfuly finished!")
#            break

#        print("-----> Waiting for {0} seconds before continuing with next iteration step (API call)".format(WAIT_TIME_BETWEEN_ITERATIONS))
#        time.sleep(WAIT_TIME_BETWEEN_ITERATIONS)

    #if the loops finished and execution did'n finish, raise expection and dump last execution results
#    if renoveTagFromAllAssetsStatusResponse['status'] != "FINISHED":
#        errorText="Error occured while fetching status for assets export. Response dump: {0}".format(response.text)
#        raise Exception(errorText)


#############################################################
#
# applying tag to seen assets in Tenable
#
#############################################################

    print("\n---> Adding seen tag to seen assets within last 24h in Tenable started")


    print("-----> Adding tag '{0}' to all seen assets within last 24h".format(TAG_VALUE_LAST_SEEN_WITHIN_LAST_24H_ALL))
    apiUrl = "https://cloud.tenable.com/tags/assets/assignments"

    payload = {
        "assets": assetsSeenList,
        "tags": ["{0}".format(discoveryWithin24hTagUuid)],
        "action": "add"
    }

    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "X-ApiKeys": TENABLE_API_KEY
    }

    response = requests.request("POST", apiUrl, json=payload, headers=headers)

    if response.status_code != 200:
        errorText="Error occured while adding not seen tag to non seen assets in Tenable (API return code {0})".format(response.text)
        raise Exception(errorText)

    print("-----> Adding tag finished")


    print("-----> Adding tag '{0}' to all seen assets within last 24h".format(TAG_VALUE_LAST_SEEN_WITHIN_LAST_24H_AWS))
    apiUrl = "https://cloud.tenable.com/tags/assets/assignments"

    payload = {
        "assets": assetsSeenListAws,
        "tags": ["{0}".format(discoveryWithin24hTagAwsUuid)],
        "action": "add"
    }

    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "X-ApiKeys": TENABLE_API_KEY
    }

    response = requests.request("POST", apiUrl, json=payload, headers=headers)

    if response.status_code != 200:
        errorText="Error occured while adding not seen tag to non seen assets in Tenable (API return code {0})".format(response.text)
        raise Exception(errorText)

    print("-----> Adding tag finished")


    print("-----> Adding tag '{0}' to all seen assets within last 24h".format(TAG_VALUE_LAST_SEEN_WITHIN_LAST_24H_AZURE))
    apiUrl = "https://cloud.tenable.com/tags/assets/assignments"

    payload = {
        "assets": assetsSeenListAzure,
        "tags": ["{0}".format(discoveryWithin24hTagAzureUuid)],
        "action": "add"
    }

    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "X-ApiKeys": TENABLE_API_KEY
    }

    response = requests.request("POST", apiUrl, json=payload, headers=headers)

    if response.status_code != 200:
        errorText="Error occured while adding not seen tag to non seen assets in Tenable (API return code {0})".format(response.text)
        raise Exception(errorText)

    print("-----> Adding tag finished")

    print("\n---> Adding seen tag to assets seen in last 24h Tenable successfully finished")

#############################################################
#
# sending SNS notification
#
#############################################################


    #sending notification that processing went well
    print("\n---> Send notification to SNS topic: {0}".format(TOPIC_ARN))

    sns = boto3.client('sns')

    messageToSend = "Tenable - not seen asset tagging is successfully finished ..."
    subjectToSend = "{0} - Tenable - not seen asset tagging - successfully finished".format(startDateTime.strftime("%Y-%m-%d"))

    sns.publish(TopicArn=TOPIC_ARN,
        Message = messageToSend,
        Subject = subjectToSend)

    print("---> SNS notification successfully sent!")

except Exception as e:
    print("ERROR> {0}".format(e))


#finishing execution of the script
endDateTime = datetime.today()
scriptExecutionTime = endDateTime - startDateTime

print("\n---> Script execution ended @ {0}".format(endDateTime))
print("\n---> Total execution time: {0}\n".format(scriptExecutionTime))
