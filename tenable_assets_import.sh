#!/bin/bash

#Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
#the European Commission - subsequent versions of the EUPL (the "Licence");
#You may not use this work except in compliance with the Licence.
#You may obtain a copy of the Licence at:
#
#   https://joinup.ec.europa.eu/software/page/eupl
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the Licence is distributed on an "AS IS" basis,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the Licence for the specific language governing permissions and
#limitations under the Licence.

#set the account list file and main parameters
[[ -z $1 ]] && echo "--->>>Parameters for the script not passed. Exiting." && exit
CLOUD_PROVIDER_NAME=$1
[[ ${CLOUD_PROVIDER_NAME,,} != "aws" ]] && [[ ${CLOUD_PROVIDER_NAME,,} != "azure" ]] && echo "--->>>Invalid parameters for the script not passed (valid values are: aws or azure). Exiting." && exit

asset_file="/home/ubuntu/outputs/${CLOUD_PROVIDER_NAME,,}-public_ip_address.csv"
error_log_file="/home/ubuntu/logs/tenable_assets_import.sh.error.log"
asset_tag="${CLOUD_PROVIDER_NAME^^}"
asset_data_source="${CLOUD_PROVIDER_NAME^^}-script"
curl_temp_file="/home/ubuntu/outputs/tenable_assets_import.sh.curl.tmp"
start_date_time=$(date)
maximum_number_of_tries=5
maximum_waiting_time_between_tries=30

tenableAPICreds=$(aws secretsmanager get-secret-value --secret-id prod/TenableAPI/S1_VA_AWS_ScriptRunner)
tenable_access_key=$(echo $tenableAPICreds | jq -r '.SecretString' | jq -r '.accessKey')
tenable_secret_key=$(echo $tenableAPICreds | jq -r '.SecretString' | jq -r '.secretKey')
tenable_propagation_wait_time=300
tenable_propagation_wait_time_export=120
SNS_ARN="arn:aws:sns:eu-central-1:042034201921:tenable-assets-import"

#inital printout of messages
echo ""
echo "--->>>Start execution of the script to import ${CLOUD_PROVIDER_NAME^^} assets and apply tags in Tenable"
echo "Started at: $start_date_time"
echo "Using asset file: $asset_file"
echo "Applying tag: $asset_tag"
echo "Applying asset data source: $asset_data_source"

#getting asset tag UUID from tenable api that we are going to apply later on
echo ""
echo "--->>>Fetching the UUID for the tag"
asset_tag_uuid=$(curl --request GET \
     --url "https://cloud.tenable.com/tags/values?f=value:eq:${CLOUD_PROVIDER_NAME^^}" \
     --header 'Accept: application/json' \
     --header "X-ApiKeys: accessKey=$tenable_access_key;secretKey=$tenable_secret_key" \
     --silent \
     | jq '.values[].uuid' | sed "s/\"//g")
echo "Asset tag uuid: $asset_tag_uuid"

#reading asset list file from external source in format ACCOUNT_ID;ACCOUNT_NAME;IP4;FQDN 
#into variables and generate string for tenable api call 
#echo ""
#echo "--->>>Loading asset file into array"
#asset_list=""
#IFS=';'
#while read -r asset_account_id asset_account_name asset_ip asset_fqdn
#do
#  echo "Account id: $asset_account_id Account name: $asset_account_name IP: $asset_ip FQDN: $asset_fqdn"
#  asset_list+="{\"fqdn\": [\"$asset_fqdn\"], \"ipv4\": [\"$asset_ip\"], \"aws_owner_id\": \"$asset_account_id\"}," 
#done < "$asset_file"
#
#[ "${asset_list: -1}" = "," ] && asset_list=${asset_list::-1}
#
#echo "Asset list: $asset_list"


#fetching list of imported assets from tenable which were imported.
#we are going to use this list to check with assets to be imported.
#All assets existing in tenable but not existing in import list
#are going to be deleted
echo ""
echo "--->>>Requesting export of assets from tennable"
checking_pass_number=0
while true
do
     echo "Execution pass: $checking_pass_number"
     asset_export_job_uuid=$(curl --request POST \
          --url 'https://cloud.tenable.com/assets/export' \
          --header 'Accept: application/json' \
          --header 'Content-Type: application/json' \
          --header "X-ApiKeys: accessKey=$tenable_access_key;secretKey=$tenable_secret_key" \
          --silent \
          --data "{\"filters\": { \"sources\": [ \"$asset_data_source\" ] },\"chunk_size\": 5000}" \
          | jq '.export_uuid' | sed "s/\"//g")
     echo "Asset export job uuid: $asset_export_job_uuid"
     [ $asset_export_job_uuid != "null" ] && break
     checking_pass_number=$(($checking_pass_number+1))
     [ $checking_pass_number -eq $maximum_number_of_tries ] && break
     sleep $maximum_waiting_time_between_tries
done

[ $asset_export_job_uuid = "null" ] && echo "" && echo "--->>>Could not retrieve assets to prevent import duplicates. Exiting script." && end_date_time=$(date) && echo "Script finished at: $end_date_time" && exit

#checking the status of exported assets job
echo ""
echo "--->>>Checking export job status of assets from tennable"
checking_pass_number=0
while true
do
     echo "Execution pass: ${checking_pass_number+1}"
     asset_export_job_data=$(curl --request GET \
          --url "https://cloud.tenable.com/assets/export/$asset_export_job_uuid/status" \
          --header 'Accept: application/json' \
          --header "X-ApiKeys: accessKey=$tenable_access_key;secretKey=$tenable_secret_key" \
          --silent \
          | jq '.')

     asset_export_job_data_status=$(echo $asset_export_job_data | jq '.status' | sed "s/\"//g")
     echo "Asset export job status: $asset_export_job_data_status"
     
     [ $asset_export_job_data_status = "FINISHED" ] && break
     #echo "export status: $asset_export_job_data_status
     checking_pass_number=$((checking_pass_number+1))
     [ $checking_pass_number -eq $maximum_number_of_tries ] && break
     sleep $maximum_waiting_time_between_tries
done

#if export did not finished in maximum number of tries, exit from the scrip gracefully
[ $asset_export_job_data_status != "FINISHED" ] && echo "" && echo "--->>>Could not export data from tenable. Exiting script." && end_date_time=$(date) && echo "Script finished at: $end_date_time" && exit

echo "Export finished"

#getting exported chunks
asset_export_job_data_chunks=$(echo $asset_export_job_data | jq '.chunks_available' | sed "s/\"//g" | tr -d '\n[]' | sed 's/ *$//g' | sed 's/^ *//g')
echo "Chunks available: $asset_export_job_data_chunks"


#getting ipv4s and fqdns
echo ""
echo "--->>>Fetching export data (IDs, IPv4s, FQDNs)"


checking_pass_number=0
[[ ! -z $asset_export_job_data_chunks ]] && IFS=' ' read -r -a chunk_array <<< "$asset_export_job_data_chunks" || echo "Nothing found in Tenable ..."
for current_chunk in "${chunk_array[@]}"
do
     echo "Getting chunk: ${checking_pass_number+1}"
     asset_export_data=$(curl --request GET \
          --url "https://cloud.tenable.com/assets/export/$asset_export_job_uuid/chunks/$current_chunk" \
          --header 'Accept: application/json' \
          --header "X-ApiKeys: accessKey=$tenable_access_key;secretKey=$tenable_secret_key" \
          --silent \
          | jq '.')
          #| jq '.' | jq '.[] | [.id, .ipv4s, .fqdns] | flatten | ascii_downcase')

     checking_pass_number=$((checking_pass_number+1))
done

echo "--->>>Creating deletion list from tenable"

#fetching the import data
asset_file_data=$(cat $asset_file)

#converting to lowercase
asset_file_data=$(echo "${asset_file_data,,}")

asset_export_data=$(echo "$asset_export_data" | jq 'map( if (.data_node.name != null) then .data_node.name |= ascii_downcase else . end )')

#looping through tenable export data and trying to find the match in import data
current_pass=1
for i in $(echo $asset_export_data | jq -c '.[]' | sed "s/ /_/g"); do

   #extracting exported data into variables to search for in import data
   current_id=$(echo $i | jq '.id' | tr -d '"' | sed "s/ //g")
   current_ipv4=$(echo $i | jq '.ipv4s | flatten' | tr -d '\n"[]' | sed "s/^ *//g" | sed "s/ *$//g" | sed "s/ //g")
   current_fqdn=$(echo $i | jq '.fqdns | flatten' | tr -d '\n"[]' | sed "s/^ *//g" | sed "s/ *$//g" | sed "s/ //g")

   echo "----NEXT TENABLE ASSET----"
   echo "Current id: $current_id"
   echo "Current ipv4: $current_ipv4"
   echo "Current fqdn: $current_fqdn"

   [ -z $current_id ] && echo "ID empty!"
   [ -z $current_ipv4 ] && echo "IP empty!"
   [ -z $current_fqdn ] && echo "FQDN empty!"

   [ -z $current_id ] && echo "current record: $i" && echo "Since ID is empty, skipping this row ..." && continue

   single_delete_template="\"field\": \"host.id\", \"operator\": \"eq\", \"value\": \"$current_id\""
   multiple_delete_template="{ \"field\": \"host.id\", \"operator\": \"eq\", \"value\": \"$current_id\" },"

   #skipping new asset entries for the assets that are already present in tenable

   [ $current_pass -eq 1 ] && [[ ! -z "$current_ipv4" ]] && [[ ! -z "$current_fqdn" ]] && [[ ! $asset_file_data == *"$current_ipv4"* ]] && [[ ! $asset_file_data == *"$current_fqdn"* ]] && echo "IP: $current_ipv4 and FQDN: $current_fqdn does not exist in assets to be imported but exists in tenable: asset marked for deletion in tenable..." && tenable_delete_single="${single_delete_template}" && current_pass=$((current_pass+1)) && continue

   [ $current_pass -eq 1 ] && [[ ! -z "$current_ipv4" ]] && [[ ! $asset_file_data == *"$current_ipv4"* ]] && echo "IP: $current_ipv4 does not exist in assets to be imported but exists in tenable: asset marked for deletion in tenable..." && tenable_delete_single="${single_delete_template}" && current_pass=$((current_pass+1)) && continue

   [ $current_pass -eq 1 ] && [[ ! -z "$current_fqdn" ]] && [[ ! $asset_file_data == *"$current_fqdn"* ]] && echo "FQDN: $current_fqdn does not exist in assets to be imported but exists in tenable: asset marked for deletion in tenable..." && tenable_delete_single="${single_delete_template}" && current_pass=$((current_pass+1)) && continue

   [[ ! -z "$current_ipv4" ]] && [[ ! -z "$current_fqdn" ]] && [[ ! $asset_file_data == *"$current_ipv4"* ]] && [[ ! $asset_file_data == *"$current_fqdn"* ]] && echo "IP: $current_ipv4 and FQDN: $current_fqdn does not exist in assets to be imported but exists in tenable: asset marked for deletion in tenable..." && tenable_delete_multiple+="${multiple_delete_template}" && current_pass=$((current_pass+1)) && continue

   [[ ! -z "$current_ipv4" ]] && [[ ! $asset_file_data == *"$current_ipv4"* ]] && echo "IP: $current_ipv4 does not exist in assets to be imported but exists in tenable: asset marked for deletion in tenable..." && tenable_delete_multiple+="${multiple_delete_template}" && current_pass=$((current_pass+1)) && continue

   [[ ! -z "$current_fqdn" ]] && [[ ! $asset_file_data == *"$current_fqdn"* ]] && echo "FQDN: $current_fqdn does not exist in assets to be imported but exists in tenable: asset marked for deletion in tenable..."  && tenable_delete_multiple+="${multiple_delete_template}" && current_pass=$((current_pass+1)) && continue

done

[ "${tenable_delete_multiple: -1}" = "," ] && tenable_delete_multiple=${tenable_delete_multiple::-1}


echo "Final deletion list single: $tenable_delete_single"
echo "Final deletion list multiple: $tenable_delete_multiple"

#creating final JSON structure
[[ ! -z "$tenable_delete_multiple" ]] && tenable_delete_multiple_final="\"or\": [$tenable_delete_multiple],"
[[ ! -z "$tenable_delete_single" ]] && tenable_delete_complete_final="{\"query\": { $tenable_delete_single }, \"hard_delete\": false }"
[[ ! -z "$tenable_delete_single" ]] && [[ ! -z "$tenable_delete_multiple" ]] && tenable_delete_complete_final="{\"query\": { $tenable_delete_multiple_final $tenable_delete_single }, \"hard_delete\": false }"
[[ ! -z "$tenable_delete_complete_final" ]] && echo "Final deletion list JSON: $tenable_delete_complete_final" || echo "Nothing to be deleted in tenable ..."

#deletion of the assets in tenable

echo "--->>>Deleting assets in tenable"
echo "--->>>DISABLED"

#calling the api to set the tags
unset asset_delete_job_uuid

#[[ ! -z "$tenable_delete_complete_final" ]] && asset_delete_job_uuid=$(curl --request POST \
#     --url "https://cloud.tenable.com/api/v2/assets/bulk-jobs/delete" \
#     --header 'Accept: application/json' \
#     --header 'Content-Type: application/json' \
#     --header "X-ApiKeys: accessKey=$tenable_access_key;secretKey=$tenable_secret_key" \
#     --silent \
#     --data "$tenable_delete_complete_final")


#checking if there is error returned from the executed bulk delete in tenable
#if so, display error, if not, show response


#[[ ! -z "$asset_delete_job_uuid" ]] && asset_delete_job_error=$(echo "$asset_delete_job_uuid" | jq '.response.error.title')
#[[ ! -z "$asset_delete_job_uuid" ]] && [[ ! "$asset_delete_job_error" = "null" ]] && echo "Error occured while deleting assets (error: $asset_delete_job_error). Aborting execution." && end_date_time=$(date) && echo "Script finished at: $end_date_time" && exit
#[[ ! -z "$asset_delete_job_uuid" ]] && echo "Assets deleted (job response body: $asset_delete_job_uuid)" 

#reading asset list file from external source in format ACCOUNT_ID;ACCOUNT_NAME;IP4;FQDN
#into variables and generate string for tenable api call

echo ""
echo "--->>>Loading asset file into import array"
asset_list=""
IFS=';'
while read -r asset_account_id asset_account_name asset_ip asset_fqdn asset_instance_id
do
  asset_fqdn=$(echo "${asset_fqdn,,}")
  echo "New row :> Account id: $asset_account_id Account name: $asset_account_name IP: $asset_ip FQDN: $asset_fqdn INSTANCE_ID: $asset_instance_id"
  #skipping new asset entries for the assets that are already present in tenable
##  [[ ! -z "$asset_ip" ]] && [[ $asset_export_data == *"$asset_ip"* ]] && echo "  IP: $asset_ip already exists in tenable assets, skipping..." && continue
##  [[ ! -z "$asset_fqdn" ]] && [[ $asset_export_data == *"$asset_fqdn"* ]] && echo "  FQDN: $asset_fqdn already exists in tenable assets, skipping..." && continue

  asset_list+="{\"fqdn\": [\"$asset_fqdn\"], \"ipv4\": [\"$asset_ip\"], \"aws_owner_id\": \"$asset_account_id\", \"aws_ec2_instance_id\": \"$asset_instance_id\"},"
done < "$asset_file"

[ "${asset_list: -1}" = "," ] && asset_list=${asset_list::-1}

echo "Final asset list to be imported: $asset_list"

[ -z "$asset_list" ] && echo "" && echo "--->>>Final import asset list is empty - nothing to be imported. Aborting execution." && end_date_time=$(date) && echo "Script finished at: $end_date_time" && exit

#creating temp file for curl command
$(echo "{\"assets\": [ $asset_list ], \"source\": \"$asset_data_source\" }" > "$curl_temp_file")

#importing assets into tenable
echo ""
echo "--->>>Importing assets in tenable"
asset_import_job_uuid=$(curl --request POST \
     --url 'https://cloud.tenable.com/import/assets' \
     --header 'Accept: application/json' \
     --header 'Content-Type: application/json' \
     --header "X-ApiKeys: accessKey=$tenable_access_key;secretKey=$tenable_secret_key" \
     --silent \
     -d @"$curl_temp_file" \
     | jq '.')
echo "Asset import job uuid: $asset_import_job_uuid"

##after silent above
#     --data "{\"assets\": [
#          $asset_list
#	  ], \"source\": \"$asset_data_source\" }" \


#deleting temp curl file
$(rm "$curl_temp_file")

asset_import_job_error=$(echo "$asset_import_job_uuid" | jq '.response.error.title')
[[ ! -z "$asset_import_job_error" ]] && [[ ! "$asset_import_job_error" = "null" ]] && echo "Error occured while importing assets (error: $asset_import_job_error). Aborting execution." && end_date_time=$(date) && echo "Script finished at: $end_date_time" && exit || echo "Assets imported (job response body: $asset_import_job_uuid)"

######CHECK IF IMPORT JOB FINISHED#################
#checking the status of imported assets
echo ""
echo "--->>>Checking import job status of assets from tennable"
asset_import_job_uuid_extracted=$(echo "$asset_import_job_uuid" | jq '.asset_import_job_uuid' | sed "s/\"//g")
checking_pass_number=0
unset asset_import_job_data
unset asset_import_job_data_status
while true
do
     echo "Execution pass: $checking_pass_number"
     asset_import_job_data=$(curl --request GET \
          --url "https://cloud.tenable.com/import/asset-jobs/$asset_import_job_uuid_extracted" \
          --header 'Accept: application/json' \
          --header "X-ApiKeys: accessKey=$tenable_access_key;secretKey=$tenable_secret_key" \
          --silent \
          | jq '.')

     asset_import_job_data_status=$(echo $asset_import_job_data | jq '.status' | sed "s/\"//g")

     echo "Asset import job status: $asset_import_job_data_status"
     #echo "Asset import job data: $asset_import_job_data"
     
     [[ "$asset_import_job_data_status" = "COMPLETE" ]] && break
     #echo "export status: $asset_export_job_data_status
     checking_pass_number=$((checking_pass_number+1))
     [ $checking_pass_number -eq $maximum_number_of_tries ] && break
     sleep $maximum_waiting_time_between_tries
done

#if export did not finished in maximum number of tries, exit from the script gracefully
[[ ! "$asset_import_job_data_status" = "COMPLETE" ]] && echo "" && echo "--->>>Could not import data to tenable. Aborting execution." && end_date_time=$(date) && echo "Script finished at: $end_date_time" && exit

echo "Asset import finished"

echo ""
echo "--->>>Waiting for the import to be propagated in tenable asset inventory"
sleep $tenable_propagation_wait_time

###################################################
#fetching list of imported assets from tenable which were imported to set tags

echo ""
echo "--->>>Requesting export of assets from tennable for tag setting"
checking_pass_number=0
unset asset_export_job_uuid
while true
do
     echo "Execution pass: $checking_pass_number"
     asset_export_job_uuid=$(curl --request POST \
          --url 'https://cloud.tenable.com/assets/export' \
          --header 'Accept: application/json' \
          --header 'Content-Type: application/json' \
          --header "X-ApiKeys: accessKey=$tenable_access_key;secretKey=$tenable_secret_key" \
          --silent \
          --data "{\"filters\": { \"sources\": [ \"$asset_data_source\" ] },\"chunk_size\": 5000}" \
          | jq '.export_uuid' | sed "s/\"//g")
     echo "Asset export job uuid: $asset_export_job_uuid"
     [ $asset_export_job_uuid != "null" ] && break
     checking_pass_number=$((checking_pass_number+1))
     [ $checking_pass_number -eq $maximum_number_of_tries ] && break
     sleep $maximum_waiting_time_between_tries
done

echo "--->>>Waiting for the export from tenable asset inventory to be ready"
sleep $tenable_propagation_wait_time_export

#checking the status of exported assets
echo ""
echo "--->>>Checking export status of assets from tennable"
checking_pass_number=0
unset asset_export_job_data
unset asset_export_job_data_status
while true
do
     echo "Execution pass: ${checking_pass_number+1}"
     asset_export_job_data=$(curl --request GET \
          --url "https://cloud.tenable.com/assets/export/$asset_export_job_uuid/status" \
          --header 'Accept: application/json' \
          --header "X-ApiKeys: accessKey=$tenable_access_key;secretKey=$tenable_secret_key" \
          --silent \
          | jq '.')

     asset_export_job_data_status=$(echo $asset_export_job_data | jq '.status' | sed "s/\"//g")
     echo "Asset export job status: $asset_export_job_data_status"

     [[ "$asset_export_job_data_status" = "FINISHED" ]] && break
     #echo "export status: $asset_export_job_data_status
     checking_pass_number=$((checking_pass_number+1))
     [ $checking_pass_number -eq $maximum_number_of_tries ] && break
     sleep $maximum_waiting_time_between_tries
done

#if export did not finished in maximum number of tries, exit from the scrip gracefully
[[ "$asset_export_job_data_status" != "FINISHED" ]] && echo "" && echo "--->>>Could not export data from tenable to set tags. Exiting script." && end_date_time=$(date) && echo "Script finished at: $end_date_time" && exit

echo "Export finished"

#getting exported chunks
unset asset_export_job_data_chunks
asset_export_job_data_chunks=$(echo $asset_export_job_data | jq '.chunks_available' | sed "s/\"//g" | tr -d '\n[]' | sed 's/ *$//g' | sed 's/^ *//g')
echo "Chunks available: $asset_export_job_data_chunks"

#getting ids to set tags to
echo ""
echo "--->>>Fetching export data (IDs)"

checking_pass_number=0
unset asset_export_data
IFS=' ' read -r -a chunk_array <<< "$asset_export_job_data_chunks"
for current_chunk in "${chunk_array[@]}"
do
     echo "Getting chunk: ${checking_pass_number+1}"
     asset_export_data=$(curl --request GET \
          --url "https://cloud.tenable.com/assets/export/$asset_export_job_uuid/chunks/$current_chunk" \
          --header 'Accept: application/json' \
          --header "X-ApiKeys: accessKey=$tenable_access_key;secretKey=$tenable_secret_key" \
          --silent \
          | jq '.' | jq '.[] | [.id] | flatten' | sed "s/]/],/g" | tr -d '\n[]')
     checking_pass_number=$((checking_pass_number+1))
done

[ "${asset_export_data: -1}" = "," ] && asset_export_data=${asset_export_data::-1}
echo "Assets export data (IDs): $asset_export_data"

echo ""
echo "--->>>Setting asset tag in tenable"

#setting the payload for api call
payload="{ \"assets\": [ $asset_export_data ], \"tags\": [ \"$asset_tag_uuid\" ], \"action\": \"add\" }"

#calling the api to set the tags
asset_tags_assignment_job_uuid=$(curl --request POST \
     --url "https://cloud.tenable.com/tags/assets/assignments" \
     --header 'Accept: application/json' \
     --header 'Content-Type: application/json' \
     --header "X-ApiKeys: accessKey=$tenable_access_key;secretKey=$tenable_secret_key" \
     --silent \
     --data "$payload" | jq '.job_uuid')

echo "Asset tag setting uuid: $asset_tags_assignment_job_uuid"

[ $asset_tags_assignment_job_uuid = "null" ] && echo "" && echo "--->>>Could not set tags for selected assets. Exiting script." && end_date_time=$(date) && echo "Script finished at: $end_date_time" && exit

echo ""
echo "--->>>Sending SNS notification ..."
timestamp_end=$(date '+%Y-%m-%d')
errorFileSize=$(wc -c "$error_log_file" | awk '{print $1}')
[[ ! $errorFileSize -eq 0 ]] && errorLog="Errors OCCURED during import. Please check logs." || errorLog="No errors found during import."
SNS_message=$(echo "Import of Tenable assets for ${CLOUD_PROVIDER_NAME^^} finished. $errorLog")
SNS_subject=$(echo "$timestamp_end - Import of Tenable assets for ${CLOUD_PROVIDER_NAME^^} - finished")
executeResult=$(aws sns publish --topic-arn "$SNS_ARN" --subject "$SNS_subject" --message "$SNS_message")

timestamp_end=$(date)
echo ""
echo "--->>>Script finished successfully for ${CLOUD_PROVIDER_NAME^^} assets!"
echo "Script finished at: $timestamp_end"
