#Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
#the European Commission - subsequent versions of the EUPL (the "Licence");
#You may not use this work except in compliance with the Licence.
#You may obtain a copy of the Licence at:
#
#   https://joinup.ec.europa.eu/software/page/eupl
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the Licence is distributed on an "AS IS" basis,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the Licence for the specific language governing permissions and
#limitations under the Licence.

import os
import requests
import configparser
import logging
from logging.handlers import RotatingFileHandler
import sys
import json
import boto3
import csv
import time
import datetime
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

INI_FILE = '/home/ubuntu/scripts/flam2serviceNow.ini'
LOG_FILE = '/home/ubuntu/logs/flam2serviceNow.log'
SERVICENOW_REGION = 'eu-central-1'
SERVICENOW_SECRET_KEY_NAME = 'prod/ServiceNowAPI/bmc_ftp_user'
TOPIC_ARN = 'arn:aws:sns:eu-central-1:042034201921:servicenow-file-upload'
errorHappened = None

# read the configuration ini file
def readConfig():
    global config
    config = configparser.ConfigParser()
    result = config.read(INI_FILE)
    if result == []:
        print('flam2serviceNow.ini configuration file not found. Please create one and place it in the directory '
              'where you are calling flam2serviceNow.')
        exit(-5)

# call read here for the config to be available from here downwards
readConfig()

# declare some constants
loggingLevels = { "debug": logging.DEBUG,
                  "info": logging.INFO,
                  "warning": logging.WARNING,
                  "error": logging.ERROR,
                  "critical": logging.CRITICAL }


class SN:
    def __init__(self, url, passwordNew):
        self.url = url
        self.username = config['sn']['attachment.username']
        self.password = passwordNew
        self.table_name = config['sn']['attachment.table_name']

        self.proxyDict = {
            "http": config['sn']['http_proxy'],
            "https": config['sn']['https_proxy']
        }

    def _Post(self, file_name, data, table_sys_id):
        global errorHappened
        try:
            # use retry loop
            retries = int(config['sn']['retries'])
            self.response = None
            while retries > 0:
                try:
                   self.response = requests.post(f'{self.url}/api/now/attachment/file?table_name={self.table_name}&table_sys_id={table_sys_id}&file_name={file_name}', 
                                            data=data, 
                                            headers={'Content-Type': 'text/csv', 'Accept': 'application/json'},
                                            auth=(self.username, self.password),
                                            proxies=self.proxyDict if config['sn']['use.proxy'] == 'True'
                                            else None)
                except:
                    sleepTime = int(config['sn']['sleep.time.if.no.access.minutes'])
                    logging.exception('SN does not seem to be returning a well formed response. '
                                      f'Maybe it is not accessible? Sleeping for {str(sleepTime)} minutes...')
                    time.sleep(sleepTime * 60)

                retries = retries - 1
                if self.response != None and (self.response.status_code == 200 or self.response.status_code == 201):
                    logging.info(f'HTTP status: {self.response.status_code}')
                    break
                elif self.response != None:
                    errorHappened = True
                    logging.warning(f'HTTP status: {self.response.status_code}')
            if (self.response == None or self.response.status_code != 200 and self.response.status_code != 201) and retries == 0:
                errorHappened = True
                logging.warning(f'Could not get valid answer. Retried {config["sn"]["retries"]} times.')
                return {}
            return self.response.json()
        except Exception as e:
            logging.exception('Error calling SN attachment')

    def _GetAttachmentsFor(self, table_sys_id):
        try:
            # use retry loop
            retries = int(config['sn']['retries'])
            self.response = None
            while retries > 0:
                try:
                   self.response = requests.get(f'{self.url}/api/now/attachment?table_name={self.table_name}&table_sys_id={table_sys_id}', 
                                              headers={'Accept': 'application/json'},
                                              auth=(self.username, self.password),
                                              proxies=self.proxyDict if config['sn']['use.proxy'] == 'True'
                                              else None)
                except:
                    sleepTime = int(config['sn']['sleep.time.if.no.access.minutes'])
                    logging.exception('SN does not seem to be returning a well formed response. '
                                      f'Maybe it is not accessible? Sleeping for {str(sleepTime)} minutes...')
                    time.sleep(sleepTime * 60)

                retries = retries - 1
                if self.response != None and self.response.status_code == 200:
                    break
                elif self.response != None:
                    logging.warning(f'HTTP status: {self.response.status_code}')
            if (self.response == None or self.response.status_code != 200) and retries == 0:
                logging.warning(f'Could not get valid answer. Retried {config["sn"]["retries"]} times.')
                return {}
            return self.response.json()
        except Exception as e:
            logging.exception('Error calling SN attachment get')

    def _DeleteAttachment(self, sys_id):
        global errorHappened
        try:
            # use retry loop
            retries = int(config['sn']['retries'])
            self.response = None
            while retries > 0:
                try:
                   self.response = requests.delete(f'{self.url}/api/now/attachment/{sys_id}',
                                              auth=(self.username, self.password),
                                              proxies=self.proxyDict if config['sn']['use.proxy'] == 'True'
                                              else None)
                except:
                    sleepTime = int(config['sn']['sleep.time.if.no.access.minutes'])
                    logging.exception('SN does not seem to be returning a well formed response. '
                                      f'Maybe it is not accessible? Sleeping for {str(sleepTime)} minutes...')
                    time.sleep(sleepTime * 60)

                retries = retries - 1
                if self.response != None and (self.response.status_code == 200 or self.response.status_code == 204):
                    break
                elif self.response != None:
                    errorHappened = True
                    logging.warning(f'HTTP status: {self.response.status_code}')
            if (self.response == None or self.response.status_code != 200 and self.response.status_code != 204) and retries == 0:
                errorHappened = True
                logging.warning(f'Could not get valid answer. Retried {config["sn"]["retries"]} times.')
                return {}
            return self.response
        except Exception as e:
            logging.exception('Error calling SN attachment delete')

    def Attach(self, file_name, data, table_sys_id):
        return self._Post(file_name, data, table_sys_id)

    def GetAttachmentsFor(self, table_sys_id):
        return self._GetAttachmentsFor(table_sys_id)

    def DeleteAttachment(self, sys_id):
        return self._DeleteAttachment(sys_id)

def attachFilesToSN(passwordNew):

    TABLE_SYS_ID = 'attachment.table_sys_id.'
    ATTACHMENT_URL = 'attachment.url.'

    for url_item in config.items('sn'):
        if url_item[0].startswith(ATTACHMENT_URL):
            sn_host = url_item[0]
            sn_host_number = int(sn_host.rsplit('.', 1)[-1])
            url = url_item[1]
            sn = SN(url, passwordNew)
            for item in config.items('sn'):
                if item[0].startswith(TABLE_SYS_ID):
                    original_item = json.loads(item[1])
                    attach_to = original_item['to']
                    if sn_host_number not in attach_to:
                        continue
                    original_file_name = original_item['file']
                    #file can contain full absolut path, so splitting per directory delimiters
                    original_file_name_tmp = original_file_name.split('/')
                    #extracting only last segment of file path
                    serviceNow_file_name = original_file_name_tmp[len(original_file_name_tmp) - 1]
                    #appending date to the file name to be used in ServiceNow upload
                    file_name = f'{serviceNow_file_name[0:len(item[1]) - 4]}-{datetime.datetime.today().strftime("%Y%m%d")}.csv'

                    table_sys_id = item[0].split(TABLE_SYS_ID)[1]
                    file = None
                    try:
                        attachments = sn.GetAttachmentsFor(table_sys_id)
                        logging.info(f'Attaching {original_file_name} as {serviceNow_file_name} to {url} with table_sys_id {table_sys_id}')
                        file = open(original_file_name, 'rb')
                        res = sn.Attach(file_name, file.read(), table_sys_id)
                        if 'result' in res:
                            for attachment in attachments['result']:
                                logging.info(f'Deleting attachment with sys_id {attachment["sys_id"]}')
                                success = sn.DeleteAttachment(attachment['sys_id'])
                    except:
                        logging.exception(f'Error attaching file {original_file_name} as {file_name} to {url} with table_sys_id {table_sys_id}')
                    finally:
                        if file != None:
                            file.close()

    logging.info(f'Done attaching.')

def setUpLogging():
    # configure logging (remove all existing handlers to avoid log file not being created on disk)
    loggingLevel = loggingLevels[config['general']['logging.level']]
    for handler in logging.root.handlers[:]:  # removing all handlers
        logging.root.removeHandler(handler)
    logFormatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler = logging.StreamHandler(sys.stdout)
    handler.setFormatter(logFormatter)
    logging.getLogger().addHandler(handler)
    handler = RotatingFileHandler(LOG_FILE, maxBytes=(1048576 * 5), backupCount=7)
    handler.setFormatter(logFormatter)
    logging.getLogger().addHandler(handler)
    logging.getLogger().setLevel(level=loggingLevel)

#@prints_exc(file_=LoggerAsFile(logging))
def main():
    global errorHappened
    try:
#        errorHappened = None

        setUpLogging()

        logging.info('================================ Started ================================')

        logging.info("Fetching ServiceNow credentials")

        session = boto3.session.Session()
        client = session.client(
            service_name='secretsmanager',
            region_name=SERVICENOW_REGION,
        )

        try:
            get_secret_value_response = client.get_secret_value(
                SecretId=SERVICENOW_SECRET_KEY_NAME
            )
        except ClientError as e:
            if e.response['Error']['Code'] == 'ResourceNotFoundException':
                print("The requested secret " + secret_name + " was not found")
            elif e.response['Error']['Code'] == 'InvalidRequestException':
                print("The request was invalid due to:", e)
            elif e.response['Error']['Code'] == 'InvalidParameterException':
                print("The request had invalid params:", e)
            elif e.response['Error']['Code'] == 'DecryptionFailure':
                print("The requested secret can't be decrypted using the provided KMS key:", e)
            elif e.response['Error']['Code'] == 'InternalServiceError':
                print("An error occurred on service side:", e)
        else:
            # Secrets Manager decrypts the secret value using the associated KMS CMK
            # Depending on whether the secret was a string or binary, only one of these fields will be populated
            if 'SecretString' in get_secret_value_response:
                #convert result string encoded as key value JSON payload into Python list
                secretDataText = get_secret_value_response['SecretString']
                secretDataList = json.loads(secretDataText)
                #TENABLE_ACCESS_KEY = secretDataList['accessKey']
                SERVICENOW_PASSWORD = secretDataList['password']
            else:
                #binary output is not compatible
                raise Exception("ServiceNow API secret returned in binary. Script execution cannot continue.")


        logging.info("ServiceNow Credentials successfuly fetched!")

        if config['sn']['attachment.enabled'] == 'True':
            logging.info('Attaching files to SN')
            attachFilesToSN(SERVICENOW_PASSWORD)

    except Exception as e:
        errorHappened = True
        logging.error(str(e))

    finally:
        #sending notification that processing went well
        logging.info("Send notification to SNS topic: {0}".format(TOPIC_ARN))

        sns = boto3.client('sns')

        if errorHappened == None:
            messageToSend = "ServiceNow file upload is successfully finished ..."
            subjectToSend = "{0} - ServiceNow file upload - successfully finished".format(datetime.datetime.today().strftime("%Y-%m-%d"))
        else:
            messageToSend = "ServiceNow file upload finished with errors. Check logs for more info."
            subjectToSend = "{0} - ServiceNow file upload - finished with errors".format(datetime.datetime.today().strftime("%Y-%m-%d"))

        sns.publish(TopicArn=TOPIC_ARN,
            Message = messageToSend,
            Subject = subjectToSend)

        logging.info("SNS notification successfully sent!")

        logging.info('Done.')

        logging.info('================================ Finished ================================')

# ##############
#  main program
# ##############
if __name__ == '__main__':

    main()
