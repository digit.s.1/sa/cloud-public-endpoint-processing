#Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
#the European Commission - subsequent versions of the EUPL (the "Licence");
#You may not use this work except in compliance with the Licence.
#You may obtain a copy of the Licence at:
#
#   https://joinup.ec.europa.eu/software/page/eupl
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the Licence is distributed on an "AS IS" basis,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the Licence for the specific language governing permissions and
#limitations under the Licence.

import boto3
import csv
import subprocess
import sys
from datetime import datetime

#checking input arguments
print("\n---> Checking input arguments")
if len(sys.argv) == 1:
    print("\n---> No argument provided. Exiting.")
    sys.exit()
else:
    ACCOUNT_TYPE = sys.argv[1]

#setting up environment
SOURCE_CONTACTS_FILE = 'ECContacts.csv'
DESTINATION_CONTACTS_FILE_TEMP = '/home/ubuntu/outputs/ECContacts.csv.tmp'
DESTINATION_CONTACTS_FILE = '/home/ubuntu/outputs/ECContacts.csv'
DESTINATION_CONTACTS_FILE_P4M = 'ECContactsP4M.csv'
DESTINATION_CONTACTS_FILE_PATH_P4M = '/home/ubuntu/outputs/' + DESTINATION_CONTACTS_FILE_P4M
PROCESSED_CONTACTS_FILENAME = '{0}-accounts.txt.all'.format(ACCOUNT_TYPE.lower())
PROCESSED_CONTACTS_FILENAME_TEMP = PROCESSED_CONTACTS_FILENAME + '.tmp'
PROCESSED_CONTACTS_FILE = '/home/ubuntu/outputs/' + PROCESSED_CONTACTS_FILENAME
PROCESSED_CONTACTS_FILE_TEMP = '/home/ubuntu/outputs/' + PROCESSED_CONTACTS_FILENAME_TEMP
PROCESSED_COMPLETE_CONTACTS_FILENAME = '{0}-accounts.complete.txt.all'.format(ACCOUNT_TYPE.lower())
PROCESSED_COMPLETE_CONTACTS_FILE = '/home/ubuntu/outputs/' + PROCESSED_COMPLETE_CONTACTS_FILENAME
S3_BUCKET_NAME = 'account-export-digits2-prod-6'
SA_S3_BUCKET_NAME = 'ist-vm'
SA_S3_BUCKET_NAME_ENDPOINT = 'exports/'
CLOUD_PROVIDER_ACCOUNT_FILTER = ACCOUNT_TYPE.upper()
TOPIC_ARN = 'arn:aws:sns:eu-central-1:042034201921:cloud-contact-file-processing'

startDateTime = datetime.today()

print("\n---> Script execution started @ {0} ...\n".format(startDateTime))

print("---> AWS Credentials successfuly fetched")

print("---> Assuming role")

stsClient = boto3.client('sts')

# Call the assume_role method of the STSConnection object and pass the role
# ARN and a role session name.
assumedRoleObject=stsClient.assume_role(
    RoleArn="arn:aws:iam::906266124095:role/accountExportBucketReadOnlyRole",
    RoleSessionName="getAccountDataSession"
)

credentials = assumedRoleObject['Credentials']

print("---> Getting source file from S3 bucket")

s3 = boto3.client('s3', aws_access_key_id = credentials['AccessKeyId'], aws_secret_access_key = credentials['SecretAccessKey'], aws_session_token = credentials['SessionToken'])
s3.download_file(S3_BUCKET_NAME, SOURCE_CONTACTS_FILE, DESTINATION_CONTACTS_FILE_TEMP)

print("---> File successfully downloaded from S3 {0}/{1} to {2}".format(S3_BUCKET_NAME, SOURCE_CONTACTS_FILE, DESTINATION_CONTACTS_FILE_TEMP))

print("---> Converting file \"{0}\" from dos to unix format".format(DESTINATION_CONTACTS_FILE_TEMP))

shellCommand = "/usr/bin/dos2unix -n {0} {1};rm {0}".format(DESTINATION_CONTACTS_FILE_TEMP, DESTINATION_CONTACTS_FILE, DESTINATION_CONTACTS_FILE_TEMP)

try:
    shellExecution = subprocess.run(shellCommand, stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True, text=True, shell=True, executable='/bin/bash')
except subprocess.CalledProcessError as e:
    print("xxx> Error occured: {0}".format(e))


print("---> Stripping off carriage return caracter from the file \"{0}\"".format(DESTINATION_CONTACTS_FILE_TEMP))

shellCommand = "cp {0} {1};cat {2} | tr -d '\r' > {3};rm {4}".format(DESTINATION_CONTACTS_FILE, DESTINATION_CONTACTS_FILE_TEMP, DESTINATION_CONTACTS_FILE_TEMP, DESTINATION_CONTACTS_FILE, DESTINATION_CONTACTS_FILE_TEMP)

try:
    shellExecution = subprocess.run(shellCommand, stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True, text=True, shell=True, executable='/bin/bash')
except subprocess.CalledProcessError as e:
    print("xxx> Error occured: {0}".format(e))


print("---> Reading file: {0} for CSV processing".format(DESTINATION_CONTACTS_FILE))


#this code creates list for simple account file without contact information
csvList = []
with open(DESTINATION_CONTACTS_FILE, mode='r') as csvFile:
    csvReader = csv.DictReader(csvFile)

    for currentRow in csvReader:
        csvRecord = []
        if currentRow["Status"] == "ACTIVE" and currentRow["AccountType"] == CLOUD_PROVIDER_ACCOUNT_FILTER and currentRow["ContactRole"] == "RESPONSIBLE":
            csvRecord.append(currentRow["AccountIdentifier"])
            csvRecord.append(currentRow["AccountName"])
            csvList.append(csvRecord)

#this code creates list for complete account file with all contact information
csvListAccountContacts = []
with open(DESTINATION_CONTACTS_FILE, mode='r') as accountFile:
    csvAccountFileReader = csv.DictReader(accountFile)

    csvRecord = []
    csvRecord.append("Account Identifier")
    csvRecord.append("Account Name")
    csvRecord.append("Account Organisation Acronym")
    csvRecord.append("Responsible contact names")
    csvRecord.append("Responsible contact emails")
    csvRecord.append("Responsible contact phones")
    csvRecord.append("Operation contact names")
    csvRecord.append("Operation contact emails")
    csvRecord.append("Operation contact phones")
    csvRecord.append("Security contact names")
    csvRecord.append("Security contact emails")
    csvRecord.append("Security contact phones")

    csvListAccountContacts.append(csvRecord)

    for currentAccountRow in csvAccountFileReader:
        
        if currentAccountRow["Status"] == "ACTIVE" and currentAccountRow["AccountType"] == CLOUD_PROVIDER_ACCOUNT_FILTER:
            currentAccount = currentAccountRow["AccountIdentifier"]
        else:
            continue

        responsibleContactNames=""
        responsibleContactEmails=""
        responsibleContactPhones=""
        operationContactNames=""
        operationContactEmails=""
        operationContactPhones=""
        securityContactNames=""
        securityContactEmails=""
        securityContactPhones=""

        with open(DESTINATION_CONTACTS_FILE, mode='r') as accountContactFile:
            csvAccountContactFileReader = csv.DictReader(accountContactFile)

            #getting all contact information denormalised in one string per contact category
            for currentAccountContactRow in csvAccountContactFileReader:

                if currentAccountContactRow["AccountIdentifier"] == currentAccount:
                    if currentAccountContactRow["ContactRole"] == "RESPONSIBLE":
                        responsibleContactNames += currentAccountContactRow["ContactLabel"] + ","
                        responsibleContactEmails += currentAccountContactRow["ContactEmail"] + ","
                        responsibleContactPhones += currentAccountContactRow["ContactPhone"] + ","

                    if currentAccountContactRow["ContactRole"] == "OPERATION":
                        operationContactNames += currentAccountContactRow["ContactLabel"] + ","
                        operationContactEmails += currentAccountContactRow["ContactEmail"] + ","
                        operationContactPhones += currentAccountContactRow["ContactPhone"] + ","

                    if currentAccountContactRow["ContactRole"] == "SECURITY":
                        securityContactNames += currentAccountContactRow["ContactLabel"] + ","
                        securityContactEmails += currentAccountContactRow["ContactEmail"] + ","
                        securityContactPhones += currentAccountContactRow["ContactPhone"] + ","

                else:
                    continue

        #stripping the commas from the end of generated strings
        responsibleContactNames = responsibleContactNames[:-1]
        responsibleContactEmails = responsibleContactEmails[:-1]
        responsibleContactPhones = responsibleContactPhones[:-1]
        operationContactNames = operationContactNames[:-1]
        operationContactEmails = operationContactEmails[:-1]
        operationContactPhones = operationContactPhones[:-1]
        securityContactNames = securityContactNames[:-1]
        securityContactEmails = securityContactEmails[:-1]
        securityContactPhones = securityContactPhones[:-1]

        csvRecord = []
#        if currentRow["Status"] == "ACTIVE" and currentRow["AccountType"] == CLOUD_PROVIDER_ACCOUNT_FILTER and currentRow["ContactRole"] == "RESPONSIBLE":
        csvRecord.append(currentAccountRow["AccountIdentifier"])
        csvRecord.append(currentAccountRow["AccountName"])
        csvRecord.append(currentAccountRow["OrganisationAcronym"])
        csvRecord.append(responsibleContactNames)
        csvRecord.append(responsibleContactEmails)
        csvRecord.append(responsibleContactPhones)
        csvRecord.append(operationContactNames)
        csvRecord.append(operationContactEmails)
        csvRecord.append(operationContactPhones)
        csvRecord.append(securityContactNames)
        csvRecord.append(securityContactEmails)
        csvRecord.append(securityContactPhones)

        csvListAccountContacts.append(csvRecord)


#this code supposed to remove duplicates via lambda function but in some cases it is buggy, so it is replaced with shell command
#csvList = list(set(map(lambda i: tuple(sorted(i)), csvList)))

csvList.sort()

print("---> Writing account processed data content into CSV file: {0}".format(PROCESSED_CONTACTS_FILE))

with open(PROCESSED_CONTACTS_FILE_TEMP, 'w') as accountCsvFile:
    # using csv.writer method from CSV package
    writeCSV = csv.writer(accountCsvFile, delimiter=';')
    writeCSV.writerows(csvList)

#making unique rows via shell command
print("---> Making file \"{0}\" sorted file with unique lines".format(PROCESSED_CONTACTS_FILE_TEMP))

shellCommand = "sort -u {0} > {1};rm {0}".format(PROCESSED_CONTACTS_FILE_TEMP, PROCESSED_CONTACTS_FILE)

try:
    shellExecution = subprocess.run(shellCommand, stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True, text=True, shell=True, executable='/bin/bash')
except subprocess.CalledProcessError as e:
    print("xxx> Error occured: {0}".format(e))

#writing complete contacts list
print("---> Writing account with contacts processed data content into CSV file: {0}".format(PROCESSED_COMPLETE_CONTACTS_FILE))
with open(PROCESSED_COMPLETE_CONTACTS_FILE, 'w') as accountContactsCsvFile:
    # using csv.writer method from CSV package
    writeCSV = csv.writer(accountContactsCsvFile, delimiter=';')
    writeCSV.writerows(csvListAccountContacts)


#counting number of account in file
numberOfAccountsFound = sum(1 for line in open(PROCESSED_CONTACTS_FILE))

print("---> Found {0} active accounts.".format(numberOfAccountsFound))

#putting files in SA S3 bucket
print("---> Putting account and account contact files to S3 bucket ({0})".format(SA_S3_BUCKET_NAME))

s3 = boto3.client('s3')

#storing non-processed contacts file to S3 bucket
tmpFileString = ""
file = open(PROCESSED_CONTACTS_FILE)
tmpFileString = file.read()
file.close()

s3.put_object(Bucket=SA_S3_BUCKET_NAME, Body=tmpFileString, Key=SA_S3_BUCKET_NAME_ENDPOINT + PROCESSED_CONTACTS_FILENAME)
print("---> File {0} successfully uploaded to S3 bucket {1}".format(PROCESSED_CONTACTS_FILE, SA_S3_BUCKET_NAME + '/' + SA_S3_BUCKET_NAME_ENDPOINT + PROCESSED_CONTACTS_FILENAME))

#storing processed contacts file to S3 bucket
tmpFileString = ""
file = open(PROCESSED_COMPLETE_CONTACTS_FILE)
tmpFileString = file.read()
file.close()

s3.put_object(Bucket=SA_S3_BUCKET_NAME, Body=tmpFileString, Key=SA_S3_BUCKET_NAME_ENDPOINT + PROCESSED_COMPLETE_CONTACTS_FILENAME)
print("---> File {0} successfully uploaded to S3 bucket {1}".format(PROCESSED_COMPLETE_CONTACTS_FILENAME, SA_S3_BUCKET_NAME + '/' + SA_S3_BUCKET_NAME_ENDPOINT + PROCESSED_COMPLETE_CONTACTS_FILENAME))

#storing source file to S3 bucket
tmpFileString = ""
file = open(DESTINATION_CONTACTS_FILE)
tmpFileString = file.read()
file.close()

s3.put_object(Bucket=SA_S3_BUCKET_NAME, Body=tmpFileString, Key=SA_S3_BUCKET_NAME_ENDPOINT + SOURCE_CONTACTS_FILE)
print("---> File {0} successfully uploaded to S3 bucket {1}".format(SOURCE_CONTACTS_FILE, SA_S3_BUCKET_NAME + '/' + SA_S3_BUCKET_NAME_ENDPOINT + SOURCE_CONTACTS_FILE))

#writing complete contacts list with delimiter ;
print("---> Re-writing contacts data into ;-delimited CSV file: {0}".format(DESTINATION_CONTACTS_FILE_PATH_P4M))
with open(DESTINATION_CONTACTS_FILE, mode="r") as ecContactsCsvFile:
    reader = csv.reader(ecContactsCsvFile, delimiter=',')
    with open(DESTINATION_CONTACTS_FILE_PATH_P4M, mode="w") as ecContactsCsvFileP4M:
        writer = csv.writer(ecContactsCsvFileP4M, delimiter=';')
        writer.writerows(reader)

#sending notification that processing went well
print("---> Send notification to SNS topic: {0}".format(TOPIC_ARN))

sns = boto3.client('sns')

messageToSend = "Contacts information processing for {0} is successfully finished ...".format(ACCOUNT_TYPE.upper())
subjectToSend = "{0} - {1} Contact information processing - successfully finished".format(startDateTime.strftime("%Y-%m-%d"), ACCOUNT_TYPE.upper())


sns.publish(TopicArn=TOPIC_ARN,
    Message = messageToSend,
    Subject = subjectToSend)

endDateTime = datetime.today()
scriptExecutionTime = endDateTime - startDateTime

print("\n---> Script execution ended @ {0}".format(endDateTime))
print("\n---> Total execution time: {0}\n".format(scriptExecutionTime))
