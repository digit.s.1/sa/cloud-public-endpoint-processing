#Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
#the European Commission - subsequent versions of the EUPL (the "Licence");
#You may not use this work except in compliance with the Licence.
#You may obtain a copy of the Licence at:
#
#   https://joinup.ec.europa.eu/software/page/eupl
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the Licence is distributed on an "AS IS" basis,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the Licence for the specific language governing permissions and
#limitations under the Licence.

import csv
import json
import time
import sys
import boto3
from datetime import datetime, timedelta
from botocore.exceptions import ClientError
import requests

#checking input arguments
print("\n---> Checking input arguments")
if len(sys.argv) == 1:
    print("\n-----> No argument provided. Exiting.")
    sys.exit()
else:
    ACCOUNT_TYPE = sys.argv[1]
print("-----> Argument provided. Checking value eligibility ...")

if ACCOUNT_TYPE.lower() not in ('aws', 'azure'):
    print("---> Not supported value provided. Exiting.")
    sys.exit()
print("---> Valid argument value provided. Continuing...")


TENABLE_REGION = 'eu-central-1'
TENABLE_SECRET_KEY_NAME = 'prod/TenableAPI/S1_VA_AWS_ScriptRunner'
ENDPOINTS_FILENAME = '{0}-public_ip_address.csv'.format(ACCOUNT_TYPE.lower())
ENDPOINTS_FILE = '/home/ubuntu/outputs/' + ENDPOINTS_FILENAME

ASSET_TAG = ACCOUNT_TYPE.upper()
ASSET_DATA_SOURCE = "{0}-script".format(ACCOUNT_TYPE.upper())
ASSET_DATA_SOURCE_FROM_TENABLE_AGENTS = "Agent"

WAIT_TIME_BETWEEN_CALLS = 5
WAIT_TIME_BETWEEN_ITERATIONS = 15
WAIT_TIME_ASSET_PROPAGATION = 300
NUMBER_OF_ITERATIONS = 5
LAST_SEEN_MAX_AGE_IN_DAYS = 30
EARLIEST_DATE_LAST_SEEN = datetime.now() - timedelta(LAST_SEEN_MAX_AGE_IN_DAYS)

TOPIC_ARN = 'arn:aws:sns:eu-central-1:042034201921:tenable-assets-import'
ERROR_MESSAGE = None

#starting execution of the script
startDateTime = datetime.today()

print("\n---> Script execution started @ {0} ...\n".format(startDateTime))

try:
    #reading credentials information from aws secret manager
    print("---> Fetching TENABLE credentials")

    session = boto3.session.Session()
    client = session.client(
        service_name='secretsmanager',
        region_name=TENABLE_REGION,
    )

    try:
        get_secret_value_response = client.get_secret_value(
            SecretId=TENABLE_SECRET_KEY_NAME
        )
    except ClientError as e:
        if e.response['Error']['Code'] == 'ResourceNotFoundException':
            print("The requested secret " + secret_name + " was not found")
        elif e.response['Error']['Code'] == 'InvalidRequestException':
            print("The request was invalid due to:", e)
        elif e.response['Error']['Code'] == 'InvalidParameterException':
            print("The request had invalid params:", e)
        elif e.response['Error']['Code'] == 'DecryptionFailure':
            print("The requested secret can't be decrypted using the provided KMS key:", e)
        elif e.response['Error']['Code'] == 'InternalServiceError':
            print("An error occurred on service side:", e)
    else:
        # Secrets Manager decrypts the secret value using the associated KMS CMK
        # Depending on whether the secret was a string or binary, only one of these fields will be populated
        if 'SecretString' in get_secret_value_response:
            #convert result string encoded as key value JSON payload into Python list
            secretDataText = get_secret_value_response['SecretString']
            secretDataList = json.loads(secretDataText)
            TENABLE_ACCESS_KEY = secretDataList['accessKey']
            TENABLE_SECRET_KEY = secretDataList['secretKey']
        else:
            #binary output is not compatible
            raise Exception("Tenable API secret returned in binary. Script execution cannot continue.")

    #setting tenable access key string for API calls
    TENABLE_API_KEY="accessKey={0};secretKey={1}".format(TENABLE_ACCESS_KEY, TENABLE_SECRET_KEY)

    print("---> TENABLE Credentials successfuly fetched")

#############################################################
#
# fetching TAG uuids from Tenable
#
#############################################################

    print("\n---> Exporting tag uuids from Tenable")

    apiUrl = "https://cloud.tenable.com/tags/values?f=value:eq:{0}".format(ASSET_TAG)

    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "X-ApiKeys": TENABLE_API_KEY
    }

    response = requests.request("GET", apiUrl, headers=headers)

    if response.status_code != 200:
        errorText="Error occured while exporting tag uuids from Tenable (API return code {0})".format(response.text)
        raise Exception(errorText)

    assetTag = json.loads(response.text)
    assetTagUuid = assetTag['values'][0]['uuid']


#############################################################
#
# generating import list for asset import in Tenable
#
#############################################################

    print("\n---> Tenable asset import data generation started.")

    print("---> Opening {0} cloud endpoint file {1} for reading".format(ACCOUNT_TYPE, ENDPOINTS_FILE))

    with open(ENDPOINTS_FILE, mode='r') as csvFile:
        print("-----> Reading file started ...")
        csvReader = csv.reader(csvFile, delimiter=';')
        print("-----> Reading file finished!")
        print("-----> Reading file into list started ...")
        endpointList = list(csvReader)
        print("-----> Reading file into list finished!")

    #creating unique list of endpoint instance IDs to prevent import data duplication
    endpointUniqueListTemp = list()
    for endpoint in endpointList:
        endpointUniqueListTemp.append(endpoint[4])
    endpointUniqueList = list(dict.fromkeys(endpointUniqueListTemp))

    endpointJsonFinalString = ""
    endpointPayloadString = ""
    endpointJsonFinalList = list()

    for endpoint in endpointUniqueList:
        endpointIPv4List = list()
        endpointFQDNList = list()
        ownerId = ""
        instanceId = ""
        i = 0

        for innerEndpoint in endpointList:
            if endpoint == innerEndpoint[4]:
                if innerEndpoint[2] != None and innerEndpoint[2] != "":
                    endpointIPv4List.append(innerEndpoint[2])
                    i = i + 1

                if innerEndpoint[3] != None and innerEndpoint[3] != "":
                    endpointFQDNList.append(innerEndpoint[3].replace("https://","").replace("http://",""))
                    i = i + 1

                ownerId = innerEndpoint[0]
                instanceId = innerEndpoint[4]

        if i > 0:
            endpointJsonSet = {"fqdn": endpointFQDNList, "ipv4": endpointIPv4List, "aws_owner_id": "{0}".format(ownerId), "aws_ec2_instance_id": "{0}".format(instanceId)}

        endpointJsonFinalList.append(endpointJsonSet)
        if endpointJsonFinalString != "":
            endpointJsonFinalString = "{0}, {1}".format(endpointJsonFinalString, endpointJsonSet)
        else:
            endpointJsonFinalString = "{0}".format(endpointJsonSet)

    endpointPayload = {"assets": endpointJsonFinalList, "source": ASSET_DATA_SOURCE}

    print("---> Tenable asset import data generation successfully finished.")

#############################################################
#
# importing assets in Tenable
#
#############################################################

    print("\n---> Importing assets in Tenable")

    apiUrl = "https://cloud.tenable.com/import/assets"

    payload = endpointPayload

    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "X-ApiKeys": TENABLE_API_KEY
    }

    response = requests.request("POST", apiUrl, json=payload, headers=headers)

    if response.status_code != 200:
        errorText="Error occured while exporting assets from Tenable (API return code {0})".format(response.text)
        raise Exception(errorText)

    assetsImportResponse=json.loads(response.text)

    print("---> Importing assets in Tenable successfully finished")

#############################################################
#
# fetching assets import status from Tenable
#
#############################################################

    print("\n---> Fetching assets import status")

    print("\n---> Waiting for {0} seconds before continuing with next API call".format(WAIT_TIME_BETWEEN_CALLS))
    time.sleep(WAIT_TIME_BETWEEN_CALLS)

    apiUrl = "https://cloud.tenable.com/import/asset-jobs/{0}".format(assetsImportResponse['asset_import_job_uuid'])

    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "X-ApiKeys": TENABLE_API_KEY
    }

    print("---> Calling asset import status API")
    for i in range(0, NUMBER_OF_ITERATIONS):
        print("-----> Iteration {0} of {1}".format(i+1, NUMBER_OF_ITERATIONS))
        response = requests.request("GET", apiUrl, headers=headers)

        if response.status_code != 200:
            errorText="Error occured while fetching assets import status from Tenable (API return code {0})".format(response.text)
            raise Exception(errorText)

        assetsExportStatusResponse=json.loads(response.text)

        if assetsExportStatusResponse['status'] == "COMPLETE":
            print("-----> Call successfuly finished!")
            break

        print("-----> Waiting for {0} seconds before continuing with next iteration step (API call)".format(WAIT_TIME_BETWEEN_ITERATIONS))
        time.sleep(WAIT_TIME_BETWEEN_ITERATIONS)

    #if the loops finished and execution did'n finish, raise expection and dump last execution results
    if assetsExportStatusResponse['status'] != "COMPLETE":
        errorText="Error occured while fetching status for assets import. Response dump: {0}".format(response.text)
        raise Exception(errorText)


#############################################################
#
# waiting period for propagation of imported assets in Tenable
#
#############################################################

    print("\n---> Waiting for data to be propagated within Tenable ({0} seconds)".format(WAIT_TIME_ASSET_PROPAGATION))
    time.sleep(WAIT_TIME_ASSET_PROPAGATION)
    print("---> Waiting period expired, continuing with script execution ...")


#############################################################
#
# exporting assets from Tenable
#
#############################################################

    print("\n---> Exporting assets from Tenable")

    apiUrl = "https://cloud.tenable.com/assets/export"


    payload = {
        "filters": {"sources": [ASSET_DATA_SOURCE, ASSET_DATA_SOURCE_FROM_TENABLE_AGENTS]},
        "chunk_size": 5000
    }
    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "X-ApiKeys": TENABLE_API_KEY
    }

    response = requests.request("POST", apiUrl, json=payload, headers=headers)

    if response.status_code != 200:
        errorText="Error occured while exporting assets from Tenable (API return code {0})".format(response.text)
        raise Exception(errorText)

    assetsExportResponse=json.loads(response.text)

    print("---> Exporting assets from Tenable successfully finished")

#############################################################
#
# fetching assets export status from Tenable
#
#############################################################

    print("\n---> Fetching assets export status")

    print("\n---> Waiting for {0} seconds before continuing with next API call".format(WAIT_TIME_BETWEEN_CALLS))
    time.sleep(WAIT_TIME_BETWEEN_CALLS)

    apiUrl = "https://cloud.tenable.com/assets/export/{0}/status".format(assetsExportResponse['export_uuid'])

    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "X-ApiKeys": TENABLE_API_KEY
    }

    print("---> Calling assets export status API")
    for i in range(0, NUMBER_OF_ITERATIONS):
        print("-----> Iteration {0} of {1}".format(i+1, NUMBER_OF_ITERATIONS))
        response = requests.request("GET", apiUrl, headers=headers)

        if response.status_code != 200:
            errorText="Error occured while fetching assets export status from Tenable (API return code {0})".format(response.text)
            raise Exception(errorText)

        assetsExportStatusResponse=json.loads(response.text)

        if assetsExportStatusResponse['status'] == "FINISHED":
            print("-----> Call successfuly finished!")
            break

        print("-----> Waiting for {0} seconds before continuing with next iteration step (API call)".format(WAIT_TIME_BETWEEN_ITERATIONS))
        time.sleep(WAIT_TIME_BETWEEN_ITERATIONS)

    #if the loops finished and execution did'n finish, raise expection and dump last execution results
    if assetsExportStatusResponse['status'] != "FINISHED":
        errorText="Error occured while fetching status for assets export. Response dump: {0}".format(response.text)
        raise Exception(errorText)


#############################################################
#
# fetching assets export chunks from Tenable
#
#############################################################

    print("\n---> Downloading assets export chunks")

    print("\n---> Waiting for {0} seconds before continuing with next API call".format(WAIT_TIME_BETWEEN_CALLS))
    time.sleep(WAIT_TIME_BETWEEN_CALLS)
    assetsData = list()

    for currentChunk in assetsExportStatusResponse['chunks_available']:

        print("\n-----> Initiating download of chunk {0} of {1}".format(currentChunk,len(assetsExportStatusResponse['chunks_available'])))
        apiUrl = "https://cloud.tenable.com/assets/export/{0}/chunks/{1}".format(assetsExportResponse['export_uuid'], currentChunk)

        headers = {
            "Accept": "application/json",
            "X-ApiKeys": TENABLE_API_KEY
        }

        print("-----> Calling asset chunk export API")

        for i in range(0, NUMBER_OF_ITERATIONS):
            print("-------> Iteration {0} of {1}".format(i+1, NUMBER_OF_ITERATIONS))
            response = requests.request("GET", apiUrl, headers=headers)

            if response.status_code != 200:
                errorText="Error occured while fetching assets chunk from Tenable (API return code {0})".format(response.text)
                raise Exception(errorText)
            else:
                print("-------> Chunk {0} started downloading ...".format(currentChunk))
                assetsData.extend(json.loads(response.text))
                print("-------> Chunk {0} sucessfuly downloaded!".format(currentChunk))
                break

    print("---> Tenable assets successfuly downloaded")

#############################################################
#
# creating list of assets to be deleted
#
#############################################################

    print("\n---> Creating list of assets to be deleted")

    deletionSetList = list()

    for asset in assetsData:
        assetLastSeenDatetime = datetime.strptime(asset['last_seen'], '%Y-%m-%dT%H:%M:%S.%fZ')
        lastSeenDiffInSeconds = (assetLastSeenDatetime - EARLIEST_DATE_LAST_SEEN).total_seconds()
        if lastSeenDiffInSeconds < 0:
            deletionSet = { "field": "host.id", "operator": "eq", "value": asset['id'] }
            deletionSetList.append(deletionSet)

    print("---> Creation of list of assets to be deleted successfully finished")

#############################################################
#
# deleting assets from Tenable
#
#############################################################

    print("\n---> Deleting assets from Tenable")

    print("------>Number of assets to be deleted: ")
    #print(deletionSetList)
    print(len(deletionSetList))

    if len(deletionSetList) != 0:
        apiUrl = "https://cloud.tenable.com/api/v2/assets/bulk-jobs/delete"

        headers = {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "X-ApiKeys": TENABLE_API_KEY
        }

        # content length error too large (I assume in bytes, chars?) -> split lists into sublists of 1000 elements and iterate over them to delete each
        deletionSetLists = [deletionSetList[idx:idx + 1000] for idx in range(0, len(deletionSetList), 1000)]

        #print(deletionSetLists)
        print("------>Number of asset sublists of 1000 elements to be deleted: ")
        print(len(deletionSetLists))

        for sublist in deletionSetLists:

            payload = {"query": { "or": sublist}, "hard_delete": True }

            response = requests.request("POST", apiUrl, json=payload, headers=headers)

            if response.status_code != 202:
                errorText="Error occured while deleting assets from Tenable (API return code {0})".format(response.text)
                raise Exception(errorText)

            assetsDeleteResponse=json.loads(response.text)

            print("---> Deleting assets from Tenable successfully completed. Total of {0} assets deleted.".format(assetsDeleteResponse['response']['data']['asset_count']))

    else:
        print("---> Deleting assets skipped since no assets found for deletion.")

#############################################################
#
# creating list of assets to be tagged
#
#############################################################

    print("\n---> Creating list of assets to be tagged")

    assetsForTaggingList = list()
    for asset in assetsData:
        assetsForTaggingList.append(asset['id'])

    print("---> Creation of list of assets to be tagged successfully finished")

#############################################################
#
# applying source tag to assets in Tenable
#
#############################################################

    print("\n---> Adding source tag to assets in Tenable started")

    apiUrl = "https://cloud.tenable.com/tags/assets/assignments"

    payload = {
        "assets": assetsForTaggingList,
        "tags": ["{0}".format(assetTagUuid)],
        "action": "add"
    }

    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "X-ApiKeys": TENABLE_API_KEY
    }

    response = requests.request("POST", apiUrl, json=payload, headers=headers)

    if response.status_code != 200:
        errorText="Error occured while applying source tag to assets in Tenable (API return code {0})".format(response.text)
        raise Exception(errorText)

    applyTagFromAllAssetsResponse=json.loads(response.text)

    print("\n---> Applying source tag to assets in Tenable successfully finished")

except Exception as e:
    print("ERROR> {0}".format(e))
    ERROR_MESSAGE = format(e)


finally:

#############################################################
#
# sending SNS notification
#
#############################################################


    #sending notification that processing went well
    print("\n---> Send notification to SNS topic: {0}".format(TOPIC_ARN))

    sns = boto3.client('sns')

    if ERROR_MESSAGE != None:
        messageToSend = "Import of Tenable assets for {0} had errors. Check logs for more information.".format(ACCOUNT_TYPE)
        subjectToSend = "{0} - Import of Tenable assets for {1} - error during execution".format(startDateTime.strftime("%Y-%m-%d"), ACCOUNT_TYPE)
    else:
        messageToSend = "Import of Tenable assets for {0} successfully finished.".format(ACCOUNT_TYPE)
        subjectToSend = "{0} - Import of Tenable assets for {1} - successfully finished".format(startDateTime.strftime("%Y-%m-%d"), ACCOUNT_TYPE)

    sns.publish(TopicArn=TOPIC_ARN,
        Message = messageToSend,
        Subject = subjectToSend)

    print("---> SNS notification successfully sent!")


#finishing execution of the script
endDateTime = datetime.today()
scriptExecutionTime = endDateTime - startDateTime

print("\n---> Script execution ended @ {0}".format(endDateTime))
print("\n---> Total execution time: {0}\n".format(scriptExecutionTime))
