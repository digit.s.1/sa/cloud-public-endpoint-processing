#!/bin/bash

#Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
#the European Commission - subsequent versions of the EUPL (the "Licence");
#You may not use this work except in compliance with the Licence.
#You may obtain a copy of the Licence at:
#
#   https://joinup.ec.europa.eu/software/page/eupl
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the Licence is distributed on an "AS IS" basis,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the Licence for the specific language governing permissions and
#limitations under the Licence.


currentDate=$(date '+%Y-%m-%d')

backupFileName="/home/ubuntu/backups/ist-vm-script-runner-backup.$currentDate.tar.gz"

$(tar --exclude "/home/ubuntu/keys" --exclude "/home/ubuntu/outputs" --exclude "/home/ubuntu/.config" --exclude "/home/ubuntu/scripts/aws_asset_inventory" --exclude "/home/ubuntu/scripts/vulntest" --exclude "/home/ubuntu/scripts/output" --exclude "/home/ubuntu/test" --exclude "/home/ubuntu/temp" --exclude "/home/ubuntu/.ssh" --exclude "/home/ubuntu/.cache" --exclude "/home/ubuntu/.Azure" --exclude "/home/ubuntu/.tenable" --exclude "/home/ubuntu/.local" --exclude "/home/ubuntu/.aws" --exclude "/home/ubuntu/aws" --exclude "/home/ubuntu/backups" -pczf $backupFileName /home/ubuntu/ > /home/ubuntu/logs/backup_machine.sh.log 2> /home/ubuntu/logs/backup_machine.sh.error.log)

$(aws s3 cp $backupFileName s3://ist-vm/backup/ >> /home/ubuntu/logs/backup_machine.sh.log 2>> /home/ubuntu/logs/backup_machine.sh.error.log)

#deleting older backups than 45 days
$(find /home/ubuntu/backups -type f -mtime +45 ! -iname "aws_asset_inventory.tar.gz" ! -iname "20220126-logs-backup.tar.gz" ! -iname "20220126-output-backup.tar.gz" -delete >> /home/ubuntu/logs/backup_machine.sh.log 2>> /home/ubuntu/logs/backup_machine.sh.error.log)

errorLog=$(cat /home/ubuntu/logs/backup_machine.sh.error.log)

messageToSend="Backup finished. If any errors here they are: >>$errorLog<<"

executeResult=$(aws sns publish --topic-arn "arn:aws:sns:eu-central-1:042034201921:ec2-instance-backups" --subject "$currentDate - AWS EC2 backup - finished for: ist-vm-script-runner" --message "$messageToSend")
