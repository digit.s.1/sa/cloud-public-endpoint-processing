import csv
import json
import time
import sys
from datetime import datetime
from botocore.exceptions import ClientError
import requests


TENABLE_REGION = 'eu-central-1'
TENABLE_SECRET_KEY_NAME = 'prod/TenableAPI/S1_VA_AWS_ScriptRunner'
AWS_CONTACTS_FILENAME = 'aws-accounts.complete.txt.all'
AWS_CONTACTS_FILE = '/home/ubuntu/outputs/' + AWS_CONTACTS_FILENAME
AZURE_CONTACTS_FILENAME = 'azure-accounts.complete.txt.all'
AZURE_CONTACTS_FILE = '/home/ubuntu/outputs/' + AZURE_CONTACTS_FILENAME
VULN_WITH_CONTACTS_FILENAME = 'vuln-cloud-assets-with-contacts.debug.csv'
VULN_WITH_CONTACTS_FILE = '/home/ubuntu/outputs/' + VULN_WITH_CONTACTS_FILENAME
S3_BUCKET_NAME = 'ist-vm'
WAIT_TIME_BETWEEN_CALLS = 5
WAIT_TIME_BETWEEN_ITERATIONS = 15
NUMBER_OF_ITERATIONS = 5

try:
    #reading credentials information form aws secrets manager
    print("---> Fetching TENABLE credentials")

    session = boto3.session.Session()
    client = session.client(
        service_name='secretsmanager',
        region_name=TENABLE_REGION,
    )

    try:
        get_secret_value_response = client.get_secret_value(
            SecretId=TENABLE_SECRET_KEY_NAME
        )
    except ClientError as e:
        if e.response['Error']['Code'] == 'ResourceNotFoundException':
            print("The requested secret " + secret_name + " was not found")
        elif e.response['Error']['Code'] == 'InvalidRequestException':
            print("The request was invalid due to:", e)
        elif e.response['Error']['Code'] == 'InvalidParameterException':
            print("The request had invalid params:", e)
        elif e.response['Error']['Code'] == 'DecryptionFailure':
            print("The requested secret can't be decrypted using the provided KMS key:", e)
        elif e.response['Error']['Code'] == 'InternalServiceError':
            print("An error occurred on service side:", e)
    else:
        # Secrets Manager decrypts the secret value using the associated KMS CMK
        # Depending on whether the secret was a string or binary, only one of these fields will be populated
        if 'SecretString' in get_secret_value_response:
            #convert result string encoded as key value JSON payload into Python list
            secretDataText = get_secret_value_response['SecretString']
            secretDataList = json.loads(secretDataText)
            TENABLE_ACCESS_KEY = secretDataList['accessKey']
            TENABLE_SECRET_KEY = secretDataList['secretKey']
        else:
            #binary output is not compatible
            raise Exception("Tenable API secret returned in binary. Script execution cannot continue.")

    #setting tenable access key string for API calls
    TENABLE_API_KEY="accessKey={0};secretKey={1}".format(TENABLE_ACCESS_KEY, TENABLE_SECRET_KEY)

    print("---> TENABLE Credentials successfuly fetched")

    #starting execution of the script
    startDateTime = datetime.today()

    print("\n---> Script execution started @ {0} ...\n".format(startDateTime))


    ##################################
    #
    # fetching from files
    #
    #################################
    # Opening JSON file
    f = open('/home/ubuntu/scripts/vulntest/chunk_18.json',)

    # returns JSON object as
    # a dictionary
    vulnerabilitiesData = json.load(f)

    f.close()

    # Opening JSON file
    f = open('/home/ubuntu/scripts/vulntest/assets_chunk_1.json',)

    # returns JSON object as
    # a dictionary
    assetsData = json.load(f)

    f.close()

#############################################################
#
# fetching cloud account contact information into list
#
#############################################################

    print("---> Fetching cloud account information into list")

    print("---> Opening AWS cloud account contact file {0} for reading".format(AWS_CONTACTS_FILE))
    with open(AWS_CONTACTS_FILE, mode='r') as csvFile:
        print("-----> Reading file started ...".format(AWS_CONTACTS_FILE))
        csvReader = csv.DictReader(csvFile, delimiter=';')
        print("-----> Reading file finished!".format(AWS_CONTACTS_FILE))
        print("-----> Reading file into list started ...".format(AWS_CONTACTS_FILE))
        awsContactList = list(csvReader)
        print("-----> Reading file into list finished!".format(AWS_CONTACTS_FILE))

    print("---> Opening AZURE cloud account contact file {0} for reading".format(AZURE_CONTACTS_FILE))
    with open(AZURE_CONTACTS_FILE, mode='r') as csvFile:
        print("-----> Reading file started ...".format(AZURE_CONTACTS_FILE))
        csvReader = csv.DictReader(csvFile, delimiter=';')
        print("-----> Reading file finished!".format(AZURE_CONTACTS_FILE))
        print("-----> Reading file into list started ...".format(AZURE_CONTACTS_FILE))
        azureContactList = list(csvReader)
        print("-----> Reading file into list finished!".format(AZURE_CONTACTS_FILE))


    #print(awsContactList)

#############################################################
#
# merging vulnerabilities, assets and contact data
#
#############################################################

#vuln:     vulnerabilitiesData
#assets:   assetsData
#contacts: awsContactList

    #loop through vulnerabilities
    outputList = []
    outputRow = []

    outputRow.append("Plugin Id")
    outputRow.append("Plugin Name")
    outputRow.append("Severity")
    outputRow.append("Asset Id")
    outputRow.append("Asset IP Address")
    outputRow.append("Protocol")
    outputRow.append("Port")
    outputRow.append("Asset FQDN")
    outputRow.append("Plugin Output")
    outputRow.append("Synopsis")
    outputRow.append("Description")
    outputRow.append("Solution")
    outputRow.append("See Also")
    outputRow.append("Vulnerability Priority Rating")
    outputRow.append("CVSS V2 Base Score")
    outputRow.append("CVSS V3 Base Score")
    outputRow.append("CVSS V2 Vector")
    outputRow.append("CVSS V3 Vector")
    outputRow.append("Exploit Maturity")
    outputRow.append("CVE")
    outputRow.append("Risk Level")
    outputRow.append("Cloud Provider")
    outputRow.append("Cloud Account Identifier")
    outputRow.append("Cloud Account Name")
    outputRow.append("Cloud Account Organisation Acronym")
    outputRow.append("Responsible contact names")
    outputRow.append("Responsible contact emails")
    outputRow.append("Responsible contact phones")
    outputRow.append("Operation contact names")
    outputRow.append("Operation contact emails")
    outputRow.append("Operation contact phones")
    outputRow.append("Security contact names")
    outputRow.append("Security contact emails")
    outputRow.append("Security contact phones")
    
    outputList.append(outputRow)

    for vuln in vulnerabilitiesData:

        #reset all temporary variable placeholders
        outputRow = []

        PluginId= ""
        PluginName= ""
        Severity= ""
        AssetId= ""
        AssetIPAddress= ""
        PluginProtocol= ""
        PluginPort= ""
        AssetFQDN= ""
        PluginOutput= ""
        PluginSynopsis= ""
        PluginDescription= ""
        PluginSolution= ""
        PluginSeeAlso= ""
        VulnerabilityPriorityRating= ""
        CVSSV2BaseScore= ""
        CVSSV3BaseScore= ""
        CVSSV2Vector= ""
        CVSSV3Vector= ""
        ExploitMaturity= ""
        CVE= ""
        RiskLevel= ""
        CloudProvider= ""
        CloudAccountIdentifier= ""
        CloudAccountName= ""
        CloudAccountOrganisationAcronym= ""
        ResponsibleContactNames= ""
        ResponsibleContactEmails= ""
        ResponsibleContactPhones= ""
        OperationContactNames= ""
        OperationContactEmails= ""
        OperationContactPhones= ""
        SecurityContactNames= ""
        SecurityContactEmails= ""
        SecurityContactPhones= ""

        #TO DELETE, DEBUG PURPOSES ONLY
        #if vuln['severity'] != "high" or vuln['severity'] != "medium":
        #    continue

        PluginId = "=\"" + str(vuln['plugin']['id']) + "\""
        PluginName = vuln['plugin']['name']
        Severity = vuln['severity']
        CVSSV3BaseScore = vuln['plugin']['cvss3_base_score']
        CVSSV2BaseScore = vuln['plugin']['cvss_base_score']
        PluginProtocol = vuln['port']['protocol']
        PluginPort = vuln['port']['port']

        try:
            PluginOutput = vuln['output'].replace("\"", "\"\"")
            if PluginOutput[0:1] == "-" or PluginOutput[0:1] == "+" or PluginOutput[0:1] == "=" or PluginOutput[0:1] == "@":
                PluginOutput = "'" + PluginOutput
        except:
            PluginOutput = ""

        try:
            PluginSynopsis = vuln['plugin']['synopsis'].replace("\"", "\"\"")
            if PluginSynopsis[0:1] == "-" or PluginSynopsis[0:1] == "+" or PluginSynopsis[0:1] == "=" or PluginSynopsis[0:1] == "@":
                PluginSynopsis = "'" + PluginSynopsis
        except:
            PluginSynopsis = ""

        try:
            PluginDescription = vuln['plugin']['description'].replace("\"", "\"\"")
            if PluginDescription[0:1] == "-" or PluginDescription[0:1] == "+" or PluginDescription[0:1] == "=" or PluginDescription[0:1] == "@":
                PluginDescription = "'" + PluginDescription
        except:
            PluginDescription = ""

        try:
            PluginSolution = vuln['plugin']['solution'].replace("\"", "\"\"")
            if PluginSolution[0:1] == "-" or PluginSolution[0:1] == "+" or PluginSolution[0:1] == "=" or PluginSolution[0:1] == "@":
                PluginSolution = "'" + PluginSolution
        except:
            PluginSolution = ""

        try:
            PluginSeeAlso = '\n'.join(vuln['plugin']['see_also']).replace("\"", "\"\"")
            if PluginSeeAlso[0:1] == "-" or PluginSeeAlso[0:1] == "+" or PluginSeeAlso[0:1] == "=" or PluginSeeAlso[0:1] == "@":
                PluginSeeAlso = "'" + PluginSeeAlso
        except:
            PluginSeeAlso = ""

        try:
            VulnerabilityPriorityRating = vuln['vpr']['score']
        except:
            VulnerabilityPriorityRating = ""

        try:
            CVSSV3Vector = vuln['cvss3_vector']['raw']
        except:
            CVSSV3Vector = ""

        try:
            CVSSV2Vector = vuln['cvss_vector']['raw']
        except:
            CVSSV2Vector = ""

        try:
            ExploitMaturity = vuln['vpr']['drivers']['exploit_code_maturity']
        except:
            ExploitMaturity = ""

        try:
            CVE = vuln['cve']
        except:
            CVE = ""

        RiskLevel = vuln['plugin']['risk_factor']


        for asset in assetsData:

            #skip assets not having ec2_owner_id
            #TO DELETE, DEBUG PURPOSES ONLY
            #if asset['aws_owner_id'] == "null":
            #    continue

            foundAssetMatch = 0

            if vuln['asset']['uuid'] == asset['id']: 

                foundAssetMatch = 1

                try:
                    AssetIPAddress = "=\"" + asset['ipv4s'][0] + "\""
                except:
                    try:
                        AssetIPAddress = "=\"" + asset['ipv4s'] + "\""
                    except:
                        AssetIPAddress = ""

                try:
                    AssetFQDN = asset['fqdns'][0]
                except:
                    try:
                        AssetFQDN = asset['fqdns']
                    except:
                        AssetFQDN = ""

                foundContactMatch = 0

                for awsContact in awsContactList:

                    if awsContact['Account Identifier'] == asset['aws_owner_id']:

                        foundContactMatch = 1

                        CloudProvider = "AWS"
                        CloudAccountIdentifier = "=\"" + awsContact['Account Identifier'] + "\""
                        CloudAccountName = awsContact['Account Name']
                        CloudAccountOrganisationAcronym = awsContact['Account Organisation Acronym']
                        ResponsibleContactNames = awsContact['Responsible contact names']
                        ResponsibleContactEmails = awsContact['Responsible contact emails']
                        ResponsibleContactPhones = "=\"" + awsContact['Responsible contact phones'] + "\""
                        OperationContactNames = awsContact['Operation contact names']
                        OperationContactEmails = awsContact['Operation contact emails']
                        OperationContactPhones = "=\"" + awsContact['Operation contact phones'] + "\""
                        SecurityContactNames = awsContact['Security contact names']
                        SecurityContactEmails = awsContact['Security contact emails']
                        SecurityContactPhones = "=\"" + awsContact['Security contact phones'] + "\""

                        break

                    else:
                        continue

                    #awsContactList loop ends here

                break

            else:
                continue
                #outputRow.append("")
                #outputRow.append("")
                #outputRow.append("")
            
            #assetsData loop ends here


        if foundAssetMatch == 0:
            AssetId = ""
            AssetIPAddress = ""
            AssetFQDN = ""

        if foundContactMatch == 0:
            CloudProvider = ""
            CloudAccountIdentifier = ""
            CloudAccountName = ""
            CloudAccountOrganisationAcronym = ""
            ResponsibleContactNames = ""
            ResponsibleContactEmails = ""
            ResponsibleContactPhones = ""
            OperationContactNames = ""
            OperationContactEmails = ""
            OperationContactPhones = ""
            SecurityContactNames = ""
            SecurityContactEmails = ""
            SecurityContactPhones = ""


        outputRow.append(PluginId)
        outputRow.append(PluginName)
        outputRow.append(Severity)
        outputRow.append(AssetId)
        outputRow.append(AssetIPAddress)
        outputRow.append(PluginProtocol)
        outputRow.append(PluginPort)
        outputRow.append(AssetFQDN)
        outputRow.append(PluginOutput)
        outputRow.append(PluginSynopsis)
        outputRow.append(PluginDescription)
        outputRow.append(PluginSolution)
        outputRow.append(PluginSeeAlso)
        outputRow.append(VulnerabilityPriorityRating)
        outputRow.append(CVSSV2BaseScore)
        outputRow.append(CVSSV3BaseScore)
        outputRow.append(CVSSV2Vector)
        outputRow.append(CVSSV3Vector)
        outputRow.append(ExploitMaturity)
        outputRow.append(CVE)
        outputRow.append(RiskLevel)
        outputRow.append(CloudProvider)
        outputRow.append(CloudAccountIdentifier)
        outputRow.append(CloudAccountName)
        outputRow.append(CloudAccountOrganisationAcronym)
        outputRow.append(ResponsibleContactNames)
        outputRow.append(ResponsibleContactEmails)
        outputRow.append(ResponsibleContactPhones)
        outputRow.append(OperationContactNames)
        outputRow.append(OperationContactEmails)
        outputRow.append(OperationContactPhones)
        outputRow.append(SecurityContactNames)
        outputRow.append(SecurityContactEmails)
        outputRow.append(SecurityContactPhones)

        outputList.append(outputRow)
        #vulnerabilitiesData loop ends here

    #print("---> Result of CSV generation:")
    #print(outputList)

    print("---> Writing vulnerability, assets and cloud account merged data content into CSV file: {0}".format(VULN_WITH_CONTACTS_FILE))

    with open(VULN_WITH_CONTACTS_FILE, 'w') as vulnWithContactCsvFile:
        # using csv.writer method from CSV package
        writeCSV = csv.writer(vulnWithContactCsvFile, delimiter=';')
        writeCSV.writerows(outputList)


except Exception as e:
    print("ERROR> {0}".format(e))


#print("---> Fetching cloud provider account contact information")

#print("---> Cloud provider account contact information successfuly fetched")


#finishing execution of the script
endDateTime = datetime.today()
scriptExecutionTime = endDateTime - startDateTime

print("\n---> Script execution ended @ {0}".format(endDateTime))
print("\n---> Total execution time: {0}\n".format(scriptExecutionTime))
