#Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
#the European Commission - subsequent versions of the EUPL (the "Licence");
#You may not use this work except in compliance with the Licence.
#You may obtain a copy of the Licence at:
#
#   https://joinup.ec.europa.eu/software/page/eupl
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the Licence is distributed on an "AS IS" basis,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the Licence for the specific language governing permissions and
#limitations under the Licence.

import boto3
import csv
import sys
import socket
from datetime import datetime

try:
    startDateTime = datetime.today()

    print("\n---> Script (convert_to_cloudEndpointCsv_without_FQDN.py) execution started @ {0} ...\n".format(startDateTime))

    #checking input arguments
    print("\n---> Checking number of input arguments")
    if len(sys.argv) < 3:
        print("\n---> No sufficient arguments (2) provided. Exiting.")
        sys.exit()
    else:
        INPUT_FILE = sys.argv[1]
        OUTPUT_FILE = sys.argv[2]

    print("---> Loading Azure Graph query CSV file into array: {0}".format(INPUT_FILE))

    #loading file in azure graph csv format into array/list
    csvList = []
    with open(INPUT_FILE, mode='r') as csvFile:
        csvReader = csv.reader(csvFile, delimiter=';')

        #extracting fields and putting them in order necessary for output format
        for currentRow in csvReader:
            csvRecord = []
            csvRecord.append(currentRow[0])
            csvRecord.append(currentRow[1])
            csvRecord.append(currentRow[2])
            csvRecord.append("")
            csvRecord.append(currentRow[4])
            csvList.append(csvRecord)

    csvList.sort()

    print("---> Loading finished!")

    print("---> Writing processed data content into CSV file: {0}".format(OUTPUT_FILE))

    #writing to tenable CSV file 
    with open(OUTPUT_FILE, 'w') as accountCsvFile:
        writeCSV = csv.writer(accountCsvFile, delimiter=';')
        writeCSV.writerows(csvList)

    print("---> Writing finished!")

except Exception as e:
    print("ERROR> {0}".format(e))

#finishing script execution
endDateTime = datetime.today()
scriptExecutionTime = endDateTime - startDateTime

print("\n---> Script (convert_to_cloudEndpointCsv_without_FQDN.py) execution ended @ {0}".format(endDateTime))
print("\n---> Total execution time: {0}\n".format(scriptExecutionTime))
