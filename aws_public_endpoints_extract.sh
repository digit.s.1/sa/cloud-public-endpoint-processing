#!/bin/bash

#Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
#the European Commission - subsequent versions of the EUPL (the "Licence");
#You may not use this work except in compliance with the Licence.
#You may obtain a copy of the Licence at:
#
#   https://joinup.ec.europa.eu/software/page/eupl
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the Licence is distributed on an "AS IS" basis,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the Licence for the specific language governing permissions and
#limitations under the Licence.

 
# set variables to be used
timestamp=$(date '+%y%m%d%H%M')
timestamp_start=$(date)
#findingsdir="aws-public_ip_address-$timestamp"
account_ids="/home/ubuntu/scripts/aws-accounts.txt.all"

output_file="/home/ubuntu/scripts/output/aws-public_ip_address.csv"
output_file_backup="/home/ubuntu/scripts/output/backup/aws-public_ip_address.csv.$timestamp"
output_file_error="/home/ubuntu/scripts/logs/aws_public_endpoints_extract.sh.error.log"
output_file_tmp="/home/ubuntu/scripts/logs/aws_public_endpoints_extract.tmp"
output_file_history="/home/ubuntu/scripts/output/aws-public_ip_address.csv.history"
output_file_history_tmp="/home/ubuntu/scripts/output/aws-public_ip_address.csv.history.tmp"

SNS_ARN="arn:aws:sns:eu-central-1:042034201921:aws-public-endpoints-extraction"

# pre-run setup
#[ -d $findingsdir ] || mkdir $findingsdir
#cp ~/.aws/config ~/.aws/config.bak

echo "Script execution started @ $timestamp_start"
echo "-------------------------------------------"

[ -f $output_file ] && cp $output_file $output_file_backup && rm $output_file
[ -f $output_file ] || touch $output_file


current_count=1

total_number_of_accounts=$(wc -l $account_ids | sed 's/ aws-accounts.txt.all//g')

while read -r line_text #account_id
do


  #####################################################
  ##  resetting environment
  ##################################################### 

  [ -f $output_file_tmp ] || touch $output_file_tmp
  unset AWS_ACCESS_KEY_ID
  unset AWS_SECRET_ACCESS_KEY
  unset AWS_SESSION_TOKEN

  #extracting account information
  account_id=$(echo $line_text | cut -f1 -d\; | tr -d '\r')
  account_name=$(echo $line_text | cut -f2 -d\; | tr -d '\r')
  echo "Account id: $account_id Account name: $account_name"
  
  echo "current count: $current_count of $total_number_of_accounts"

  # prepare config file for remote account
  #sed "s/ACCOUNT_ID/$account_id/" config.template > ~/.aws/config
  if [ $account_id != "042034201921" ]
  then
    eval $(aws sts assume-role --role-arn arn:aws:iam::$account_id:role/SecurityAccessReadOnlyRole --role-session-name test 2>$output_file_error | jq -r '.Credentials | "export AWS_ACCESS_KEY_ID=\(.AccessKeyId)\nexport AWS_SECRET_ACCESS_KEY=\(.SecretAccessKey)\nexport AWS_SESSION_TOKEN=\(.SessionToken)\n"')
  fi


  if [ -z $AWS_ACCESS_KEY_ID ] && [ $account_id != "042034201921" ]
  then
    current_count=$(($current_count + 1))
    echo "No permissions to assume role"
    echo "---NEXT ACCOUNT--------------"
    continue
  fi

  #####################################################
  ##  Looping through active regions for current account
  #####################################################
  for region in $(aws ec2 describe-regions  | jq '.Regions[] | select(.OptInStatus=="opt-in-not-required") | .RegionName' --raw-output); do
    #use this section if you want to loop through specific regions
    #if [ $region != "eu-north-1" ] && [ $region != "eu-west-1" ] && [ $region != "eu-west-2" ] && [ $region != "eu-west-3" ] && [ $region != "eu-central-1" ] && [ $region != "us-east-1" ] && [ $region != "us-east-2" ]
    #if [ $region != "eu-west-1" ] && [ $region != "eu-central-1" ] && [ $region != "us-east-1" ]
    #then
    #  continue
    #fi
    echo "Region: $region"
  
  #####################################################
  ##  EC 2 Elastic IP(s) fetch 
  #####################################################

    echo "-----EC2---------------"
    ip_query_result=$(aws ec2 describe-instances --region $region --query 'Reservations[*].Instances[*].[PublicIpAddress, PublicDnsName, InstanceId] | []' | sed 's/,/;/g' | sed 's/    //g' | tr -d '\n[]\"' | sed 's/Done//g' | sed 's/null//g')
    echo "$ip_query_result"
    IFS=';' read -r -a ip_query_result_array <<< "$ip_query_result"
	  
	  #ip_query_result_count=$(echo ${#array[@]})
	  #ip_query_result_count=$((ip_query_result_count))
	  
	  #if [[ $ip_query_result_count -gt 0 ]]
	  #then
	  #  echo "$account_id;$account_name" >> "aws-public_ip_address-$timestamp.log"
	  #fi
	   
    count=1
    for element in "${ip_query_result_array[@]}"; do
	if [[ $count -eq 3 ]]
	  then
        echo -n "$element" >> "$output_file_tmp"
	    echo "" >> "$output_file_tmp"
	    count=1
	elif [[ $count -eq 2 ]]
      then
        echo -n "$element;" >> "$output_file_tmp"
        count=$(($count + 1))
    else
	    echo -n "$account_id;$account_name;$element;" >> "$output_file_tmp"
	    count=$(($count + 1))
	fi
    done
    
    echo "" >> "$output_file_tmp"

    echo "" "Done with EC2"
  

  #####################################################
  ##   Elastic Load Balancer(s) (ELB) fetch 
  #####################################################
    echo "-----ELB------------"
    query_result=$(aws elbv2 describe-load-balancers --query 'LoadBalancers[*].[DNSName, LoadBalancerArn] | []' --region $region | sed 's/,/;/g' | sed 's/    //g' | tr -d '\n[]\"' | sed 's/Done//g' | sed 's/null//g')
    echo "$query_result"
    IFS=';' read -r -a query_result_array <<< "$query_result"
	  
    count=1
    for element in "${query_result_array[@]}"; do
	if [[ $count -eq 2 ]]
	then
	  echo -n "$element" >> "$output_file_tmp"
	  echo "" >> "$output_file_tmp"
	  count=1
	else
      echo -n "$account_id;$account_name;;$element;" >> "$output_file_tmp"
	  count=$(($count + 1))
	fi
    done
    
    echo "" >> "$output_file_tmp"
    
    echo "" "Done with ELB"

    unset query_result

  #####################################################
  ##  RDS fetch 
  #####################################################

    echo "-----RDS clusters------------"
    query_result=$(aws rds describe-db-clusters --region $region | jq -r '.DBClusters[] | .Endpoint + ";" + .ReaderEndpoint + ";" + .DBClusterArn + ";"' | sed 's/,/;/g' | sed 's/    //g' | tr -d '\n[]\"' | sed 's/Done//g' | sed 's/null//g')
    echo "$query_result"
    IFS=';' read -r -a query_result_array <<< "$query_result"
 
    count=1
    unset dnsEntry1
    unset dnsEntry2
    unset arnEntry

    for element in "${query_result_array[@]}"; do
      [[ $count -eq 1 ]] && dnsEntry1=$element
      [[ $count -eq 2 ]] && dnsEntry2=$element
      [[ $count -eq 3 ]] && arnEntry=$element
      
      [[ $count -eq 3 ]] && [[ ! -z $arnEntry ]] && [[ ! -z $dnsEntry1 ]] && echo "$account_id;$account_name;;$dnsEntry1;$arnEntry" >> "$output_file_tmp"
      [[ $count -eq 3 ]] && [[ ! -z $arnEntry ]] && [[ ! -z $dnsEntry2 ]] && echo "$account_id;$account_name;;$dnsEntry2;$arnEntry" >> "$output_file_tmp"
      [[ $count -eq 3 ]] && [[ -z $arnEntry ]] && [[ ! -z $dnsEntry1 ]] && [[ ! -z $dnsEntry2 ]] && echo "$account_id;$account_name;;$dnsEntry1;$dnsEntry2" >> "$output_file_tmp"

      [[ $count -eq 3 ]] && count=1 && continue

	  count=$(($count + 1))
    done

    echo "" >> "$output_file_tmp"

    echo "" "Done with RDS clusters"

    unset query_result

    echo "-----RDS instances------------"
    query_result=$(aws rds describe-db-instances --region $region | jq -r '.DBInstances[] | select( .PubliclyAccessible==true ) | .Endpoint.Address + ";" + .ListenerEndpoint.Address + ";" + .DBInstanceArn + ";"' | sed 's/,/;/g' | sed 's/    //g' | tr -d '\n[]\"' | sed 's/Done//g' | sed 's/null//g')
    echo "$query_result"
    IFS=';' read -r -a query_result_array <<< "$query_result"

    count=1
    unset dnsEntry1
    unset dnsEntry2
    unset arnEntry

    for element in "${query_result_array[@]}"; do
      [[ $count -eq 1 ]] && dnsEntry1=$element
      [[ $count -eq 2 ]] && dnsEntry2=$element
      [[ $count -eq 3 ]] && arnEntry=$element

      [[ $count -eq 3 ]] && [[ ! -z $arnEntry ]] && [[ ! -z $dnsEntry1 ]] && echo "$account_id;$account_name;;$dnsEntry1;$arnEntry" >> "$output_file_tmp"
      [[ $count -eq 3 ]] && [[ ! -z $arnEntry ]] && [[ ! -z $dnsEntry2 ]] && echo "$account_id;$account_name;;$dnsEntry2;$arnEntry" >> "$output_file_tmp"
      [[ $count -eq 3 ]] && [[ -z $arnEntry ]] && [[ ! -z $dnsEntry1 ]] && [[ ! -z $dnsEntry2 ]] && echo "$account_id;$account_name;;$dnsEntry1;$dnsEntry2" >> "$output_file_tmp"

      [[ $count -eq 3 ]] && count=1 && continue

      count=$(($count + 1))
    done

    echo "" >> "$output_file_tmp"
 
    echo "" "Done with RDS instances"	

    unset query_result

  #####################################################
  ##  Redshift fetch 
  #####################################################
    echo "-----Redshift------------"
    query_result=$(aws redshift describe-endpoint-access --query 'EndpointAccessList[*].[Address, ClusterIdentifier] | []' --region $region | sed 's/,/;/g' | sed 's/    //g' | tr -d '\n[]\"' | sed 's/Done//g' | sed 's/null//g')
    echo "$query_result"
    IFS=';' read -r -a query_result_array <<< "$query_result"
	  
    count=1
    for element in "${query_result_array[@]}"; do
	if [[ $count -eq 2 ]]
	then
	  echo -n "$element" >> "$output_file_tmp"
	  echo "" >> "$output_file_tmp"
	  count=1
	else
	  echo -n "$account_id;$account_name;$element;;" >> "$output_file_tmp"
	  count=$(($count + 1))
	fi
    done
    
    echo "" >> "$output_file_tmp"
    
    echo "" "Done with Redshift"

    unset query_result

  #end region loop
  done

  #####################################################
  ##  CloudFront fetch (not part of region loop since
  ##  CloudFront is region agnostic)
  #####################################################
  echo "-----CloudFront------------"
  unset skip_duplicate
  query_result=$(aws cloudfront list-distributions | jq -r '.DistributionList.Items[] | .Origins.Items[].DomainName + ";" + .ARN + ";"' | sed 's/,/;/g' | sed 's/    //g' | tr -d '\n[]\"' | sed 's/Done//g' | sed 's/null//g')
  echo "$query_result"
  IFS=';' read -r -a query_result_array <<< "$query_result"

  count=1
  for element in "${query_result_array[@]}"; do
  if [[ $count -eq 2 ]]
  then
    [[ -z "$skip_duplicate" ]] && echo -n "$element" >> "$output_file_tmp" && echo "" >> "$output_file_tmp"
    count=1
  else
    output_file_tmp_var=$(cat "$output_file_tmp")
    unset skip_duplicate
    #this code avoid putting same FQDN but with various ID
    [[ ! $output_file_tmp_var == *"$account_id;$account_name;;$element;"* ]] && echo -n "$account_id;$account_name;;$element;" >> "$output_file_tmp" || skip_duplicate=1
    count=$(($count + 1))
  fi
  done

  echo "" >> "$output_file_tmp"

  echo "" "Done with CloudFront"

  unset query_result

  #####################################################
  ##  Route53 (not part of region loop since Route53 
  ##  is region agnostic)
  #####################################################
  echo "-----Route53------------"
  query_result=$(aws route53 list-hosted-zones --query 'HostedZones[*].[Name, Id] | []' | sed 's/,/;/g' | sed 's/    //g' | tr -d '\n[]\"' | sed 's/Done//g' | sed 's/null//g')
  echo "$query_result"
  IFS=';' read -r -a query_result_array <<< "$query_result"
  
  count=1
  for element in "${query_result_array[@]}"; do
    if [[ $count -eq 2 ]]
    then
      echo -n "$element" >> "$output_file_tmp"
      echo "" >> "$output_file_tmp"
      count=1
    else
      echo -n "$account_id;$account_name;;$element;" >> "$output_file_tmp"
      count=$(($count + 1))
    fi
  done
  
  echo "" >> "$output_file_tmp"
  
  echo "" "Done with Route53"

  unset query_result

  echo "---postprocessing---"

  current_count=$(($current_count + 1))

  #####################################################
  ##  cleaning up empty records
  #####################################################  
  echo "---FINDINGS------------------"
  cat "$output_file_tmp"
  echo "---BEFORE PROCESSING---------"
  cat "$output_file"
  echo "---AFTER PROCESSING----------"
  #####################################################
  ## stripping lines with:
  ##    * no info
  ##    * ending with: .local. .internal. local.
  ##    * empty lines
  ##    * duplicate lines
  ## removing last character if it equals . from
  ## each line
  #####################################################
  cat "$output_file_tmp" | sed "s/$account_id;$account_name;;;.*$//g" | sed "s/$account_id;$account_name;;.*\.local\.;.*//g" | sed "s/$account_id;$account_name;;.*\.internal\.;.*//g" | sed "s/$account_id;$account_name;;.*local\.;.*//g" | sed "s/\.;/;/g" | grep . >> $output_file
  cat "$output_file"
  echo "---NEXT ACCOUNT--------------"


  rm $output_file_tmp

#end account loop
done < "$account_ids"

#creating history file
echo ""
echo "Creating history file ..."
cat "$output_file" >> "$output_file_history"
cat "$output_file_history" | sort | uniq > "$output_file_history_tmp"
cp "$output_file_history_tmp" "$output_file_history"
rm "$output_file_history_tmp"

#reseting environment for s3 and sns calls
unset AWS_ACCESS_KEY_ID
unset AWS_SECRET_ACCESS_KEY
unset AWS_SESSION_TOKEN

echo ""
echo "Storing results to S3 bucket ..."
aws s3 cp "$output_file" s3://ist-vm/exports/
aws s3 cp "$output_file_history" s3://ist-vm/exports/

echo ""
echo "Sending SNS notification ..."
timestamp_end=$(date '+%Y-%m-%d')
errorFileSize=$(wc -c "$output_file_error" | awk '{print $1}') 
[[ ! $errorFileSize -eq 0 ]] && errorLog="Errors OCCURED during extraction. Please check logs." || errorLog="No errors found during extraction."
SNS_message=$(echo "AWS public endpoint extraction finished. $errorLog")
SNS_subject=$(echo "$timestamp_end - AWS public endpoint extraction - finished")
executeResult=$(aws sns publish --topic-arn "$SNS_ARN" --subject "$SNS_subject" --message "$SNS_message")

#finishing script
timestamp_end=$(date)
echo ""
echo "Script execution ended @ $timestamp_end"
