#!/bin/bash

#Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
#the European Commission - subsequent versions of the EUPL (the "Licence");
#You may not use this work except in compliance with the Licence.
#You may obtain a copy of the Licence at:
#
#   https://joinup.ec.europa.eu/software/page/eupl
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the Licence is distributed on an "AS IS" basis,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the Licence for the specific language governing permissions and
#limitations under the Licence.

# set variables to be used
timestamp=$(date '+%y%m%d%H%M')
#findingsdir="aws-public_ip_address-$timestamp"
account_ids="/home/ubuntu/outputs/aws-accounts.txt.all"

output_file_error="/home/ubuntu/outputs/aws_asset_inventory/aws_asset_inventory.sh.error.log"

output_directory="/home/ubuntu/outputs/aws_asset_inventory/output"

backup_output_directory=$(echo "$output_directory"-)$(date +%F)

[[ ! -d $backup_output_directory ]] && mkdir -p $backup_output_directory && echo $backup_output_directory

cp -rf $output_directory $backup_output_directory

rm -rf $output_directory
 
mkdir -p $output_directory

# pre-run setup
#[ -d $findingsdir ] || mkdir $findingsdir
#cp ~/.aws/config ~/.aws/config.bak

timestamp_start=$(date)
echo "Script execution started @ $timestamp_start"
echo "-------------------------------------------"

#[ -f $output_file ] && cp $output_file $output_file_backup && rm $output_file
#[ -f $output_file ] || touch $output_file


current_count=1

total_number_of_accounts=$(wc -l $account_ids | sed 's/ aws-accounts.txt//g')

while read -r line_text #account_id
do

#[ $current_count -eq 2 ] && break
#  [ -f $output_file_tmp ] || touch $output_file_tmp
  unset AWS_ACCESS_KEY_ID
  unset AWS_SECRET_ACCESS_KEY
  unset AWS_SESSION_TOKEN

  account_id=$(echo $line_text | cut -f1 -d\;)
  account_name=$(echo $line_text | cut -f2 -d\;)
  echo "Account id: $account_id Account name: $account_name"
  
  echo "current count: $current_count of $total_number_of_accounts"

  # prepare config file for remote account
  #sed "s/ACCOUNT_ID/$account_id/" config.template > ~/.aws/config
  if [ $account_id != "042034201921" ]
  then
    eval $(aws sts assume-role --role-arn arn:aws:iam::$account_id:role/SecurityAccessReadOnlyRole --role-session-name test 2>$output_file_error | jq -r '.Credentials | "export AWS_ACCESS_KEY_ID=\(.AccessKeyId)\nexport AWS_SECRET_ACCESS_KEY=\(.SecretAccessKey)\nexport AWS_SESSION_TOKEN=\(.SessionToken)\n"')
  fi


  if [ -z $AWS_ACCESS_KEY_ID ] && [ $account_id != "042034201921" ]
  then
    current_count=$(($current_count + 1))
    echo "No permissions to assume role"
    echo "---NEXT ACCOUNT--------------"
    continue
  fi

   # go through all enabled regions
  #for region in $(aws ec2 describe-regions  | jq '.Regions[] | select(.OptInStatus=="opt-in-not-required") | .RegionName' --raw-output); do
  #  echo "  Region: $region"
    # check if security hub is enabled
    #aws securityhub describe-hub --region "$region" --profile "$account_id" > /dev/null 2>&1
    #if [ $? -eq 0 ]; then
      #echo "    extracting findings..." >> "aws-securityhub-findings-$timestamp.log"
      # save enabled standards to file
      #aws securityhub get-enabled-standards --region "$region" --profile "$account_id" > "$findingsdir/$account_id--$region--standards.json"
      # save findings to file
      #aws securityhub get-findings --filters '{"ComplianceStatus": [{"Value": "FAILED","Comparison":"EQUALS"}], "RecordState": [{"Value": "ACTIVE","Comparison":"EQUALS"}]}' --region "$region" --profile "$account_id" > "$findingsdir/$account_id--$region--findings--$timestamp.json"
      #gzip "$findingsdir/$account_id--$region--findings--$timestamp.json"
  #done

  
  #####################################################
  ##  Looping through active regions for current account
  #####################################################
  for region in $(aws ec2 describe-regions  | jq '.Regions[] | select(.OptInStatus=="opt-in-not-required") | .RegionName' --raw-output); do
    #if [ $region != "eu-north-1" ] && [ $region != "eu-west-1" ] && [ $region != "eu-west-2" ] && [ $region != "eu-west-3" ] && [ $region != "eu-central-1" ] && [ $region != "us-east-1" ] && [ $region != "us-east-2" ]
    #then
    #  continue
    #fi
    echo "Region: $region"
 
    working_directory="${output_directory}/${account_id}/${region}"

    [[ -d "$working_directory" ]] || mkdir -p "$working_directory" 
 
  #####################################################
  ##  EC 2 Elastic IP(s) fetch 
  #####################################################

    echo "-----EC2---------------"
    aws ec2 describe-instances --region $region > "$working_directory"/ec2.json

#    continue

#    ip_query_result=$(aws ec2 describe-instances --region $region --query 'Reservations[*].Instances[*].[PublicIpAddress, PublicDnsName] | []' | sed 's/,/;/g' | sed 's/    //g' | tr -d '\n[]\"' | sed 's/Done//g' | sed 's/null//g')
#    echo "$ip_query_result"
#    IFS=';' read -r -a ip_query_result_array <<< "$ip_query_result"
	  
	  #ip_query_result_count=$(echo ${#array[@]})
	  #ip_query_result_count=$((ip_query_result_count))
	  
	  #if [[ $ip_query_result_count -gt 0 ]]
	  #then
	  #  echo "$account_id;$account_name" >> "aws-public_ip_address-$timestamp.log"
	  #fi
	   
#    count=1
#    for element in "${ip_query_result_array[@]}"; do
#	if [[ $count -eq 2 ]]
#	  then
#	    echo -n "$element" >> "$output_file_tmp"
#	    echo "" >> "$output_file_tmp"
#	    count=1
#	  else
#	    echo -n "$account_id;$account_name;$element;" >> "$output_file_tmp"
#	    count=$(($count + 1))
#	fi
#   done
    
#    echo "" >> "$output_file_tmp"

    echo "" "Done with EC2"
  
  #####################################################
  ##   Elastic Load Balancer(s) (ELB) fetch 
  #####################################################
  #echo "---ELB--------------"
  #aws elb describe-load-balancers --query 'LoadBalancerDescriptions[*].DNSName | []' --region $region
    echo "-----ELB------------"
    aws elb describe-load-balancers --region $region > "$working_directory"/elb.json
#    query_result=$(aws elb describe-load-balancers --query 'LoadBalancerDescriptions[*].DNSName | []' --region $region | sed 's/,/;/g' | sed 's/    //g' | tr -d '\n[]\"' | sed 's/Done//g' | sed 's/null//g')
#    echo "$query_result"
#    IFS=';' read -r -a query_result_array <<< "$query_result"
	  
	  #query_result_count=$(echo ${#array[@]})
	  #query_result_count=$((query_result_count))
	  
	  #if [[ $query_result_count -gt 0 ]]
	  #then
	  #  echo "$account_id;$account_name" >> "aws-public_ip_address-$timestamp.log"
	  #fi
	   
#    count=1
#    for element in "${query_result_array[@]}"; do
#	if [[ $count -eq 2 ]]
#	then
#	  echo -n "$element" >> "$output_file_tmp"
#	  echo "" >> "$output_file_tmp"
#	  count=1
#	else
#	  echo "$account_id;$account_name;;$element" >> "$output_file_tmp"
#	  count=$(($count + 1))
#	fi
#    done
    
#    echo "" >> "$output_file_tmp"
    
    echo "" "Done with ELB"

#    unset query_result

  #####################################################
  ##  CloudFront fetch 
  #####################################################
  #echo "---CloudFront-------"
  #aws cloudfront list-distributions --query 'DistributionList[*].Items[*].DistributionConfig.Origins.Items[].DomainName | []' --region $region
    echo "-----CloudFront------------"
    aws cloudfront list-distributions --region $region > "$working_directory"/cloudfront.json
#    query_result=$(aws cloudfront list-distributions --query 'DistributionList[*].Items[*].DistributionConfig.Origins.Items[].DomainName | []' --region $region | sed 's/,/;/g' | sed 's/    //g' | tr -d '\n[]\"' | sed 's/Done//g' | sed 's/null//g')
#    echo "$query_result"
#    IFS=';' read -r -a query_result_array <<< "$query_result"
	  
	  #query_result_count=$(echo ${#array[@]})
	  #query_result_count=$((query_result_count))
	  
	  #if [[ $query_result_count -gt 0 ]]
	  #then
	  #  echo "$account_id;$account_name" >> "aws-public_ip_address-$timestamp.log"
	  #fi
	   
#    count=1
#    for element in "${query_result_array[@]}"; do
#	if [[ $count -eq 2 ]]
#	then
#	  echo -n "$element" >> "$output_file_tmp"
#	  echo "" >> "$output_file_tmp"
#	  count=1
#	else
#	  echo -n "$account_id;$account_name;$element;" >> "$output_file_tmp"
#	  count=$(($count + 1))
#	fi
#   done
    
#    echo "" >> "$output_file_tmp"
    
    echo "" "Done with CloudFront"

#    unset query_result

  #####################################################
  ##  RDS fetch 
  #####################################################
  #echo "---RDS clusters--------------"
  #aws rds describe-db-clusters --query 'DBClusters[*].[Endpoint,ReaderEndpoint] | []' --region $region
  #aws rds describe-db-instances --query 'DBInstances[?PubliclyAccessible==`true`].[Endpoint.Address,ListenerEndpoint.Address] | []' --region $region
    echo "-----RDS clusters------------"
    aws rds describe-db-clusters --region $region > "$working_directory"/rds_cluster.json
#    query_result=$(aws rds describe-db-clusters --query 'DBClusters[*].[Endpoint,ReaderEndpoint] | []' --region $region | sed 's/,/;/g' | sed 's/    //g' | tr -d '\n[]\"' | sed 's/Done//g' | sed 's/null//g')
#    echo "$query_result"
#    IFS=';' read -r -a query_result_array <<< "$query_result"
	  
	  #query_result_count=$(echo ${#array[@]})
	  #query_result_count=$((query_result_count))
	  
	  #if [[ $query_result_count -gt 0 ]]
	  #then
	  #  echo "$account_id;$account_name" >> "aws-public_ip_address-$timestamp.log"
	  #fi
	   
#    count=1
#    for element in "${query_result_array[@]}"; do
#	if [[ $count -eq 2 ]]
#	then
#	  echo -n "$element" >> "$output_file_tmp"
#	  echo "" >> "$output_file_tmp"
#	  count=1
#	else
#	  echo "$account_id;$account_name;;$element" >> "$output_file_tmp"
#	  count=$(($count + 1))
#	fi
#    done
    
#    echo "" >> "$output_file_tmp"
    
    echo "" "Done with RDS clusters"

#    unset query_result

    echo "-----RDS instances------------"
    aws rds describe-db-instances --region $region > "$working_directory"/rds_instances.json
#    query_result=$(aws rds describe-db-instances --query 'DBInstances[?PubliclyAccessible==`true`].[Endpoint.Address,ListenerEndpoint.Address] | []' --region $region | sed 's/,/;/g' | sed 's/    //g' | tr -d '\n[]\"' | sed 's/Done//g' | sed 's/null//g')
#    echo "$query_result"
#    IFS=';' read -r -a query_result_array <<< "$query_result"
	  
	  #query_result_count=$(echo ${#array[@]})
	  #query_result_count=$((query_result_count))
	  
	  #if [[ $query_result_count -gt 0 ]]
	  #then
	  #  echo "$account_id;$account_name" >> "aws-public_ip_address-$timestamp.log"
	  #fi
	   
#    count=1
#    for element in "${query_result_array[@]}"; do
#	if [[ $count -eq 2 ]]
#	then
#	  echo -n "$element" >> "$output_file_tmp"
#	  echo "" >> "$output_file_tmp"
#	  count=1
#	else
#	  echo "$account_id;$account_name;;$element" >> "$output_file_tmp"
#	  count=$(($count + 1))
#	fi
#    done
    
#    echo "" >> "$output_file_tmp"
    
    echo "" "Done with RDS instances"	

#    unset query_result

  #####################################################
  ##  Redshift fetch 
  #####################################################
  #echo "---Redshift---------"
  #aws redshift describe-endpoint-access --query 'EndpointAccessList[*].Address | []' --region $region
    echo "-----Redshift------------"
    aws redshift describe-endpoint-access --region $region > "$working_directory"/redshift.json
#    query_result=$(aws redshift describe-endpoint-access --query 'EndpointAccessList[*].Address | []' --region $region | sed 's/,/;/g' | sed 's/    //g' | tr -d '\n[]\"' | sed 's/Done//g' | sed 's/null//g')
#    echo "$query_result"
#    IFS=';' read -r -a query_result_array <<< "$query_result"
	  
	  #query_result_count=$(echo ${#array[@]})
	  #query_result_count=$((query_result_count))
	  
	  #if [[ $query_result_count -gt 0 ]]
	  #then
	  #  echo "$account_id;$account_name" >> "aws-public_ip_address-$timestamp.log"
	  #fi
	   
#    count=1
#    for element in "${query_result_array[@]}"; do
#	if [[ $count -eq 2 ]]
#	then
#	  echo -n "$element" >> "$output_file_tmp"
#	  echo "" >> "$output_file_tmp"
#	  count=1
#	else
#	  echo -n "$account_id;$account_name;$element;" >> "$output_file_tmp"
#	  count=$(($count + 1))
#	fi
#    done
    
#    echo "" >> "$output_file_tmp"
    
    echo "" "Done with Redshift"

    #####################################################
    ##  Elastic Container Service (ECS) fetch
    #####################################################

    echo "-----Elastic Container Service (ECS)------------"

    aws ecs describe-clusters > "$working_directory"/ecs.json

    echo "" "Done with ECS"

  #loop for regions finished here
  done

  #####################################################
  ##  Elastic Kubernetes Service (EKS) fetch
  #####################################################

  echo "-----Elastic Kubernetes Service (EKS)------------"

  aws eks list-clusters > "$working_directory"/eks.json

  echo "" "Done with EKS"

  #####################################################
  ##  Config fetch
  #####################################################

  echo "-----Config -----------------------------------"

  aws configservice get-status > "$working_directory"/config.json

  echo "" "Done with Config"

  #####################################################
  ##  S3 fetch
  #####################################################

  echo "-----S3 ---------------------------------------"

  aws s3api list-buckets > "$working_directory"/s3.json

  echo "" "Done with S3"

#    unset query_result

  
  #####################################################
  ##  Route53 (not part of region loop since Route53 
  ##  is region agnostic)
  #####################################################
  #echo "---Route53----------"
  #aws route53 list-hosted-zones --query 'HostedZones[*].Name | []'

#  echo "-----Route53------------"
#  query_result=$(aws route53 list-hosted-zones --query 'HostedZones[*].Name | []' | sed 's/,/;/g' | sed 's/    //g' | tr -d '\n[]\"' | sed 's/Done//g' | sed 's/null//g')
#  echo "$query_result"
#  IFS=';' read -r -a query_result_array <<< "$query_result"
  
  #query_result_count=$(echo ${#array[@]})
  #query_result_count=$((query_result_count))
  
  #if [[ $query_result_count -gt 0 ]]
  #then
  #  echo "$account_id;$account_name" >> "aws-public_ip_address-$timestamp.log"
  #fi
   
#  count=1
#  for element in "${query_result_array[@]}"; do
#    if [[ $count -eq 2 ]]
#    then
#      echo -n "$element" >> "$output_file_tmp"
#      echo "" >> "$output_file_tmp"
#      count=1
#    else
#      echo "$account_id;$account_name;;$element" >> "$output_file_tmp"
#      count=$(($count + 1))
#    fi
#  done
  
#  echo "" >> "$output_file_tmp"
  
#  echo "" "Done with Route53"

#  unset query_result

#  echo "---postprocessing---"

  current_count=$(($current_count + 1))

  #####################################################
  ##  cleaning up empty records
  #####################################################  
#  echo "---FINDINGS------------------"
#  cat "$output_file_tmp"
#  echo "---BEFORE PROCESSING---------"
#  cat "$output_file"
#  echo "---AFTER PROCESSING----------"
  #####################################################
  ## stripping lines with:
  ##    * no info
  ##    * ending with: .local. .internal. local.
  ##    * empty lines
  ##    * duplicate lines
  ## removing last character if it equals . from
  ## each line
  #####################################################
#  cat "$output_file_tmp" | sed "s/$account_id;$account_name;;$//g" | sed "s/.*\.local\.$//g" | sed "s/.*\.internal\.$//g" | sed "s/.*local\.$//g" | sed "s/\.$//g" | grep . >> $output_file
#  cat "$output_file"
  echo "---NEXT ACCOUNT--------------"


  #####################################################
  ##  resetting environment
  ##################################################### 

  unset AWS_ACCESS_KEY_ID
  unset AWS_SECRET_ACCESS_KEY
  unset AWS_SESSION_TOKEN
  
#  rm $output_file_tmp

done < "$account_ids"


timestamp_end=$(date)
echo "Script execution ended @ $timestamp_end"

#echo "Storing results to S3 bucket"
#aws s3 cp "$output_file" s3://ist-vm/exports/

